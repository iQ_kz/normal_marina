#include "stdafx.h"
#include "SurveyCore.h"
#include "SurveyType.h"
#include "UserManager.h"
#include "HostManager.h"
#include "FileManager.h"
#include "DocumentManager.h"
#include "Design.h"
#include "BlockSchemeDesign.h"
#include "Header.h"


namespace survey {

// 	IXMLObject* CreatorProject(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
// 
// 	IXMLObject* CreatorLogic(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
// 
// 	IXMLObject* CreatorDesign(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
// 
// 	IXMLObject* CreatorToDo(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
// 
// 	IXMLObject* CreatorRoLs(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);

	IXMLObject* CreatorText(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
}


std::unique_ptr<survey::UserManager>  survey::Singleton<survey::UserManager>::m_BuildObj;
std::unique_ptr<survey::HostManager>  survey::Singleton<survey::HostManager>::m_BuildObj;
std::unique_ptr<survey::DocumentManager>  survey::DocumentManager::s_DocManager;


const std::unordered_map<std::string, survey::SurveyType>  survey::SurveyType::s_Types = { 

	{ "project", survey::SurveyType{ "project", 0, 1, "", std::vector<std::string> { }, nullptr } },
	{ "logic", survey::SurveyType{ "logic", 0, 1, "", std::vector<std::string> { }, nullptr } },
	{ "design", survey::SurveyType{ "design", 0, 1, "", std::vector<std::string> { }, nullptr } },
	{ "block_sheme_design", survey::SurveyType{ "block_sheme_design", 0, 1, "", std::vector<std::string> { }, nullptr } },
	{ "toDo", survey::SurveyType{ "toDo", 0, 1, "", std::vector<std::string> { }, nullptr } },
	{ "roLs", survey::SurveyType{ "roLs", 0, 1, "", std::vector<std::string> { }, nullptr } },
	{ "images", survey::SurveyType{ "images", 0, 1, "", std::vector<std::string> { }, nullptr } },
	{ "texts", survey::SurveyType{ "texts", 0, 1, "", std::vector<std::string> { }, nullptr } },
	{ "text", survey::SurveyType{ "text", 0, 1, "", std::vector<std::string> { }, CreatorText } },
	{ "design_el", survey::SurveyType{ "design_el", 0, 1, "", std::vector<std::string> { }, CreatorDesignElement } },
	{ "bs_design_el", survey::SurveyType{ "bs_design_el", 0, 1, "", std::vector<std::string> { }, CreatorBlockShemeDesign } },
	{ "header", survey::SurveyType{ "header", 0, 1, "", std::vector<std::string> { }, CreatorHeader } },
	{ "question", survey::SurveyType{ "question", 0, 1, "", std::vector<std::string> { }, CreatorQuestion } },
	{ "qa", survey::SurveyType{ "qa", 0, 1, "", std::vector<std::string> { }, CreatorQa } },
	{ "block", survey::SurveyType{ "block", 0, 1, "", std::vector<std::string> { }, CreatorBlock } },
	{ "page", survey::SurveyType{ "page", 0, 1, "", std::vector<std::string> { }, CreatorPage } },
	{ "chapter", survey::SurveyType{ "chapter", 0, 1, "", std::vector<std::string> { }, CreatorChapter } },
	{ "coord", survey::SurveyType{ "coord", 0, 1, "", std::vector<std::string> { }, CreatorCoordinate } },
	{ "color", survey::SurveyType{ "color", 0, 1, "", std::vector<std::string> { }, CreatorColor } },
	{ "header_design", survey::SurveyType{ "header_design", 0, 1, "", std::vector<std::string> { }, CreatorHeaderDesignElement } },
	{ "image_element", survey::SurveyType{ "image_element", 0, 1, "", std::vector<std::string> { }, CreatorImageElement } },
	{ "border_side", survey::SurveyType{ "border_side", 0, 1, "", std::vector<std::string> { }, CreatorBorderElement } },
	{ "border_side_element", survey::SurveyType{ "border_side_element", 0, 1, "", std::vector<std::string> { }, CreatorBorderSideElement } },
	{ "image", survey::SurveyType{ "image", 0, 1, "", std::vector<std::string> { }, CreatorImage } },

	{ "logic_el", survey::SurveyType{ "logic_el", 0, 1, "", std::vector<std::string> { }, CreatorLogicElement } },
	{ "show", survey::SurveyType{ "show", 0, 1, "", std::vector<std::string> { }, CreatorShowElement } },
	{ "hide", survey::SurveyType{ "hide", 0, 1, "", std::vector<std::string> { }, CreatorHideElement } },
	{ "enable", survey::SurveyType{ "enable", 0, 1, "", std::vector<std::string> { }, CreatorEnableElement } },
	{ "disable", survey::SurveyType{ "disable", 0, 1, "", std::vector<std::string> { }, CreatorDisableElement } },
	{ "value", survey::SurveyType{ "value", 0, 1, "", std::vector<std::string> { }, CreatorValueElement } },
	{ "empty", survey::SurveyType{ "value", 0, 1, "", std::vector<std::string> { }, CreatorEmptyElement } },
	{ "while", survey::SurveyType{ "while", 0, 1, "", std::vector<std::string> { }, CreatorWhileElement } },
	{ "while_shown", survey::SurveyType{ "while_shown", 0, 1, "", std::vector<std::string> { }, CreatorWhileShownElement } },
	{ "while_enabled", survey::SurveyType{ "while_enabled", 0, 1, "", std::vector<std::string> { }, CreatorWhileEnabledElement } },
	{ "while_hidden", survey::SurveyType{ "while_hidden", 0, 1, "", std::vector<std::string> { }, CreatorWhileHiddenElement } },
	{ "while_disable", survey::SurveyType{ "while_disable", 0, 1, "", std::vector<std::string> { }, CreatorWhileDisabledElement } },
	{ "on_click", survey::SurveyType{ "on_click", 0, 1, "", std::vector<std::string> { }, CreatorOnClickElement } },
	{ "on_enter", survey::SurveyType{ "on_enter", 0, 1, "", std::vector<std::string> { }, CreatorOnEnterElement } },
	{ "on_show", survey::SurveyType{ "on_show", 0, 1, "", std::vector<std::string> { }, CreatorOnShowElement } },
	{ "on_enable", survey::SurveyType{ "on_enable", 0, 1, "", std::vector<std::string> { }, CreatorOnEnableElement } },
	{ "on_leave", survey::SurveyType{ "on_leave", 0, 1, "", std::vector<std::string> { }, CreatorOnLeaveElement } },
	{ "on_hide", survey::SurveyType{ "on_hide", 0, 1, "", std::vector<std::string> { }, CreatorOnHideElement } },
	{ "on_disable", survey::SurveyType{ "on_disable", 0, 1, "", std::vector<std::string> { }, CreatorOnDisableElement } },
	{ "on_change", survey::SurveyType{ "on_change", 0, 1, "", std::vector<std::string> { }, CreatorOnChangeElement } },
};


namespace survey {



// 	IXMLObject* CreatorProject(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
// 	{
// 		return NULL;
// 	}
// 
// 	IXMLObject* CreatorLogic(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
// 	{
// 		return NULL;
// 	}
// 
// 	IXMLObject* CreatorDesign(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
// 	{
// 		return NULL;
// 	}
// 
// 	IXMLObject* CreatorToDo(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
// 	{
// 		return NULL;
// 	}
// 
// 	IXMLObject* CreatorRoLs(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
// 	{
// 		return NULL;
// 	}


	IXMLObject* SurveyType::CreatorText(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Text(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorDesignElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new DesignElement(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorBlockShemeDesign(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new BSDesignElement(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorHeader(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Header(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorQuestion(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Question(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorQa(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Qa(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorBlock(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Block(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorPage(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Page(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorChapter(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Chapter(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorCoordinate(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Coordinate(pXmlElement, childs, pDocument);
	}
	
	IXMLObject* SurveyType::CreatorColor(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Color(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorHeaderDesignElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new HeaderDesignElement(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorImageElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new ImageElement(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorBorderElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new BorderElement(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorBorderSideElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new BorderSideElement(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorImage(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new Image(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorLogicElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new LogicElement(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorShowElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new ShowObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorEnableElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new EnableObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorValueElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new ValueObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorHideElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new ShowObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorDisableElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new EnableObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorEmptyElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new ValueObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorWhileElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new WhileObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorWhileShownElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new WhileShownObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorWhileEnabledElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new WhileEnabledObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorWhileHiddenElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new WhileHiddenObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorWhileDisabledElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new WhileDisabledObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorOnClickElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new OnClickObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorOnEnterElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new OnEnterObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorOnShowElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new OnShowObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorOnEnableElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new OnEnableObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorOnLeaveElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new OnLeaveObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorOnHideElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new OnHideObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorOnDisableElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new OnDisableObject(pXmlElement, childs, pDocument);
	}

	IXMLObject* SurveyType::CreatorOnChangeElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument)
	{
		return new OnChangeObject(pXmlElement, childs, pDocument);
	}
}