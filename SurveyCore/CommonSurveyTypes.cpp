#include "stdafx.h"
#include "CommonSurveyTypes.h"
#include "Document.h"


namespace survey {

	Image* ImageElement::GetImage() const
	{
		// to do return images
		auto itrt = m_pDocument->GetImages()->GetImages().find(m_ImageProperties.m_ImageId);
		if (itrt == m_pDocument->GetImages()->GetImages().end())
			return NULL;

		return itrt->second;
	}


}