#include "stdafx.h"
#include "SurveyCore.h"
#include "MathSv.h"


using namespace survey;


const survey::math::Vector2 NaN_VK (
	std::numeric_limits<double>::quiet_NaN(),
	std::numeric_limits<double>::quiet_NaN(),
	std::numeric_limits<double>::quiet_NaN()
);

const survey::math::Vector2 OX_VK(1, 0);
const survey::math::Vector2 OY_VK(0, 1);
const survey::math::Vector2 Null_VK(0, 0);


namespace survey {
	namespace math {

		// --------------------------------------------------------------
		// ----------------------- Math ------------------------------
		// ------------------------------------------------------------

		bool Compare(double vl1, double vl2, double tol)
		{
			return fabs(vl1 - vl2) <= tol;
		}

		bool Compare(const Vector2& vc1, const Vector2& vc2, double tol)
		{
			if (Compare(vc1.X, vc2.X, tol))
				if (Compare(vc1.Y, vc2.Y, tol))
					return true;

			return false;
		}

		Vector2 operator * (const Vector2& vc, const Matrix2& mt)
		{
			return Vector2(
				mt.VLS[0][0] * vc.X + mt.VLS[1][0] * vc.Y + mt.VLS[2][0] * vc.W,
				mt.VLS[0][1] * vc.X + mt.VLS[1][1] * vc.Y + mt.VLS[2][1] * vc.W,
				mt.VLS[0][2] * vc.X + mt.VLS[1][2] * vc.Y + mt.VLS[2][2] * vc.W);
		}

		Vector2& operator *= (Vector2& vc, const Matrix2& mt)
		{
			return vc = vc * mt;
		}

		Vector2 TransformNormal(const Vector2& vc, const Matrix2& mt)
		{
			return Vector2(
				mt.VLS[0][0] * vc.X + mt.VLS[1][0] * vc.Y,
				mt.VLS[0][1] * vc.X + mt.VLS[1][1] * vc.Y,
				mt.VLS[0][2] * vc.X + mt.VLS[1][2] * vc.Y);
		}

		Matrix2 MatrixTranslation(Vector2 vc)
		{
			Matrix2 res;
			res.VLS[2][0] = vc.X;
			res.VLS[2][1] = vc.Y;
			return res;
		}

		Matrix2 MatrixRotation(double angle)
		{
			Matrix2 res;
			res.VLS[0][0] = cos(angle);
			res.VLS[1][1] = cos(angle);
			res.VLS[1][0] = -sin(angle);
			res.VLS[0][1] = sin(angle);

			return res;
		}

		double AngleRotation(const Matrix2& mtr)
		{
			return atan2(mtr[0][1], mtr[0][0]);
		}

		double Dot(const Vector2& vc1, const Vector2& vc2)
		{
			return vc1.X*vc2.X + vc1.Y*vc2.Y;
		}

		Matrix2 InverseMatrix(const Matrix2& mtr)
		{
			const Vector2 pos(mtr.VLS[2][0], mtr.VLS[2][1]);

			return Matrix2(
				mtr.VLS[0][0], mtr.VLS[1][0], 0,
				mtr.VLS[0][1], mtr.VLS[1][1], 0,
				-Dot(Vector2(mtr.VLS[0][0], mtr.VLS[0][1]), pos),
				-Dot(Vector2(mtr.VLS[1][0], mtr.VLS[1][1]), pos),
				1
				);
		}
	
		// --------------------------------------------------------------
		// ----------------------- Vector2 ------------------------------
		// -------------------------------------------------------------

		Vector2 Vector2::operator + (const Vector2& vc) const
		{
			return Vector2(X + vc.X, Y + vc.Y);
		}

		Vector2 Vector2::operator - (const Vector2& vc) const
		{
			return Vector2(X - vc.X, Y - vc.Y);
		}

		Vector2 Vector2::operator * (double vl) const
		{
			return Vector2(X * vl, Y * vl);
		}

		Vector2 Vector2::operator / (double vl) const
		{
			return Vector2(X / vl, Y / vl);
		}

		Vector2& Vector2::operator += (const Vector2& vc)
		{
			X += vc.X; Y += vc.Y; return *this;
		}

		Vector2& Vector2::operator -= (const Vector2& vc)
		{
			X -= vc.X; Y -= vc.Y; return *this;
		}

		Vector2& Vector2::operator *= (double vl)
		{
			X *= vl; Y *= vl; return *this;
		}

		Vector2& Vector2::operator /= (double vl)
		{
			X /= vl; Y /= vl; return *this;
		}

		Vector2 operator * (const double& vl, const Vector2 vc)
		{
			return vc * vl;
		}

		Vector2 Vector2::OrtX() const { return Vector2(X, 0); }
		Vector2 Vector2::OrtY() const { return Vector2(0, Y); }

		Vector2 Vector2::Revers() const { return Vector2(-X, -Y); }

		double Vector2::Length() const { return sqrt(X*X + Y*Y); }

		double Vector2::LenghtSquare() const { return X*X + Y*Y; }

		bool Vector2::Normalize(double tol)
		{
			double ln = X*X + Y*Y;
			if (Compare(ln, 0, tol))
				return false;

			ln = sqrt(ln);

			X /= ln; Y /= ln;
			return true;
		}

		bool Vector2::IsNormal(double tol) const
		{
			return Compare(X*X + Y*Y, 1, tol);
		}

		// statics
		const Vector2& Vector2::NaN() { return NaN_VK; }

		const Vector2& Vector2::Null() { return Null_VK; }

		const Vector2& Vector2::OX() { return OX_VK; }

		const Vector2& Vector2::OY() { return OY_VK; }

		// --------------------------------------------------------------
		// ----------------------- Matrix2 ------------------------------
		// -------------------------------------------------------------

		Matrix2::Matrix2()
			: V_11(1), V_12(0), V_13(0)
			, V_21(0), V_22(1), V_23(0)
			, V_31(0), V_32(0), V_33(1)
		{ }

		Matrix2::Matrix2(double v_11, double v_12, double v_13
			, double v_21, double v_22, double v_23
			, double v_31, double v_32, double v_33
			)
			: V_11(v_11), V_12(v_12), V_13(v_13)
			, V_21(v_21), V_22(v_22), V_23(v_23)
			, V_31(v_31), V_32(v_32), V_33(v_33)
		{ }

		double* Matrix2::operator [] (UINT row)
		{
			assert(row < 3);
			return VLS[row];
		}

		const double* Matrix2::operator [] (UINT row) const
		{
			assert(row < 3);
			return VLS[row];
		}

		double Matrix2::operator()(UINT row, UINT col) const
		{
			assert(row < 3 && col < 3);
			return VLS[row][col];
		}

		Matrix2 Matrix2::operator * (double vl) const
		{
			return Matrix2(V_11 * vl, V_12 * vl, V_13 * vl,
				V_21 * vl, V_22 * vl, V_23 * vl,
				V_31 * vl, V_32 * vl, V_33 * vl);
		}

		Matrix2& Matrix2::operator *= (double vl)
		{
			for (UINT i = 0; i < 3; i++)
				for (UINT j = 0; j < 3; j++)
					VLS[i][j] *= vl;

			return *this;
		}

		Matrix2 Matrix2::operator / (double vl) const
		{
			return Matrix2(V_11 / vl, V_12 / vl, V_13 / vl,
				V_21 / vl, V_22 / vl, V_23 / vl,
				V_31 / vl, V_32 / vl, V_33 / vl);
		}

		Matrix2& Matrix2::operator /= (double vl)
		{
			for (UINT i = 0; i < 3; i++)
				for (UINT j = 0; j < 3; j++)
					VLS[i][j] /= vl;

			return *this;
		}

		Matrix2 Matrix2::operator + (const Matrix2& mtr) const
		{
			Matrix2 outMtr;

			for (UINT i = 0; i < 3; i++)
				for (UINT j = 0; j < 3; j++)
					outMtr.VLS[i][j] = VLS[i][j] + mtr.VLS[i][j];

			return outMtr;
		}

		Matrix2& Matrix2::operator += (const Matrix2& mtr)
		{
			for (UINT i = 0; i < 3; i++)
				for (UINT j = 0; j < 3; j++)
					VLS[i][j] += mtr.VLS[i][j];

			return *this;
		}

		Matrix2 Matrix2::operator - (const Matrix2& mtr) const
		{
			Matrix2 outMtr;

			for (UINT i = 0; i < 3; i++)
				for (UINT j = 0; j < 3; j++)
					outMtr.VLS[i][j] = VLS[i][j] - mtr.VLS[i][j];

			return outMtr;
		}

		Matrix2& Matrix2::operator -= (const Matrix2& mtr)
		{
			for (UINT i = 0; i < 3; i++)
				for (UINT j = 0; j < 3; j++)
					VLS[i][j] -= mtr.VLS[i][j];

			return *this;
		}

		Matrix2 Matrix2::operator * (const Matrix2& mtr) const
		{
			Matrix2 out;

			for (UINT i = 0; i < 3; i++)
			{
				for (UINT j = 0; j < 3; j++)
				{
					double summ = 0;
					for (UINT k = 0; k < 3; k++)
						summ += VLS[i][k] * mtr.VLS[k][j];

					out.VLS[i][j] = summ;
				}
			}

			return out;
		}

		Matrix2& Matrix2::operator *= (const Matrix2& mtr)
		{
			return *this = *this * mtr;
		}

	}
}