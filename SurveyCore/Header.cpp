#include "stdafx.h"
#include "Header.h"
#include "Document.h"


namespace survey {

	Header::Header(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord,
		const ColorDat& color, const CoordDat& coord, const std::string& text,
		const BorderDat& borderDat, const ImageProperties& imageProperties,
		const std::string& name)
		: DocumentObject(pDoc, bsColor, bsCoord, name)
		, m_pText(NULL)
	{
		m_pDocument->GetDesign()->AddChild(m_pDesignElement = new HeaderDesignElement(m_ID, coord, color, borderDat, imageProperties, pDoc));
		m_pText = pDoc->GetTexts()->AddText(m_ID, text);
	}


	void Header::EjectSourcesElements(std::unordered_map<std::string, std::list<IXMLObject*>>& sourcesElements)
	{
		DocumentObject::EjectSourcesElements(sourcesElements);
		sourcesElements[Texts::Type().m_Name].push_back(m_pText);
		m_pText = NULL;
	}


	void Header::loadElement(const TiXmlElement* pTiElement)
	{
		m_pText = m_pDocument->GetTexts()->GetTextByID(m_ID);
		assert(m_pText);
	}

}
