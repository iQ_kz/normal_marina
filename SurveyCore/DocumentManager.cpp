#include "stdafx.h"
#include "UserManager.h"
#include "DocumentManager.h"
#include "Cryptography.h"
#include "FileManager.h"

namespace survey {


	PermissionToWrite RoLs::CurrentUserPermissionWrite() const
	{
		UserInfo* pCurrentUser = UserManager::Instance()->CurrentUser();
		if (!pCurrentUser)
			return PermissionToWrite::PermWrLockAll;

		return m_WriteRols.at(pCurrentUser->m_Login);
	}

	PermissionToRead RoLs::CurrentUserPermissionRead() const
	{
		UserInfo* pCurrentUser = UserManager::Instance()->CurrentUser();
		if (!pCurrentUser)
			return PermissionToRead::PermRdLockAll;

		return m_ReadRols.at(pCurrentUser->m_Login);
	}


	DocumentManager::DocumentManager()
	{
		m_Documents.emplace("default", DocumentPtr(new Document("default")));

		std::string md5A = Cryptography::CalculateMd5("string One");
		std::string md5B = Cryptography::CalculateMd5("string One");
		std::string md5C = Cryptography::CalculateMd5("string Two");
		std::string md5D = Cryptography::CalculateMd5("string Abra cadabra");

		double gf = 9;

// 		FileManager* pFileMngr = FileManager::Instance(boost::filesystem::path(L"C:\\Users\\Alexandr\\Desktop\\surv_work_directory"), boost::filesystem::path(L"C:\\Users\\Alexandr\\Desktop\\surv_work_directory"));
// 
// 
// 		const auto& projects = pFileMngr->GetProjectInfos();
// 
// 		for (const auto& pairPrj : projects)
// 		{
// 			std::string id = pairPrj.second->GetID();
// 			std::string name = pairPrj.second->GetName();
// 			boost::posix_time::ptime time = pairPrj.second->GetCreatedData();
// 
// 			double dsf = 9898;
// 		}
// 

		// ImageHelper::ScaleImage(L"600488.jpg", L"600488.png", ImageType::IT_PNG, 10);


	}


	Document* DocumentManager::LoadDocumentByID(const std::string& id)
	{
		try
		{
			if (m_Documents.find(id) != m_Documents.end())
				return m_Documents.at(id).get();

			DocumentInfo docInfo;
			if (!FileManager::GetInstance()->GetDocument(id, docInfo))
			{
				// to do log error
				return NULL;
			}

			Document* pNewDoc = new Document(docInfo);
			m_Documents.emplace(pNewDoc->Name(), DocumentPtr(pNewDoc));

			EventArg arg = { "dialog" };
			InvokeChange(this, &arg);

			return pNewDoc;
		}
		catch (...)
		{
			// to do log error
		}

		return NULL;
	}

}


