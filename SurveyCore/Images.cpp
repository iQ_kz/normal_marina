#include "stdafx.h"
#include <boost/filesystem/path.hpp>
#include "Cryptography.h"
#include "Images.h"
#include "Document.h"

namespace survey {

	Image::Image(const boost::filesystem::path& pathFile, Document* pDoc)
		: IXMLObject(pDoc)
	{
		assert(pDoc);

		if (!boost::filesystem::is_regular_file(pathFile))
		{
			// to do log error
			throw std::exception("Invalid path!");
		}

		// id
		m_ID = Cryptography::UUID_Generate();

		// ssh 
		m_SSH = Cryptography::Bin2hex(Cryptography::CalculateMd5_ByFile(pathFile));

		// name
		m_ImageName = boost::filesystem::change_extension(pathFile, "").string();

		// create new path in file trash
		boost::filesystem::path imagesDir = FileManager::GetInstance()->GetWorkDirectory() / boost::filesystem::path("images");
		if (!boost::filesystem::is_directory(imagesDir))
		{
			if (!boost::filesystem::create_directory(imagesDir))
			{
				// to do log error
				throw std::exception("Invalid path!");
			}
		}

		boost::filesystem::path imagesPath = imagesDir / m_ID;
		boost::filesystem::path previewPath = imagesDir / (m_ID + "_preview" + ".png");

		// create preview
		if (!ImageHelper::ResizeImage(pathFile, previewPath, ImageType::IT_PNG, 640, 480))
		{
			// to do log error
			throw std::exception("Fail image!");
		}

		// copy image file
		boost::system::error_code errorCode;
		boost::filesystem::copy_file(pathFile, imagesPath, errorCode);

		if (errorCode)
		{
			// to do log error
			throw std::exception("Fail image!");
		}
	}


	bool Image::ImageData(std::vector<unsigned char>& fData) const
	{
		boost::filesystem::path imagesPath = GetImagePath();

		FileStream flStream(imagesPath);
		return flStream.GetFileData(fData);
	}


	bool Image::ImageDataAsPNG(std::vector<unsigned char>& fData) const
	{
		std::vector<unsigned char> imageData;
		if (!ImageData(imageData))
		{
			// to do log error
			return false;
		}

		return ImageHelper::ConvertToPNG(imageData, fData);
	}


	bool Image::PreviewImageData(std::vector<unsigned char>& fData) const
	{
		boost::filesystem::path imagesDir = FileManager::GetInstance()->GetWorkDirectory() / boost::filesystem::path("images");
		boost::filesystem::path imagesPath = imagesDir / (m_ID + "_preview" + ".png");

		FileStream flStream(imagesPath);
		return flStream.GetFileData(fData);
	}


	boost::filesystem::path Image::GetImagePath() const
	{
		return FileManager::GetInstance()->GetWorkDirectory() / boost::filesystem::path("images") / m_ID;
	}

}