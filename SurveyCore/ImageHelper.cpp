#include "stdafx.h"
#include "ximage.h"
#include "ImageHelper.h"


namespace survey {



	bool ImageHelper::ResizeImage(const boost::filesystem::path& srcImgPath, const boost::filesystem::path& dstImgPath, ImageType imgTp, int width, int height)
	{
		try {

			if (width <= 0 || height <= 0)
			{
				// to do log error
				return false;
			}

			CxImage img;
			if (!img.Load(srcImgPath.c_str(), ENUM_CXIMAGE_FORMATS::CXIMAGE_FORMAT_UNKNOWN))
			{
				// to do log error
				return false;
			}

			const float scaleW = float(width) / img.GetWidth();
			const float scaleH = float(height) / img.GetHeight();

			const float scale = std::min<float>(scaleW, scaleH);

			width = int(img.GetWidth() * scale);
			height = int(img.GetHeight() * scale);

			if (!img.Resample(width, height, 2))
			{
				// to do log error
				return false;
			}

			if (!img.Save(dstImgPath.c_str(), ENUM_CXIMAGE_FORMATS(imgTp)))
			{
				// to do log error
				return false;
			}

			return true;
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		return false;
	}


	bool ImageHelper::ScaleImage(const boost::filesystem::path& srcImgPath, const boost::filesystem::path& dstImgPath, ImageType imgTp, float scale)
	{
		try {

			if (scale <= 0.f)
			{
				// to do log error
				return false;
			}

			CxImage img;
			if (!img.Load(srcImgPath.c_str(), ENUM_CXIMAGE_FORMATS::CXIMAGE_FORMAT_UNKNOWN))
			{
				// to do log error
				return false;
			}

			const int width = int(img.GetWidth() * scale);
			const int height = int(img.GetHeight() * scale);

			if (!img.Resample(width, height, 2))
			{
				// to do log error
				return false;
			}

			if (!img.Save(dstImgPath.c_str(), ENUM_CXIMAGE_FORMATS(imgTp)))
			{
				// to do log error
				return false;
			}

			return true;
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		return false;
	}


	bool ImageHelper::ConvertToPNG(const std::vector<unsigned char>& inData, std::vector<unsigned char>& outData)
	{
		try {

			outData.clear();

			CxImage img((uint8_t*)inData.data(), inData.size(), ENUM_CXIMAGE_FORMATS::CXIMAGE_FORMAT_UNKNOWN);
			if (!img.IsValid())
				return false;

			unsigned char* pBuffer = NULL;
			int32_t szBuffer = 0;

			if (!img.Encode(pBuffer, szBuffer, ENUM_CXIMAGE_FORMATS::CXIMAGE_FORMAT_PNG))
				return false;

			outData.reserve(szBuffer);
			std::copy(pBuffer, pBuffer + szBuffer, std::back_inserter(outData));

			delete pBuffer;
			return true;
		}
		catch (const std::exception& ex)
		{
			// to do log error
		}
		catch (...)
		{
			// to do log error
		}

		return false;
	}

}
