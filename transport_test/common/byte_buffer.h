#pragma once
#include <vector>
#include <memory>
typedef unsigned char byte;


    class byte_buffer
    {
    public:
        typedef std::vector <byte> byte_storage_type;
        byte_buffer(){ _bytes.reserve(1024); begin_write(); }
        byte_buffer(const byte_buffer& obj){ operator =(obj); }
        byte_buffer& operator =(const byte_buffer& obj)
        {
            if (this != &obj)
            {
                _bytes = obj._bytes;
            }
            return *this;
        }

        void		begin_write()				{ _bytes.resize(sizeof(size_t)); }
        const byte* get_data()const				{ return &_bytes[get_index()]; }
        const byte* get_data(size_t index)const { return &_bytes[index]; }
        size_t		get_size()				{
			size_t& ret = access_size();
			return ret; 
		}
        size_t		get_index()const			{ return sizeof(size_t); }

        const byte* get_full_buffer()const		{ return &_bytes.front(); }
        size_t		get_full_size()const		{ return _bytes.size(); }

        byte*		append_buff(size_t size)
        {
			if (size > 0)
			{
				size_t old_full_size = _bytes.size();
				size_t old_size = get_size();
				size_t& new_size = access_size();
				new_size = old_size + size;
				_bytes.resize(old_full_size + size);
				return &_bytes[old_full_size];
			}
			else
			{
				return nullptr;
			}
        }

        void reserve(size_t newSize){ _bytes.reserve(newSize); }
		void set_size(size_t size)
		{
			size_t& size_ = access_size();
			size_ = size;
		}

    protected:
        size_t&		access_size(){ return *(size_t*)&_bytes.front(); }
        byte_storage_type _bytes;
    };

    typedef std::shared_ptr <byte_buffer>   byte_buffer_ptr;


