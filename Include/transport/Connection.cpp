#include "stdafx.h"
#include "Connection.h"

#include <sstream>
#include <boost/shared_ptr.hpp>

namespace survey
{
	namespace transport
	{
		typedef Connection::IConnectionImpl IConnectionImpl;
		template<typename SocketType>
		class ConnectionImpl : public 	IConnectionImpl
		{

			
		public:
			ConnectionImpl() {}

			typedef ConnectionImpl<SocketType> ThisType;
			typedef SocketType SocketType;
			typedef boost::shared_ptr <SocketType> SocketPtr;
			typedef boost::shared_ptr <boost::asio::strand > SrandPtr;

			void Init(IO_Svc& IO)
			{
				
				m_readStrand = boost::make_shared<boost::asio::strand >(IO);
				m_writeStrand = boost::make_shared<boost::asio::strand >(IO);

			}

			virtual void Send(ByteBufferPtr BuffPtr,  ConnectionPtr pMaster)
			{
				BOOST_LOG_NAMED_SCOPE("Connection: Send");
				BOOST_LOG_SEV(slg, normal) << " - ";
				boost::shared_ptr <size_t> SizePtr(new size_t);
				size_t& s = *SizePtr;
				s = BuffPtr->GetSize();
				m_SocketPtr->async_write_some(boost::asio::buffer((Byte*)&s, sizeof(s)), m_writeStrand->wrap(boost::bind(&ThisType::OnSendHead, this, BuffPtr, SizePtr, boost::asio::placeholders::error, pMaster)));
			}

			virtual void OnSendHead(ByteBufferPtr BuffPtr, boost::shared_ptr <size_t> SizePtr, const boost::system::error_code& Error, ConnectionPtr pMaster)
			{
				BOOST_LOG_NAMED_SCOPE("Connection: Send");
				if (Error)
				{
					std::stringstream out;
					out <<  " OnSendHead : error" << std::endl;
					BOOST_LOG_SEV(slg, error) << out.str();
					pMaster->OnError(out.str(), Error);
				}
				else
				{
					m_SocketPtr->async_write_some(boost::asio::buffer(BuffPtr->Data(), BuffPtr->GetSize()), m_writeStrand->wrap(boost::bind(&ThisType::OnSend, this, BuffPtr, boost::asio::placeholders::error, pMaster)));
				}
			}


			virtual void OnSend(ByteBufferPtr BuffPtr, const boost::system::error_code& Error, ConnectionPtr pMaster)
			{
				BOOST_LOG_NAMED_SCOPE("Connection: OnSend");
				BOOST_LOG_SEV(slg, normal) << " - ";
				if (Error)
				{
					std::stringstream out;
					out << " OnSend: error" << std::endl;
					BOOST_LOG_SEV(slg, error) << Error.message();
					pMaster->OnError(out.str(), Error);

				}
				else
				{
					std::cout << "thread: " << boost::this_thread::get_id() << "; OnSend: Ok" << std::endl;
					//DoRead(pMaster);
				}
			}

			virtual void DoRead(ConnectionPtr pMaster)
			{
				BOOST_LOG_NAMED_SCOPE("Connection: DoRead");

				BOOST_LOG_SEV(slg, normal) << " - ";
				boost::shared_ptr <size_t> sizeptr(new size_t);
				size_t& s = *sizeptr;
				m_SocketPtr->async_read_some(boost::asio::buffer((Byte*)&s, sizeof(s)), m_writeStrand->wrap(boost::bind(&ThisType::OnReadSize, this, sizeptr, boost::asio::placeholders::error, pMaster)));

			}

			virtual void OnReadSize(boost::shared_ptr <size_t> SizePtr, const boost::system::error_code& Error, ConnectionPtr pMaster)
			{
				BOOST_LOG_NAMED_SCOPE("Connection: OnReadSize");
				BOOST_LOG_SEV(slg, normal) << " - ";
				if (Error)
				{
					std::stringstream out;
					out << " OnReadSize: error" << std::endl;
					std::string str = out.str();
					BOOST_LOG_SEV(slg, error) << Error.message();
					pMaster->OnError(str, Error);
				}
				else
				{
					ByteBufferPtr BuffPtr = ByteBuffer::Create();
					BuffPtr->Resize(*SizePtr);
					m_SocketPtr->async_read_some(boost::asio::buffer(BuffPtr->Data(), BuffPtr->GetSize()), m_writeStrand->wrap(boost::bind(&ThisType::OnRead, this, BuffPtr, boost::asio::placeholders::error, pMaster)));
				}
			}

			virtual void OnRead(ByteBufferPtr BuffPtr, const boost::system::error_code& Error, ConnectionPtr pMaster)
			{
				BOOST_LOG_NAMED_SCOPE("Connection: OnRead");
				BOOST_LOG_SEV(slg, normal) << " - ";
				
				pMaster->OnRead(BuffPtr, Error);
				DoRead(pMaster);
			}

			virtual tcp::socket& Socket() = 0;


			void Close()
			{
				if (m_SocketPtr)
				{
					Socket().close();
				}
			}

			virtual void ServerStart( ConnectionPtr pMaster){ m_WillignessHandler(pMaster); }

			virtual void ClientStart( ConnectionPtr pMaster){ m_WillignessHandler(pMaster); }

			void SetWillignessHandler(Connection::WillignessHandlerType Handler){ m_WillignessHandler = Handler; }

		protected:

		//	ConnectionPtr m_Master;

			Connection::WillignessHandlerType m_WillignessHandler;

			SocketPtr m_SocketPtr;

			src::severity_logger< SeverityLevel > slg;
			SrandPtr m_readStrand;
			SrandPtr m_writeStrand;
		};


		class OpenConnection : public ConnectionImpl<tcp::socket>
		{
		public:
			typedef ConnectionImpl<tcp::socket> base;
			OpenConnection() :base(){}


			void Init(IO_Svc& IO)
			{
				base::Init(IO);
				m_SocketPtr = boost::make_shared<SocketType>(IO);
			}


			virtual tcp::socket& Socket() 
			{
				return *m_SocketPtr;
			}
		};


		class SSLConnection : public ConnectionImpl<SSLSocket>
		{
		public:
			//typedef  Connection<SSLSocket>::SocketType SocketType;
			typedef SSLSocket SocketType;
			typedef ConnectionImpl<SSLSocket> base;

			SSLConnection( ){}


			void Init(IO_Svc& IO, SSLContextPtr ContextPtr)
			{
				base::Init(IO);

				m_ContextPtr = ContextPtr;

				
				m_SocketPtr = boost::make_shared<SocketType>(IO, *m_ContextPtr);
			}


			virtual boost::asio::ip::tcp::socket& Socket()
			{
				SSLSocket&s = *m_SocketPtr;
				SSLSocket::lowest_layer_type& llt = s.lowest_layer();
				return reinterpret_cast<boost::asio::ip::tcp::socket&>(llt);
			}


			void ServerStart( ConnectionPtr pMaster)
			{
				BOOST_LOG_SEV(slg, normal) << "SSLConnection: ServerStart";
				m_SocketPtr->async_handshake(boost::asio::ssl::stream_base::server,
					boost::bind(&SSLConnection::HandleHandshake, this,
					boost::asio::placeholders::error, pMaster));
			}


			void ClientStart( ConnectionPtr pMaster)
			{
				BOOST_LOG_SEV(slg, normal) << "SSLConnection: ClientStart";
				m_SocketPtr->async_handshake(boost::asio::ssl::stream_base::client,
					boost::bind(&SSLConnection::HandleHandshake, this,
					boost::asio::placeholders::error, pMaster));
			}


			void HandleHandshake(const boost::system::error_code& error, ConnectionPtr pMaster)
			{
				BOOST_LOG_SEV(slg, normal) << "SSLConnection: HandleHandshake";
				m_WillignessHandler(pMaster);
			}

			SSLContextPtr m_ContextPtr;

			src::severity_logger< SeverityLevel > slg;
		};


		survey::transport::ConnectionPtr Connection::Create(IO_Svc& IO)
		{
			Connection* NewSession = new Connection(IO);
			ConnectionPtr ret(NewSession);
			OpenConnection* conn = new OpenConnection();
			conn->Init(IO);
			NewSession->m_ConnectionPtr = IConnectionPtr(conn);

			
			return ret;
		}


		survey::transport::ConnectionPtr Connection::CreateCrypt(IO_Svc& IO, SSLContextPtr ContextPtr)
		{
			Connection* NewSession = new Connection(IO);

			ConnectionPtr ret(NewSession);

			SSLConnection* conn = new SSLConnection();
			conn->Init(IO, ContextPtr);

			NewSession->m_ConnectionPtr = IConnectionPtr(conn);
										 
			
			return ret;
		}

	

		void Connection::OnRead(ByteBufferPtr BuffPtr, const boost::system::error_code& Error)
		{
			BOOST_LOG_NAMED_SCOPE("Session: OnRead");
			BOOST_LOG_SEV(slg, normal) << " - ";

			try
			{
				if (Error)
				{

					std::stringstream out;
					out << "thread: " << boost::this_thread::get_id() << "l; OnRead: error" << std::endl;

					BOOST_LOG_SEV(slg, error) << Error.message();

					m_pConnectManager->OnError(out.str(), Error, GetID());

				}
				else
				{
					std::cout << "thread: " << boost::this_thread::get_id() << "; OnRead: Ok" << std::endl;
					BuffPtr->SetStart();

					if (m_WorkManagerPtr)
					{
						m_InDeque.Set(BuffPtr);
						m_WorkManagerPtr->Work(boost::bind(&Connection::ProcessMessages, shared_from_this()));
					}
					else
					{
						m_Stub->OnRead(BuffPtr);
					}
				}
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

		void Connection::DoRead()
		{
			m_ConnectionPtr->DoRead(shared_from_this());
		}

		survey::transport::Connection::ConnectionPtr Connection::CreateTcpProxy(IO_Svc& IO)
		{
			ConnectionPtr ret;


			return ret;
		}

	}//!namespace transport

}//!namespace survey
