#include "stdafx.h"
#include "transport_settings.h"

#pragma warning(push)
#pragma warning(disable : 4512)
#include <boost/program_options.hpp>
#pragma warning(pop)
#include <fstream>
#include <boost/filesystem.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;


bool survey::transport::TransportSettings::Load(std::string FileName)
{
	bool ret = false;
//	m_ConnectionType = eSSL;

	po::options_description desc("Allowed options");
	std::string ConnnectionTypestr;

	desc.add_options()
		("transport.connection_type", po::value<std::string>(&ConnnectionTypestr)->default_value("native"), "type of connection")
		("transport.port", po::value<std::string>(&m_port)->default_value("801"), "Port")
		("transport.host", po::value<std::string>(&m_host)->default_value("127.0.0.1"), "Host")
		("transport.reconnect", po::value<bool>(&m_Reconnect)->default_value("true"), "Reconnect")
		;

	std::ifstream ifs(FileName.c_str());

	if (ifs)
	{
		po::variables_map vm;
		po::store(po::parse_config_file(ifs, desc, true), vm);
		po::notify(vm);
		ret = true;
	}

	if (ConnnectionTypestr == "native")
	{
		m_ConnectionType = eNative;
	}
	else if (ConnnectionTypestr == "ssl")
	{
		m_ConnectionType = eSSL;
	}

	return ret;
}
