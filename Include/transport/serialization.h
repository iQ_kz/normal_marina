#pragma once
#include "transport_types.h"
#include <vector>

namespace survey
{
	namespace transport
	{


		void Write(ByteBuffer& Buff, const size_t v);


		void Read(ByteBuffer& Buff, size_t& v);


		void Write(ByteBuffer& Buff, const std::string& v);

		void Read(ByteBuffer& Buff, std::string& v);


		void Write(ByteBuffer& Buff, const ByteBuffer& v);

		void Read(ByteBuffer& Buff, ByteBuffer& v);


		void Write(ByteBuffer& Buff, const time_t v);

		void Read(ByteBuffer& Buff, time_t& v);

		template<typename ItemType>
		void Write(ByteBuffer& Buff, const std::vector<ItemType>& v)
		{
			size_t len = v.size();
			Write(Buff,len);
			for (auto c : v)
			{
				Write(Buff,c);
			}
		}

		template<typename ItemType>
		void Read(ByteBuffer& Buff,  std::vector<ItemType>& v)
		{
			size_t len = 0;
			Read(Buff,len);
			v.resize(len);
			for (auto& c : v)
			{
				Read(Buff,c);
			}
		}


	}//!namespace transport

}//!namespace survey
