#include "stdafx.h"
#include "transport_core.h"
#include <sstream>


namespace survey
{
	namespace transport
	{

		void TCP_Connector::Connect(const std::string& host, const std::string& Port)
		{
			BOOST_LOG_NAMED_SCOPE("TCP_Connector: Connect");
			try
			{
				tcp::resolver resolver(IO_Handler::m_IO);
				tcp::resolver::query query(tcp::v4(), host, Port);
				tcp::resolver::iterator iterator = resolver.resolve(query);
				tcp::endpoint EP = *iterator;
				m_SessPtr = m_ConnectManager->CreateSession();

				//*# ������������� ���������� ���������� �������� ��� ��������.
				//m_Deadline.expires_from_now(boost::posix_time::seconds(60));
				//m_Deadline.async_wait(boost::bind(&TCP_Connector::CheckDeadline, this));

				m_SessPtr->Socket().async_connect(EP, boost::bind(&TCP_Connector::OnConnect, this, _1));
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

		void TCP_Connector::Stop()
		{
			m_Deadline.cancel();
			//m_SockerPtr->close();
		}

		void TCP_Connector::OnConnect(const boost::system::error_code& Error)
		{
			BOOST_LOG_NAMED_SCOPE("TCP_Connector: OnConnect");
			try
			{
				if (Error)
				{
					std::cout << Error.message() << std::endl;
					m_ConnectManager->OnError(Error.message(), Error, 0);
				}
				else
				{

					m_ConnectManager->OnConnect(m_SessPtr);
				}
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}


		void TCP_Acceptor::Start(unsigned short Port)
		{
			BOOST_LOG_NAMED_SCOPE("TCP_Acceptor: Start");

			BOOST_LOG_SEV(slg, notification) << " Port = " << Port;
			try
			{
				tcp::endpoint Endpoint(tcp::v4(), Port);
				m_Acceptor = boost::make_shared<tcp::acceptor>(IO_Handler::m_IO, tcp::endpoint(tcp::v4(), Port));

				StartAccept();
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

		void TCP_Acceptor::StartAccept()
		{
			ConnectionPtr SessPtr = m_ConnectManager->CreateSession();
			m_Acceptor->async_accept(SessPtr->Socket(), boost::bind(&TCP_Acceptor::OnAccept, this, SessPtr, boost::asio::placeholders::error));
		}

		void TCP_Acceptor::OnAccept(ConnectionPtr SPtr, const boost::system::error_code& Error)
		{
			BOOST_LOG_NAMED_SCOPE("TCP_Connector: OnConnect");
			try
			{
				if (Error)
				{
					m_ConnectManager->OnError("TCP_Acceptor::OnAccept", Error, 0);
				}
				else
				{
					m_ConnectManager->OnConnect(SPtr);
				}
				StartAccept();
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

	}//!namespace transport

}//!namespace survey
