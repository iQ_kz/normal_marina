#pragma once

#include "transport_types.h"
#include "Connection.h"
#include "../include/logger.h"





namespace survey
{
	namespace transport
	{
		struct IConnectManager;

		class ThreadManager
		{
		public:
			template <typename Func>
			void AddThread(Func f)
			{
				m_ThreadContainer.create_thread(f);
			}

			boost::thread_group	  m_ThreadContainer;
			typedef boost::thread* ThreadPtr;
			
		};
		typedef boost::shared_ptr <ThreadManager> ThreadManagerPtr;
	


		/*# �������� �� ����������� �������� ����������� � �������.
		*
		*/
		class TCP_Connector : public IO_Handler
		{
		public:
			TCP_Connector(IO_Svc& IO) : IO_Handler(IO), m_ConnectManager(nullptr), m_Deadline(IO){}

			typedef  boost::function<void(SocketPtr Ptr, const boost::system::error_code& error)>	 HandlerType;


			void Connect(const std::string&  host, const std::string&   Port);

			void Stop();

			void SetHandler(IConnectManager*  Handler)
			{
				m_ConnectManager = Handler;
			}

			void CheckDeadline()
			{
				Stop();
			}

		private:
			void OnConnect(const boost::system::error_code& Error);

			IConnectManager*   m_ConnectManager;
			deadline_timer m_Deadline;
			ConnectionPtr m_SessPtr;
			src::severity_logger< SeverityLevel > slg;
		};


		class TCP_Acceptor : public  IO_Handler
		{
		public:
			TCP_Acceptor(IO_Svc& IO) :IO_Handler(IO), m_ConnectManager(nullptr){}

			//typedef boost::function<void(SocketPtr SPtr, const boost::system::error_code& Error)>  HandlerType;

			void Start(unsigned short Port);

			void StartAccept();

			void Stop()
			{
				BOOST_LOG_SEV(slg, notification) << "TCP_Acceptor: Stop";
				m_Acceptor->close();
			}

			void SetHandler(IConnectManager* Handler)
			{
				m_ConnectManager = Handler;
			}

		private:
			void OnAccept(ConnectionPtr SPtr, const boost::system::error_code& Error);

			boost::shared_ptr <tcp::acceptor> m_Acceptor;
			IConnectManager*   m_ConnectManager;
			src::severity_logger< SeverityLevel > slg;
		};


		template<typename HandlerType>
		class StubBase : public IStub
		{
		public:
			typedef HandlerType HandlerType;
			~StubBase()
			{
				Disconnect();
			}

			void Init(ConnectionPtr SPtr)
			{
				BOOST_LOG_NAMED_SCOPE("StubBase: Init");
				try
				{
					m_SessionPtr = SPtr;
					m_Handler.Init(SPtr);
					m_SessionPtr->SetStub(this);
				}
				catch (std::exception& e)
				{
					BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
					throw;
				}
				catch (...)
				{
					BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
					throw;
				}
			}

			void Disconnect()
			{ 
				m_Handler.Logout();
				if (m_SessionPtr)
				{
					m_SessionPtr->Disconnect();
				}
				
			}

			ID GetID()
			{
				return m_SessionPtr->GetID();
			}

			HandlerType& Handler(){ return m_Handler; }
		protected:
			HandlerType m_Handler;
			ConnectionPtr m_SessionPtr;
			src::severity_logger< SeverityLevel > slg;
		};

		template <typename Worker>
		class IOWorker
		{
		public:

			typedef IOWorker<Worker> ThisType;
			typedef Worker Worker;
			typedef boost::shared_ptr <Worker> WorkerPtr;
			typedef boost::shared_ptr <IO_Svc::work  > SvcWorkPtr;
			IOWorker()
			{
				m_Svc = boost::make_shared <IO_Svc>();
				m_WorkerPtr = boost::make_shared <Worker>(*m_Svc);
				m_SvcWorkPtr = boost::make_shared<IO_Svc::work>(*m_Svc);
			}

			void Init(ThreadManagerPtr& ThreadManagerObj, size_t ThreadCount)
			{
				BOOST_LOG_NAMED_SCOPE("IOWorker: Init");
				try
				{
					for (size_t i = 0; i < ThreadCount; ++i)
					{
						ThreadManagerObj->AddThread(boost::bind(&IO_Svc::run, m_Svc));
					}
				}
				catch (std::exception& e)
				{
					BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
					throw;
				}
				catch (...)
				{
					BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
					throw;
				}
			}

			WorkerPtr m_WorkerPtr;
		private:

			SvcWorkPtr m_SvcWorkPtr;
			IO_SvcPtr m_Svc;
			src::severity_logger< SeverityLevel > slg;
		};


	}//!namespace transport

}//!namespace survey
