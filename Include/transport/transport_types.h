#pragma once

#include <vector>
#include <boost/shared_ptr.hpp>
#include <deque>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>

#include <boost/timer.hpp>
#include <boost/chrono.hpp>
#include <boost/make_shared.hpp>
#include <boost/function.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/asio/ssl.hpp>




#include "../../Include/ID_Genearator.h"
#include "../../Include/transport_client_interface.h"


namespace survey
{
	namespace transport
	{

		typedef unsigned char Byte;

		using boost::asio::deadline_timer;

		using boost::asio::ip::tcp;

		namespace asio = boost::asio;

		typedef boost::asio::io_service IO_Svc;

		typedef boost::shared_ptr <IO_Svc> IO_SvcPtr;

		typedef boost::shared_ptr <tcp::socket> SocketPtr;

		typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> SSLSocket;

		typedef boost::shared_ptr <SSLSocket> SSLSocketPtr;

		typedef boost::asio::ssl::context SSLContext;

		typedef boost::shared_ptr <SSLContext> SSLContextPtr;
		

	 /*
		struct IClient2Serv
		{
			virtual void Login(const std::string& login, const std::string& password, ID RequestID) = 0;

		};

		struct IServ2Client
		{
			virtual void OnLogin(const Result& res, ID RequestID) = 0;
		};*/


		class IO_Handler : public boost::noncopyable
		{
		public:
			IO_Handler(IO_Svc& IO) :m_IO(IO){}

		protected:
			IO_Svc& m_IO;
		};

		class ByteBuffer
		{
			typedef std::vector <Byte> ByteStorage;
			ByteBuffer() :m_Size(0), m_Pos(NULL){}


		public:
			ByteBuffer(const  char* buff, size_t len) :m_ByteStorage(buff, buff + len), m_Size(len){}
			typedef boost::shared_ptr <ByteBuffer>	ByteBufferPtr;
			static 	 ByteBufferPtr Create()
			{ //�������������� ������� � ���� �������.
				return ByteBufferPtr(new ByteBuffer);
			}

			void Append(const Byte* SrcBuff, size_t Len)
			{
				if (Len)
				{
					m_Size += Len;
					m_ByteStorage.resize(m_Size);
					Byte* Buff = &m_ByteStorage[m_Pos];
					memcpy(Buff, SrcBuff, Len);
					m_Pos += Len;
				}
			}

			void GetFront(Byte* DestBuff, size_t Len)
			{
				if (Len)
				{
					Byte* SrcBuff = &m_ByteStorage[m_Pos];
					memcpy(DestBuff, SrcBuff, Len);
					m_Pos += Len;

				}
			}

			Byte*CurrenData(){ return &m_ByteStorage[m_Pos]; }

			const Byte* Data() const
			{
				const Byte* ret = NULL;
				if (m_Size)
				{
					ret = (const Byte*)&m_ByteStorage[0];
				}
				return ret;
			}

			Byte* Data()
			{
				Byte* ret = NULL;
				if (m_Size)
				{
					ret = &m_ByteStorage[0];
				}
				return ret;
			}

			size_t GetSize() const
			{
				return m_ByteStorage.size();
			}

			void Resize(size_t Size)
			{
				m_ByteStorage.resize(Size);
				m_Size = Size;
			}

			void SetStart()
			{
				m_Pos = 0;
			}

		private:

			ByteStorage m_ByteStorage;
			size_t m_Size;
			size_t m_Pos;
		};

		typedef ByteBuffer::ByteBufferPtr	ByteBufferPtr;



		struct IStub
		{
			virtual void OnRead(ByteBufferPtr BuffPtr) = 0;
			virtual ~IStub(){}
		};

		typedef boost::shared_ptr <IStub> StubPtr;


		class Connection;
		typedef boost::shared_ptr <Connection> ConnectionPtr;


		typedef ID SessionId;

		struct Session
		{
			Session(SessionId id, ConnectionPtr conn) : Id(id), Connn(conn){}
			SessionId Id;
			ConnectionPtr Connn;
		};

		typedef boost::shared_ptr <Session> SessionPtr;

		struct IConnectManager
		{
			virtual void OnConnect(ConnectionPtr SPtr) = 0;
			virtual void OnDisconnect(ConnectionPtr SPtr) = 0;
			virtual void OnError(const std::string msg, const boost::system::error_code& Error, ID Id) = 0;
			virtual ConnectionPtr CreateSession() = 0;
		};


		class WorkManager : public  IO_Handler
		{
		public:
			WorkManager(IO_Svc& IO) :IO_Handler(IO){}
			typedef boost::function<void()> TaskHandler;
			void Work(TaskHandler Work)
			{
				IO_Handler::m_IO.post(Work);
			}

		private:

		};

		typedef boost::shared_ptr <WorkManager>	WorkManagerPtr;


		class MessageDeque
		{
			typedef std::deque <ByteBufferPtr> MessageStorage;
		public:
			typedef boost::mutex Lock;
			typedef boost::lock_guard <Lock> Guard;

			void Set(ByteBufferPtr Msg)
			{
				Guard  g(m_Lock);
				m_MessageStorage.push_back(Msg);
			}

			ByteBufferPtr Get()
			{
				ByteBufferPtr ret;

				Guard  g(m_Lock);

				if (!m_MessageStorage.empty())
				{
					ret = m_MessageStorage.front();
					m_MessageStorage.pop_front();
				}
				return ret;
			}

			void Clear()
			{
				Guard  g(m_Lock);
				m_MessageStorage.clear();
			}

		private:
			MessageStorage m_MessageStorage;
			Lock m_Lock;
		};




		class CryprContextManager
		{
		public:
			CryprContextManager()// :m_Context(boost::asio::ssl::context::sslv23)
			{
				m_Context = boost::make_shared<SSLContext>(boost::asio::ssl::context::sslv23);
			}


			SSLContextPtr Context(){ return m_Context; }

		protected:
			SSLContextPtr m_Context;
			
		};


		struct  ResourceInfo
		{
			std::string Hash;
			std::string Name;
			ResourceInfo(){}
			ResourceInfo(const ResourceInfo& obj){ operator =(obj); }
			ResourceInfo& operator =(const ResourceInfo& obj)
			{
				Hash = obj.Hash;
				Name = obj.Name;
				return*this;
			}
		};

		typedef std::vector <ResourceInfo> ResourceInfoV;


		struct ResourceData
		{
			ResourceInfo Resource;
			
			ProjectId Id;
			ResourceData() :Id(0){}
			ResourceData(const ResourceData& obj)
			{
				operator =(obj);
			}

			ResourceData& operator=(const ResourceData& obj)
			{
				Resource = obj.Resource;
				Id = obj.Id;
				return *this;
			}
		};




	}//!namespace transport

}//!namespace survey
