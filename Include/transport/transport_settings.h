#pragma once
//#include <fstream>
//#include <boost/filesystem.hpp>
#include "../common_types.h"
#include <string>


namespace survey
{
	namespace transport
	{

		class TransportSettings
		{
		public:
			TransportSettings() :m_ConnectionType(eNative), m_port("801"), m_Reconnect(true){}
			bool Load(std::string FileName);


			EConnectionType GetConnectionType()const { return m_ConnectionType; }
			void SetConnectionType(EConnectionType ct){ m_ConnectionType = ct; }
			const std::string GetPort()const { return m_port; }
			void SetPort(std::string port){ m_port = port; }
			bool GetReconnect()const { return m_Reconnect; }
			void SetReconnect(bool reconnect){ m_Reconnect = reconnect; }

			const std::string& GetHost()const { return m_host; }

			void SetHost(const std::string& host){ m_host = host; }



		private:
			EConnectionType m_ConnectionType;
			std::string m_port;
			std::string m_host;
			bool m_Reconnect;
		};



	}//!namespace transport

}//!namespace survey

