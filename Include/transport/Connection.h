#pragma once

#include "transport_types.h"
//#include "i_connection_impl.h"	

#include "../logger.h"

namespace survey
{
	namespace transport
	{

		/*#
		*/
		class Connection : public IO_Handler, public boost::enable_shared_from_this<Connection>
		{

		public:
			typedef boost::unique_lock<boost::mutex> Lock;
			typedef boost::function<void(ConnectionPtr SPtr)> WillignessHandlerType;


			

			Connection(IO_Svc& IO) : IO_Handler(IO), m_ID(0), m_pConnectManager(nullptr){}

			~Connection()
			{
			}



			struct IConnectionImpl
			{
				virtual ~IConnectionImpl(){}

				virtual void Send(ByteBufferPtr BuffPtr, ConnectionPtr pMaster) = 0;

				virtual void DoRead(ConnectionPtr pMaster) = 0;
				virtual tcp::socket& Socket() = 0;

				virtual void Close() = 0;

				virtual void ServerStart( ConnectionPtr pMaster) = 0;

				virtual void ClientStart( ConnectionPtr pMaster) = 0;

				virtual void SetWillignessHandler(WillignessHandlerType Handler) = 0;
			};

			typedef boost::shared_ptr <IConnectionImpl>  IConnectionPtr;


			typedef boost::shared_ptr<Connection> ConnectionPtr;


			static ConnectionPtr CreateTcpProxy(IO_Svc& IO);

			static ConnectionPtr Create(IO_Svc& IO);

			static ConnectionPtr CreateCrypt(IO_Svc& IO, SSLContextPtr ContextPtr);

			void Send(ByteBufferPtr BuffPtr)
			{
				m_ConnectionPtr->Send(BuffPtr, shared_from_this());

			}

			void DoRead();

			void OnRead(ByteBufferPtr BuffPtr, const boost::system::error_code& Error);

			ID GetID()
			{
				return m_ID;
			}

			void SetID(ID Id)
			{
				m_ID = Id;
			}

			void SetStub(IStub* Stub)
			{
				m_Stub = StubPtr(Stub);
			}

			void SetEventHandler(IConnectManager* pEventHandler)
			{
				m_pConnectManager = pEventHandler;
			}

			StubPtr GetStub()
			{
				Lock lock(m_mutex);
				return m_Stub;
			}

			void Disconnect()
			{
				
				m_ConnectionPtr->Close();
				m_pConnectManager ->OnDisconnect(shared_from_this());
				//m_ConnectionPtr.reset();
				Lock lock(m_mutex);
				m_Stub.reset();
			//	m_InDeque.Clear();
				//m_WorkManagerPtr.reset();
			}

			void ProcessMessages()
			{
				ByteBufferPtr Buff = m_InDeque.Get();
				if (Buff)
				{
					StubPtr stub;

					{
						Lock lock(m_mutex);
						stub = m_Stub;
					}

					if (stub)
					{
						stub->OnRead(Buff);
					}
				}
			}

			void SetWorkManager(WorkManagerPtr WorkManagerObj)
			{
				m_WorkManagerPtr = WorkManagerObj;
			}

			tcp::socket& Socket()
			{
				return m_ConnectionPtr->Socket();
			}

			void OnError(const std::string& str, const boost::system::error_code& Error)
			{
				m_pConnectManager->OnError(str, Error, GetID());
			}


			virtual void ServerStart(){ m_ConnectionPtr->ServerStart(shared_from_this()); }

			virtual void ClientStart(){ m_ConnectionPtr->ClientStart(shared_from_this()); }


			void SetWillignessHandler(WillignessHandlerType Handler){ m_ConnectionPtr->SetWillignessHandler(Handler); }
			 
		private:
			//	

			EConnectionType m_EConnectionType;

			IConnectionPtr m_ConnectionPtr;
			//SocketPtr m_SocketPtr;
			ID m_ID;
			StubPtr m_Stub;
			IConnectManager* m_pConnectManager;
			MessageDeque m_InDeque;
			WorkManagerPtr m_WorkManagerPtr;
			src::severity_logger< SeverityLevel > slg;
			boost::mutex m_mutex;
			
		};

		


	}//!namespace transport

}//!namespace wsv
