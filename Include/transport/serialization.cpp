#include "stdafx.h"
#include "serialization.h"


namespace survey
{
	namespace transport
	{

		void Write(ByteBuffer& Buff, const size_t v)
		{
			Buff.Append((Byte*)&v, sizeof(v));
		}

		void Write(ByteBuffer& Buff, const std::string& v)
		{
			size_t Len = v.size();
			Write(Buff, Len);

			if (Len)
			{
				Buff.Append((Byte*)&v.front(), Len);
			}
		}

		void Write(ByteBuffer& Buff, const ByteBuffer& v)
		{
			Write(Buff, v.GetSize());
			Buff.Append(v.Data(), v.GetSize());
		}

		void Write(ByteBuffer& Buff, const time_t v)
		{
			Buff.Append((Byte*)&v, sizeof(v));
		}

		void Read(ByteBuffer& Buff, size_t& v)
		{
			Buff.GetFront((Byte*)&v, sizeof(v));
		}

		void Read(ByteBuffer& Buff, std::string& v)
		{
			size_t Len = 0;
			Read(Buff, Len);
			v.resize(Len);
			size_t l = v.size();
			Buff.GetFront((Byte*)&v[0], Len);
		}

		void Read(ByteBuffer& Buff, ByteBuffer& v)
		{ 
			size_t Len = 0;
			Read(Buff, Len);
			v.Resize(Len);
			if (Len)
			{
				Buff.GetFront(v.CurrenData(), Len);
			}
		}

		void Read(ByteBuffer& Buff, time_t& v)
		{
			Buff.GetFront((Byte*)&v, sizeof(v));
		}

	}//!namespace transport

}//!namespace survey
