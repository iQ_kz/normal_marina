#pragma once
#include "SurveyCore.h"


namespace survey {
	namespace math {


		struct SURVEYCORE_API Matrix2
		{
			Matrix2();

			Matrix2(double v_11, double v_12, double v_13
				, double v_21, double v_22, double v_23
				, double v_31, double v_32, double v_33
				);

			// operators
			double* operator [] (unsigned int row);
			const double* operator [] (unsigned int row) const;
			double operator () (unsigned int row, unsigned int col) const;

			Matrix2 operator * (double vl) const;
			Matrix2& operator *= (double vl);
			Matrix2 operator / (double vl) const;
			Matrix2& operator /= (double vl);

			Matrix2 operator + (const Matrix2& mtr) const;
			Matrix2& operator += (const Matrix2& mtr);
			Matrix2 operator - (const Matrix2& mtr) const;
			Matrix2& operator -= (const Matrix2& mtr);

			Matrix2 operator * (const Matrix2& mtr) const;
			Matrix2& operator *= (const Matrix2& mtr);

			// methods

			// fields
			union {
				struct {
					double  V_11, V_12, V_13;
					double  V_21, V_22, V_23;
					double  V_31, V_32, V_33;
				};

				double VLS[3][3];
			};

		};

	}
}