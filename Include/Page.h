#pragma once
#include "DocumentObject.h"
#include "Block.h"


namespace survey {


	class SURVEYCORE_API Page : public DocumentObject, public virtual IRangedNotifycatorSupport
	{
	public:

		Page(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord, const ColorDat& color, const CoordDat& coord, const std::string& name = "")
			: DocumentObject(pDoc, bsColor, bsCoord, color, coord, name)
		{
			m_RangedNotifycators.emplace(this);
		}

		Page(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: DocumentObject(pTiElement, childs, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		// ===================  XML ======================================

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{
			if (pXmlObject->GetType() != Block::Type() && pXmlObject->GetType() != Header::Type())
				throw std::exception("Invalid type of the added object");

			DocumentObject::AddChild(pXmlObject, pAfterXmlObject);
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}


		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = DocumentObject::SaveToXmlElement();
			// ....
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}


		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("page"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("page"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			// ....
		}

		void afterTreeUpdate()
		{
			setRangedNotifycators(std::unordered_set<IRangedNotifycatorSupport*> { this });
		}

	protected:

		Page(const Page& obj) : DocumentObject(obj) { }
		Page& operator = (const Page& obj) { return *this; }

		// fields

		// friends
		friend class DocumentObject;
	};


}