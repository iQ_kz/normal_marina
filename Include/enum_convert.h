#pragma once 
#include <string>
#include <stdexcept>
#include "common_types.h"

namespace  survey
{

	class EnumConvert
	{
	public:
		static std::string ConnectionType2String(EConnectionType ConnType)
		{
			static const char* Names[] = { "native", "ssl" };
			if (ConnType < 0 || ConnType >= UnknownConnType)
			{
				throw std::range_error("Wrong connection type");
			}
			return  	std::string(Names[ConnType]);
		}


	};

}//!namespace  survey