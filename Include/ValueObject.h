#pragma once
#include "SurveyObject.h"

namespace survey {
	class SURVEYCORE_API ValueObject : public IXMLObject
	{
	public:

		// =====================================================================================================
		ValueObject(Document* pDoc, const std::string& condition, const float& value)
			: IXMLObject(pDoc)
			, m_Condition(condition)
			, m_Value(value)
		{ }

		ValueObject(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(pTiElement, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}


		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);

			// clear data
			m_Condition.clear();

			loadElement(pTiElement);
		}

		// =====================================================================================================

		std::string GetCondition() const { return m_Condition; }

		void SetCondition(const std::string& str)
		{
			m_Condition = str;
		}
		void SetValue(const float& val)
		{
			m_Value = val;
		}
		// ============================ XML ======================================================================

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();
			pXmlelement->SetAttribute("if", m_Condition);
			pXmlelement->SetAttribute("set", m_Value);

			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("value"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("value"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			if (const char* pVl = pTiElement->Attribute("if"))
				m_Condition = pVl;
			if (const char* pVl = pTiElement->Attribute("set"))
				m_Value = atof(pVl);
		}

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		ValueObject(const ValueObject& obj) : IXMLObject(obj) { }
		ValueObject& operator = (const ValueObject& obj) { return *this; }

		// fields

		std::string m_Condition;
		float m_Value;
	};
}