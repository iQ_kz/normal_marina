#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "Section.h"


namespace survey {

	// predefinitions
	class SURVEYCORE_API Images;


	class SURVEYCORE_API Image : public IXMLObject, public INotifycatorSupport
	{
	public:

		Image(const boost::filesystem::path& pathFile, Document* pDoc);

		Image(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		const std::string& GetID() const
		{
			return m_ID;
		}

		const std::string& GetName() const
		{
			return m_ImageName;
		}

		void SetName(const std::string& name)
		{
			m_ImageName = name;

			EventArg arg("Name");
			InvokeChange(this, &arg);
		}

		bool ImageData(std::vector<unsigned char>& fData) const;

		bool ImageDataAsPNG(std::vector<unsigned char>& fData) const;

		bool PreviewImageData(std::vector<unsigned char>& fData) const;

		boost::filesystem::path GetImagePath() const;

		// ==================================== XML =============================================

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlElement = IXMLObject::SaveToXmlElement();
			
			pXmlElement->SetAttribute("id", m_ID);
			pXmlElement->SetAttribute("ssh", m_SSH);
			pXmlElement->SetAttribute("name", m_ImageName);
			return pXmlElement;
		}

		// ==================================== Types ===========================================

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("image"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("image"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			const char* attr = pTiElement->Attribute("id");
			if (!attr)
				throw std::exception("Invalid XML");

			m_ID = attr;

			attr = pTiElement->Attribute("ssh");
			if (!attr)
				throw std::exception("Invalid XML");

			m_SSH = attr;

			attr = pTiElement->Attribute("name");
			if (!attr)
				throw std::exception("Invalid XML");

			m_ImageName = attr;
		}

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		std::string  m_ID;
		std::string  m_SSH;
		std::string  m_ImageName;

		// friends
		friend class Images;	
	};


	class SURVEYCORE_API Images : public Section
	{
	public:

		Images(Document* pDoc)
			: Section(pDoc)
		{ }

		Images(const TiXmlElement* pTiElement, Document* pDoc)
			: Section(pTiElement, pDoc)
		{
			updateFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		Image* AddImage(const boost::filesystem::path& pathFile)
		{
			try
			{
				Image* pImage = new Image(pathFile, m_pDocument);	
				AddChild(pImage);
				m_Images.emplace(pImage->m_ID, pImage);

				// notify
				CollectionEventArg evArg("images", CollectionEventType::CET_PushBack, pImage);
				InvokeChange(this, &evArg);
				return pImage;
			}
			catch (...)
			{
				// to do log error
			}

			return NULL;
		}

		void RemoveImage(Image* pImage)
		{
			if (!pImage)
				return;

			m_Images.erase(pImage->m_ID);

			// notify
			CollectionEventArg evArg("images", CollectionEventType::CET_Erase, pImage);
			InvokeChange(this, &evArg);

			pImage->KillHimself();
		}

		void RemoveImages(const std::set<Image*>& images)
		{
			std::list<IXMLObject*> xmlObjs;

			for (Image* pImage : images)
			{
				assert(pImage);

				xmlObjs.push_back(pImage);
				pImage->KillHimself();
			}

			// notify
			CollectionEventArg evArg("images", CollectionEventType::CET_EraseRange, xmlObjs);
			InvokeChange(this, &evArg);
		}

		const std::unordered_map<std::string, Image*>&  GetImages() const { return m_Images; }

		// ==================================== XML =============================================

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			Section::UpdateFromXmlElement(pTiElement);
			updateFromXmlElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = Section::SaveToXmlElement();
			// ...
			return pXmlelement;
		}

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			m_Images.clear();
			afterTreeUpdate();

			// notify
			CollectionEventArg evArg("images", CollectionEventType::CET_Reset);
			InvokeChange(this, &evArg);
		}

		// ==================================== Types ===========================================

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("images"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("images"); }

	private:

		void updateFromXmlElement(const TiXmlElement* pTiElement)
		{
			//...
		}

		void afterTreeUpdate()
		{
			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				Image* pImage = dynamic_cast<Image*>(pObject);
				if (pImage)
					m_Images.emplace(pImage->m_ID, pImage);

				pObject = pObject->NextBrother();
			}
		}

	protected:

		Images(const Images& obj) : Section(obj) { }
		Images& operator = (const Images& obj) { return *this; }

		// fields
		std::unordered_map<std::string, Image*>  m_Images;
	};

}


