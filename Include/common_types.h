#pragma once

#include <string>
#include <list>
#include <unordered_map>

#include <boost/filesystem.hpp>




namespace  survey
{

	typedef unsigned int ID;

	/*# ������������� ���������� ������. 
	*	��� ���������, ����� �� ������� ������� ���� �� �����. ��������, ����� �������������� � �������. 
	*/ 
	typedef unsigned int ClientToken;

	/*# ��������� ���������� ������������� �������.
	*/
	typedef ID ProjectId;





	enum EConnectionType
	{
		eNative = 0, 
		eSSL = 1, 
		UnknownConnType = 2 
	};


	typedef std::string LocalID;
	typedef ProjectId GlobalID;

	class UploadProjID
	{
	public:
		UploadProjID(const LocalID& ClientID) :m_ServId(0), m_ClientID(ClientID){}
		UploadProjID(GlobalID ServId) : m_ServId(ServId){}

		bool IsServerID(){ return 0 != m_ServId; }

		const LocalID& ClientID(){ return m_ClientID; }
		GlobalID ServerID(){ return m_ServId; }

	private:
		GlobalID m_ServId;
		LocalID m_ClientID;
		UploadProjID() :m_ServId(0){}
	};


	// --------------------------- By commits -----------------------------------------------------------------

	struct CommitProjectData
	{
		// xml to string (utf-8)	
		std::string m_ProjectInfo;

		// name block -> xml block path
		std::unordered_map<std::string, boost::filesystem::path>  m_Blocks;

		// name block -> binary file path
		std::unordered_map<std::string, boost::filesystem::path>  m_Files;
	};


	struct ConfirmInfo
	{
		std::string  m_ProjectConfirmGuid;

		// name block -> confirm hash
		std::unordered_map<std::string, std::string>  m_Blocks;

		// name file -> is confirm
		std::unordered_map<std::string, bool>  m_Files;
	};


	struct SourceDescription
	{
		std::string  m_Name;  // utf8
		bool m_IsConfirm;
	};


	struct CommitBinaryInfo
	{
		std::string  m_Hash;
		SourceDescription  m_SrcDescription;
		boost::filesystem::path  m_FilePath;
	};



	struct SourceImageDescription
	{
		std::string  m_Name;  // utf8
		bool  m_IsConfirm;
	};


	struct CommitImagesInfo
	{
		SourceImageDescription   m_SrcDescription;
		boost::filesystem::path  m_ImagePath;
	};


	struct CommitData
	{
		// guid -> CommitProjectData
		std::unordered_map<std::string, CommitProjectData>  m_ProjectCommits;
		std::list<CommitBinaryInfo>  m_BinnaryCommit;
		std::list<CommitImagesInfo>  m_ImagesCommit;
	};


	struct CommitConfirm
	{
		std::list<ConfirmInfo>  m_ProjectConfirms;
		std::list<std::string>  m_BinaryInfoHashs;
		std::list<std::string>  m_ImageInfoHashs;
	};


	// ---------------------------------------------------------------------------------------------------------

};// !namespace  survey													  