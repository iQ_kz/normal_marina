#pragma once
#include "SurveyCore.h"
#include "SurveyType.h"


namespace survey {


	class SURVEYCORE_API IXMLObject : public ISurveyObject
	{
	protected:

		IXMLObject(Document* pDocument)
			: m_pParent(NULL)
			, m_pChildFirst(NULL)
			, m_pChildLast(NULL)
			, m_pPreviosBrother(NULL)
			, m_pNextBrother(NULL)
			, m_pDocument(pDocument)
		{
			assert(pDocument);
		}

		IXMLObject(const std::list<IXMLObject*>& childs, Document* pDocument)
			: m_pParent(NULL)
			, m_pChildFirst(NULL)
			, m_pChildLast(NULL)
			, m_pPreviosBrother(NULL)
			, m_pNextBrother(NULL)
			, m_pDocument(pDocument)
		{
			assert(pDocument);
			setChilds(childs);
		}

		IXMLObject(const TiXmlElement* pTiElement, Document* pDocument)
			: m_pParent(NULL)
			, m_pChildFirst(NULL)
			, m_pChildLast(NULL)
			, m_pPreviosBrother(NULL)
			, m_pNextBrother(NULL)
			, m_pDocument(pDocument)
		{
			updateFromXmlElement(pTiElement);
			buildChildTree(pTiElement);
		}

	public:

		virtual ~IXMLObject()
		{
			m_pDocument = NULL;
			m_pParent = m_pChildFirst = m_pChildLast = m_pPreviosBrother = m_pNextBrother = NULL;
		}

		Document* GetDocument() const { return m_pDocument; }

		// xml support
		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement);
		virtual TiXmlElement* SaveToXmlElement() const;

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement);
		TiXmlElement* SaveTreeToXmlElement() const;

		// struct tree support
		IXMLObject* Parent() const { return m_pParent; }
		IXMLObject* ChildFirst() const { return m_pChildFirst; }
		IXMLObject* ChildLast() const { return m_pChildLast; }
		IXMLObject* PreviosBrother() const { return m_pPreviosBrother; }
		IXMLObject* NextBrother() const { return m_pNextBrother; }

		std::list<IXMLObject*> Childs() const;
		std::list<IXMLObject*> ChildsTree() const;

		template<class T>
		std::list<T*> Childs() const
		{
			const std::list<IXMLObject*>& childs = Childs();

			std::list<T*> resLst;

			for (IXMLObject* pXmlObject : childs)
				if (T* pTypedObject = dynamic_cast<T*>(pXmlObject))
					resLst.push_back(pTypedObject);

			return resLst;
		}

		template<class T>
		std::list<T*> ChildsTree() const
		{
			const std::list<IXMLObject*>& childs = ChildsTree();

			std::list<T*> resLst;

			for (IXMLObject* pXmlObject : childs)
				if(T* pTypedObject = dynamic_cast<T*>(pXmlObject))
					resLst.push_back(pTypedObject);

			return resLst;
		}

		virtual void RemoveChild(IXMLObject* pXmlObject);
		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL);

		// killers !!!
		virtual void KillHimself();

		// fabrique of all elements of document
		static IXMLObject* CreateXMLObject(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);

	private:

		void updateFromXmlElement(const TiXmlElement* pTiElement);
		void addChild(IXMLObject* pXmlObject);
		void buildChildTree(const TiXmlElement* pTiElement);
		void setChilds(const std::list<IXMLObject*>& childs);

	protected:

		IXMLObject(const IXMLObject& obj) { }
		IXMLObject& operator = (const IXMLObject& obj) { return *this; }

		void releaseChildTree();

		// fields
		IXMLObject*  m_pParent;
		IXMLObject*  m_pChildFirst;
		IXMLObject*  m_pChildLast;
		IXMLObject*  m_pPreviosBrother;
		IXMLObject*  m_pNextBrother;

		Document*    m_pDocument;
	};

}