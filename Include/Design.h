#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "MathSv.h"
#include "NotifycatorSupport.h"
#include "Section.h"
#include "CommonSurveyTypes.h"
#include "DesignTree.h"


namespace survey {

	// predefinition
	class SURVEYCORE_API DocumentObject;
	class SURVEYCORE_API Design;


	class SURVEYCORE_API DesignElement : public IXMLObject, public INotifycatorSupport
	{
	public:

		DesignElement(ID id, const CoordDat& coord, const ColorDat& color, Document* pDoc)
			: IXMLObject(pDoc)
			, m_ID(id)
			, m_pCoordinate(NULL)
			, m_pColor(NULL)
		{
			AddChild(m_pCoordinate = new Coordinate(coord, pDoc));
			AddChild(m_pColor = new Color(color, pDoc));

			m_WCS = coord.ToMatrix();
		}

		DesignElement(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(childs, pDoc)
			, m_pCoordinate(NULL)
			, m_pColor(NULL)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		// ==================  Design tree position ========================================

		DocumentObject* GetOwner() const;

		DesignElement* GetParentDesignElement() const;

		void SetTop() { }
		void SetLow() { }

		void SetAllTop() { }
		void SetAllLow() { }

		void Up() { }
		void Down() { }

		// ==================  Color ============================================

		ColorDat GetColor() const 
		{
			assert(m_pColor);
			return m_pColor->GetColor();
		}

		void SetColor(const ColorDat& clr)
		{
			assert(m_pColor);
			m_pColor->SetColor(clr);

			EventArg arg("Color");
			InvokeChange(this, &arg);
		}

		// =============== Coordinate =============================================

		CoordDat GetCoordinate() const
		{
			assert(m_pCoordinate);
			return m_pCoordinate->GetCoord();
		}

		CoordDat GetAbsoluteCoordinate() const;

		math::Matrix2 GetWCS() const;

		void SetCoordinate(const CoordDat& crd);

		void SetAbsoluteCoordinate(const CoordDat& crd);

		void UpdateCoordinate();

		// =======================================================================

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();			
			pXmlelement->SetAttribute("id", m_ID);
			return pXmlelement;
		}

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			
			m_pColor = NULL;
			m_pCoordinate = NULL;

			afterTreeUpdate();
		}

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("design_el"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("design_el"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			const char* attr = pTiElement->Attribute("id");
			if (!attr)
				throw std::exception("Invalid XML");

			m_ID = boost::lexical_cast<ID>(attr);
		}

		void afterTreeUpdate()
		{
			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				if (Color* pColor = SurveyType::Cast<Color>(pObject))
					m_pColor = pColor;
				else if (Coordinate* pCoord = SurveyType::Cast<Coordinate>(pObject))
					m_pCoordinate = pCoord;

				pObject = pObject->NextBrother();
			}

			if(!m_pColor || !m_pCoordinate)
				throw std::exception("Invalid XML");
		}

		void updateCoordinate();

		void updateDownTreeCoordinate();

	protected:

		DesignElement(const DesignElement& obj) : IXMLObject(obj) { }
		DesignElement& operator = (const DesignElement& obj) { return *this; }

		// fields
		Coordinate*  m_pCoordinate;
		Color*       m_pColor;

		ID             m_ID;
		math::Matrix2  m_WCS;

		// friends
		friend class Design;
	};


	class SURVEYCORE_API HeaderDesignElement : public DesignElement
	{
	public:

		HeaderDesignElement(ID id, const CoordDat& coord, const ColorDat& color, const BorderDat& borderDat, const ImageProperties& imageProperties, Document* pDoc)
			: DesignElement(id, coord, color, pDoc)
			, m_pBorderElement(NULL)
			, m_pImageElement(NULL)
		{
			AddChild(m_pBorderElement = new BorderElement(borderDat, pDoc));
			AddChild(m_pImageElement = new ImageElement(imageProperties, pDoc));
		}

		HeaderDesignElement(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: DesignElement(pTiElement, childs, pDoc)
			, m_pBorderElement(NULL)
			, m_pImageElement(NULL)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		BorderElement* GetBorderElement() const
		{
			return m_pBorderElement;
		}

		ImageElement* GetImageElement() const
		{
			return m_pImageElement;
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DesignElement::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}

		// ==================================== Types ===========================================

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("header_design"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("header_design"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);
		}

		void afterTreeUpdate()
		{
			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				if (BorderElement* pBorderElement = SurveyType::Cast<BorderElement>(pObject))
					m_pBorderElement = pBorderElement;
				else if (ImageElement* pImageElement = SurveyType::Cast<ImageElement>(pObject))
					m_pImageElement = pImageElement;

				pObject = pObject->NextBrother();
			}

			if (!m_pBorderElement || !m_pImageElement)
				throw std::exception("Invalid XML");
		}

	protected:

		HeaderDesignElement(const HeaderDesignElement& obj) : DesignElement(obj) { }
		HeaderDesignElement& operator = (const HeaderDesignElement& obj) { return *this; }

		// fields
		BorderElement*  m_pBorderElement;
		ImageElement*   m_pImageElement;
	};



	class SURVEYCORE_API Design : public Section
	{
	public:

		Design(Document* pDoc)
			: Section(pDoc)
		{ }

		Design(const TiXmlElement* pTiElement, Document* pDoc)
			: Section(pTiElement, pDoc)
		{
			updateFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		DesignElement* AddDesignElement(ID id, const CoordDat& coord, const ColorDat& color)
		{
			DesignElement* pDesignElement = new DesignElement(id, coord, color, m_pDocument);
			AddChild(pDesignElement);
			return pDesignElement;
		}

		void RemoveDesignElement(DesignElement* pDesignElement)
		{
			if (!pDesignElement)
				return;

			m_DesignElements.erase(pDesignElement->m_ID);

			// notify
			CollectionEventArg evArg("designs", CollectionEventType::CET_Erase, pDesignElement);
			InvokeChange(this, &evArg);

			pDesignElement->KillHimself();
		}

		void RemoveDesignRange(const std::list<DesignElement*>& designs)
		{
			std::list<IXMLObject*> xmlObjs;

			for (DesignElement* pDesignEl : designs)
			{
				assert(pDesignEl);

				xmlObjs.push_back(pDesignEl);				
				pDesignEl->KillHimself();
			}

			// notify
			CollectionEventArg evArg("designs", CollectionEventType::CET_EraseRange, xmlObjs);
			InvokeChange(this, &evArg);
		}

		std::list<DesignElement*>  GetDesignElements() const
		{
			std::list<DesignElement*> outList;

			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				DesignElement* pDesignElement = dynamic_cast<DesignElement*>(pObject);
				if (pDesignElement)
					outList.push_back(pDesignElement);

				pObject = pObject->NextBrother();
			}

			return outList;
		}

		DesignElement* GetDesignElementByID(ID id) const
		{
			auto itrtDes = m_DesignElements.find(id);
			if (itrtDes != m_DesignElements.end())
				return itrtDes->second;

			return NULL;
		}

		// =================================== XML ===============================================

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			Section::UpdateFromXmlElement(pTiElement);
			updateFromXmlElement(pTiElement);
		}

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{
			Section::AddChild(pXmlObject, pAfterXmlObject);

			DesignElement* pDesignElement = dynamic_cast<DesignElement*>(pXmlObject);
			if (!pDesignElement)
				return;

			m_DesignElements.emplace(pDesignElement->m_ID, pDesignElement);

			// notify
			CollectionEventArg evArg("designs", CollectionEventType::CET_PushBack, pDesignElement);
			InvokeChange(this, &evArg);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = Section::SaveToXmlElement();
			// ...
			return pXmlelement;
		}

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			m_DesignElements.clear();
			afterTreeUpdate();

			// notify
			CollectionEventArg evArg("designs", CollectionEventType::CET_Reset);
			InvokeChange(this, &evArg);
		}

		// ==================================== Types ===========================================

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("design"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("design"); }

	private:

		void updateFromXmlElement(const TiXmlElement* pTiElement)
		{
			//...
		}

		void afterTreeUpdate()
		{
			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				DesignElement* pDesignEl = dynamic_cast<DesignElement*>(pObject);
				if (pDesignEl)
					m_DesignElements.emplace(pDesignEl->m_ID, pDesignEl);

				pObject = pObject->NextBrother();
			}

			// m_DesignTree.Reload(m_DesignElements);  работы временно приостановлены
		}

	protected:

		Design(const Design& obj) : Section(obj) { }
		Design& operator = (const Design& obj) { return *this; }

		// fields
		std::unordered_map<ID, DesignElement*>  m_DesignElements;
		Tree<DesignElement>  m_DesignTree;
	};

}