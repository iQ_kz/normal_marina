#pragma once
#include "SurveyCore.h"


namespace survey {

	// predefinition
	class SURVEYCORE_API ISurveyObject;
	class SURVEYCORE_API IDocumentObject;
	class SURVEYCORE_API IXMLObject;
	class SURVEYCORE_API DocumentObject;


	class SURVEYCORE_API EventArg
	{
	public:

		EventArg()
		{ }

		EventArg(const std::string& eventName)
			: m_EventName(eventName)
		{ }

		virtual std::string ToString() { return m_EventName; }

		std::string  m_EventName;
	};


	enum CollectionEventType
	{
		CET_PushBack,
		CET_Insert,
		CET_Erase,
		CET_InsertRange,
		CET_EraseRange,
		CET_Reset,
	};

	class SURVEYCORE_API CollectionEventArg : public EventArg
	{
	public:

		CollectionEventArg(const std::string& collectionName, CollectionEventType event, IXMLObject* pObject = NULL, IXMLObject* pBeforObject = NULL)
			: EventArg(collectionName)
			, m_EventType(event)
			, m_pObject(pObject)
			, m_pBeforObject(pBeforObject)
		{ }

		CollectionEventArg(const std::string& collectionName, CollectionEventType event, const std::list<IXMLObject*>& objs)
			: EventArg(collectionName)
			, m_EventType(event)
			, m_pObject(NULL)
			, m_pBeforObject(NULL)
			, m_Objects(objs)
		{ }

		CollectionEventType  m_EventType;
		IXMLObject*  m_pObject;
		IXMLObject*  m_pBeforObject;
		std::list<IXMLObject*>  m_Objects;
	};


	class SURVEYCORE_API INotifycator
	{
	public:

		virtual ~INotifycator() { }

		virtual void OnChanged(ISurveyObject* sender, EventArg* pArgs, bool async = false) = 0;
	};


	class SURVEYCORE_API INotifycatorSupport
	{
	protected:

		INotifycatorSupport()
		{ }

	public:

		virtual ~INotifycatorSupport() { ResetNotificators(); }
	
		void InvokeChange(ISurveyObject* sender, EventArg* pArgs, bool async = false)
		{
			for (auto itrtNotify : m_pNotifycators)
				itrtNotify->OnChanged(sender, pArgs, async);
		}

		void AddNotificator(INotifycator* pNotifycator) 
		{
			m_pNotifycators.emplace(pNotifycator);
		}
		
		void ResetNotificators() { m_pNotifycators.clear(); }

	protected:

		INotifycatorSupport(const INotifycatorSupport& obj) {}
		INotifycatorSupport& operator = (const INotifycatorSupport& obj) { return *this; }

		std::unordered_set<INotifycator*>  m_pNotifycators;
	};


	class SURVEYCORE_API IRangedNotifycatorSupport : virtual public INotifycatorSupport
	{
	protected:

		IRangedNotifycatorSupport() 
		{ }

		void notifyAddChild(IXMLObject* pXmlObject);

		void notifyRemoveChild(IXMLObject* pXmlObject);

		// friends
		friend class DocumentObject;
	};

}