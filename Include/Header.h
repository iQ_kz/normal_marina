#pragma once
#include "DocumentObject.h"


namespace survey {


	class SURVEYCORE_API Header : public DocumentObject
	{
	public:

		Header(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord,
			const ColorDat& color, const CoordDat& coord, const std::string& text,
			const BorderDat& borderDat, const ImageProperties& imageProperties,
			const std::string& name = "");

		Header(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: DocumentObject(pTiElement, childs, pDoc)
			, m_pText(NULL)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		Text* GetText() const
		{
			return m_pText;
		}

		virtual void EjectSourcesElements(std::unordered_map<std::string, std::list<IXMLObject*>>& sourcesElements) override;

		// ===================  XML ======================================

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}


		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = DocumentObject::SaveToXmlElement();
			// ....
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}


		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("header"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("header"); }

	private:

		void loadElement(const TiXmlElement* pTiElement);

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		Header(const Header& obj) : DocumentObject(obj) { }
		Header& operator = (const Header& obj) { return *this; }

		// fields
		Text*           m_pText;
	};


}