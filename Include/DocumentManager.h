#pragma once
#include "SurveyCore.h"
#include "FileManager.h"
#include "DocumentObject.h"
#include "Document.h"


namespace survey {


	class SURVEYCORE_API DocumentManager : public ISurveyObject, public INotifycatorSupport
	{
		static std::unique_ptr<DocumentManager>  s_DocManager;

		DocumentManager(void);	

	public:

		virtual ~DocumentManager()
		{ }

		static DocumentManager* Instance()
		{
			if (!s_DocManager)
				s_DocManager.reset(new DocumentManager());

			return s_DocManager.get();
		}

		static DocumentManager* TryGetInstance()
		{
			return s_DocManager.get();
		}

		static void Clear()
		{
			s_DocManager.reset(NULL);
		}

		static const ID s_MajorVersion = 0;
		static const ID s_MinorVersion = 1;


		Document* CreateDocument(const std::string name)
		{
			if (m_Documents.find(name) != m_Documents.end())
				return NULL;

			m_Documents.emplace(name, DocumentPtr(new Document(name)));

			EventArg arg = {"dialog"};
			InvokeChange(this, &arg);

			return m_Documents.at(name).get();
		}

		Document* LoadDocumentByID(const std::string& id);

		void CloseDocument(Document* pDoc)
		{
			if (!pDoc)
				return;

			auto itrtDoc = m_Documents.find(pDoc->Name());
			if (itrtDoc == m_Documents.end())
				return;

			m_Documents.erase(itrtDoc);

			EventArg arg = { "dialog" };
			InvokeChange(this, &arg);	
		}

		const std::unordered_map<std::string, DocumentPtr>& Documents() const { return m_Documents; }

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("documentManager"); }

	private:

		DocumentManager(const DocumentManager& obj) {}
		DocumentManager& operator = (const DocumentManager& obj) { return *this; }

		// fields
		std::unordered_map<std::string, DocumentPtr>  m_Documents;

		// friends
		friend Singleton<DocumentManager>;
	};


}