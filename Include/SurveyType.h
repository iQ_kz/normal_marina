#pragma once
#include "SurveyCore.h"


namespace survey {

	// predifinitions
	class SurveyType;
	class IXMLObject;
	class Document;



	class SURVEYCORE_API ISurveyObject
	{
	protected:

		ISurveyObject() { }

	public:

		virtual ~ISurveyObject() { }

		virtual const SurveyType& GetType() const = 0;

	protected:

		ISurveyObject(const ISurveyObject& obj) {}
		ISurveyObject& operator = (const ISurveyObject& obj) { return *this; }

	};


	struct SURVEYCORE_API SurveyType
	{
		std::string  m_Name; // == xml value

		ID            m_MinorVersion;
		ID            m_MajorVersion;

		std::string               m_AncestorType;
		std::vector<std::string>  m_Inheritors;

		std::function<IXMLObject*(const TiXmlElement*, const std::list<IXMLObject*>&, Document*)>  m_fCreator;

		// methods
		bool operator == (const SurveyType& survType) const
		{
			return m_Name == survType.m_Name;
		}

		bool operator != (const SurveyType& survType) const
		{
			return !operator==(survType);
		}

		// statics
		static const std::unordered_map<std::string, SurveyType>  s_Types;

		// creators
		static IXMLObject* CreatorText(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorDesignElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorBlockShemeDesign(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorHeader(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorQuestion(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorQa(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorBlock(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorPage(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorChapter(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorCoordinate(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorColor(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorHeaderDesignElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorImageElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorBorderElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorBorderSideElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorImage(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);

		static IXMLObject* CreatorLogicElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorShowElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorHideElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorEnableElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorDisableElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorValueElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorEmptyElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorWhileElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorWhileShownElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorWhileEnabledElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorWhileHiddenElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorWhileDisabledElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorOnClickElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorOnEnterElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorOnShowElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorOnLeaveElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorOnHideElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorOnEnableElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorOnDisableElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);
		static IXMLObject* CreatorOnChangeElement(const TiXmlElement* pXmlElement, const std::list<IXMLObject*>& childs, Document* pDocument);

		template<class T>
		static T* Cast(ISurveyObject* pObject);
	};



	template<class T>
	T* SurveyType::Cast(ISurveyObject* pObject)
	{
		if (T::Type() == pObject->GetType())
			return dynamic_cast<T*>(pObject);

		return NULL;
	}
}