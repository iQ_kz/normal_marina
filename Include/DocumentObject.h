#pragma once
#include "SurveyCore.h"
#include "SurveyType.h"
#include "SurveyObject.h"
#include "NotifycatorSupport.h"
#include "CommonSurveyTypes.h"


namespace survey {


	class SURVEYCORE_API IDocumentObject : public IXMLObject, virtual public INotifycatorSupport
	{
	protected:

		IDocumentObject(Document* pDoc, const std::string& name = "");

		IDocumentObject(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc);

	public:

		virtual ~IDocumentObject() { }

		ID GetID() const { return m_ID; }
		
		ID GetParentID() const
		{
			if (IDocumentObject* pParent = dynamic_cast<IDocumentObject*>(m_pParent))
				return pParent->GetID();

			return 0;
		}

		const std::string& GetName() const { return m_Name; }

		void SetName(const std::string& name)
		{
			m_Name = name;

			EventArg arg("Name");
			InvokeChange(this, &arg);
		}

		// xml support
		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override;
		virtual TiXmlElement* SaveToXmlElement() const override;

		virtual void KillHimself() override;

	protected:

		IDocumentObject(const IDocumentObject& obj) : IXMLObject(obj) { }
		IDocumentObject& operator = (const IDocumentObject& obj) { return *this; }

		ID  m_ID;
		std::string  m_Name;
	};


	// predefinition
	class SURVEYCORE_API DesignElement;
	class SURVEYCORE_API BSDesignElement;
	class SURVEYCORE_API Text;
	class SURVEYCORE_API Page;
	class SURVEYCORE_API Project;


	class SURVEYCORE_API DocumentObject : public IDocumentObject
	{
	public:

		DocumentObject(Document* pDoc, const std::string& name);

		DocumentObject(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord, const std::string& name);

		DocumentObject(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord, const ColorDat& color, const CoordDat& coord, const std::string& name);

		DocumentObject(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc);

		virtual ~DocumentObject();

		DesignElement* GetDesignElement() const;

		BSDesignElement* GetBSDesignElement() const;

		virtual void KillHimself() override;

		std::list<DocumentObject*>  GetDesignObjects() const;

		std::list<DesignElement*>  GetTopDesignObjects() const;

		std::list<DocumentObject*>  GetBlockShemeDesignObjects() const;

		virtual void EjectSourcesElements(std::unordered_map<std::string, std::list<IXMLObject*>>& sourcesElements);

		// ============================= XML =========================================================

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override;

		virtual TiXmlElement* SaveToXmlElement() const override;

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override;

		virtual void RemoveChild(IXMLObject* pXmlObject) override;

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override;

	private:

		void setRangedNotifycators(const std::unordered_set<IRangedNotifycatorSupport*>& parentNotifycators);

		void removeRangedNotifycators(const std::unordered_set<IRangedNotifycatorSupport*>& parentNotifycators);

		void loadElement(const TiXmlElement* pTiElement);

		void afterTreeUpdate();

		void releaseSources();

		void updateDesignElement();

	protected:

		// fields
		DesignElement*    m_pDesignElement;
		BSDesignElement*  m_pBSDesignElement;

		std::unordered_set<IRangedNotifycatorSupport*>  m_RangedNotifycators;

		// friends
		friend class Page;
		friend class Project;
	};

}