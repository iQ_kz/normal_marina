#pragma once
#include "SurveyObject.h"

#include "ShowObject.h"
#include "HideObject.h"
#include "ValueObject.h"
#include "EmptyObject.h"
#include "EnableObject.h"
#include "DisableObject.h"
#include "WhileObject.h"
#include "WhileEnabledObject.h"
#include "WhileDisabledObject.h"
#include "WhileShownObject.h"
#include "WhileHiddenObject.h"
#include "OnChangeObject.h"
#include "OnClickObject.h"
#include "OnEnableObject.h"
#include "OnDisableObject.h"
#include "OnEnterObject.h"
#include "OnLeaveObject.h"
#include "OnShowObject.h"
#include "OnHideObject.h"

namespace survey {
	enum TypesLogicElement
	{
		TLE_Text = 1,
		TLE_Bool = 2,
		TLE_EnumInt = 3,
	};

	struct LogicElementsValue
	{
		std::string levText;
		double levNumeric;
		bool levBool;
	};
	class SURVEYCORE_API LogicElement : public IXMLObject
	{
	public:

		LogicElement(Document* pDoc, ID userID, TypesLogicElement typesLogicElement)
			: IXMLObject(pDoc)
			, m_UsesId(userID)
			, m_TypesLogicElement(typesLogicElement)
		{ }


		LogicElement(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(pTiElement, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}


		// =========================== Info =====================================================================

		bool IsShown() const { return m_Shown; }
		void SetShown(const bool& inx) { m_Shown = inx; }

		bool IsEnabled() const { return m_Enabled; }
		void SetEnabled(const bool& inx) { m_Enabled = inx; }

		LogicElementsValue GetValue() const{ return m_Value; }
		void SetValue(const LogicElementsValue& value) { m_Value = value; }
		// ===========================================================================================================

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}


		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();
			pXmlelement->SetAttribute("uses", m_UsesId);
			pXmlelement->SetAttribute("type", m_TypesLogicElement);
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{

			if (pXmlObject->GetType() != ShowObject::Type() && pXmlObject->GetType() != HideObject::Type() &&
				pXmlObject->GetType() != EnableObject::Type() && pXmlObject->GetType() != DisableObject::Type() &&
				pXmlObject->GetType() != ValueObject::Type() && pXmlObject->GetType() != EmptyObject::Type())
				throw std::exception("Invalid type of the added object");


			IXMLObject::AddChild(pXmlObject, pAfterXmlObject);
		}

		// =========================== Types =====================================================================
		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("logic_el"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("logic_el"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			pTiElement->Attribute("id", (int*)&m_UsesId);
			pTiElement->Attribute("type", (int*)&m_TypesLogicElement);
		}

		void afterTreeUpdate() {
		};

	protected:

		LogicElement(const LogicElement& obj) : IXMLObject(obj) { }
		LogicElement& operator = (const LogicElement& obj) { return *this; }

		// fields
		bool				m_Shown;
		bool				m_Enabled;
		LogicElementsValue	m_Value;

		ID					m_UsesId;
		TypesLogicElement	m_TypesLogicElement;

		// friends
		friend class Logic;
	};
}