#pragma once
#include "SurveyCore.h"


namespace survey {

	class FileStream
	{
	public:

		FileStream(const boost::filesystem::path& filePath)
			: m_FilePath(filePath)
		{ }

		bool GetFileData(std::vector<unsigned char>& fileData) const
		{
			try
			{
				if (!boost::filesystem::is_regular_file(m_FilePath))
					return false;

				boost::filesystem::ifstream previewFile(m_FilePath, boost::filesystem::ifstream::ate | boost::filesystem::ifstream::binary);
				if (!previewFile.is_open())
					return false;

				const size_t sizePreview = previewFile.tellg();
				previewFile.seekg(0, boost::filesystem::ifstream::beg);

				fileData.resize(sizePreview, 0);

				previewFile.read((char*)fileData.data(), sizePreview);
				previewFile.close();

				return true;
			}
			catch (const boost::filesystem::filesystem_error& ex)
			{
				// to do log error
			}
			catch (const std::exception& ex)
			{
				// to do log error
			}
			catch (...)
			{
				// to do log error
			}

			return false;
		}

		bool SetFileData(const std::vector<unsigned char>& fileData)
		{
			try
			{
				boost::filesystem::ofstream  previewFile(m_FilePath, std::ios_base::binary);
				previewFile.write((char*)fileData.data(), fileData.size());
				previewFile.close();

				return true;
			}
			catch (const boost::filesystem::filesystem_error& ex)
			{
				// to do log error
			}
			catch (const std::exception& ex)
			{
				// to do log error
			}
			catch (...)
			{
				// to do log error
			}

			return false;

		}

	private:

		boost::filesystem::path  m_FilePath;
	};

}