#pragma once
#include <my_global.h>
#include <mysql.h>
#include <string>
#include <memory>
#include <boost\function.hpp>
#include <boost\bind.hpp>
#include <sstream>
#include <map>

class MySqlConnector
{

public:


	struct Result
	{
		Result(){ ZeroMemory(this, sizeof(Result)); }
		MYSQL_RES *result;
		int num_fields;
		bool GetRow(MYSQL_ROW& row)
		{
			return !!(row = mysql_fetch_row(result)) ;
		}

		uint64_t GetRowCount()
		{
			uint64_t ret = 0;
			if (result)
			{
				ret = mysql_num_rows(result);
			}
			return ret;
		}
	};
	typedef std::shared_ptr<Result> ResultPtr;

	MySqlConnector() :m_Con(NULL){}
	~MySqlConnector(){ Disconnect(); }
	bool Connect(std::string host, int port, std::string user, std::string password, std::string dbName)
	{
		bool ret = false;

		m_Con = mysql_init(NULL);
		if (m_Con == NULL)
		{
			_OnError();
			return ret;
		}
		if (mysql_real_connect(m_Con, host.c_str(), user.c_str(), password.c_str(), dbName.c_str(), 0, NULL, 0) == NULL)
		{
			_OnError();
		}
		else
		{
			ret = m_Con != NULL;
		}

		return ret;
	}

	void Disconnect(){ if (m_Con) mysql_close(m_Con); m_Con = NULL; }

	bool IsConnected(){ return m_Con != nullptr; }

	ResultPtr Query(std::string qs)
	{
		ResultPtr ret;
		if (!m_Con){ _OnError(); return ret; }

		if (mysql_query(m_Con, qs.c_str()))
		{
			_OnError();
			return ret;
		}
		ret = ResultPtr(new Result);
		ret->result = mysql_store_result(m_Con);


		if (ret->result)
		{
			ret->num_fields = mysql_num_fields(ret->result);
		}
		

		return ret;
	}


	void SetErrCollback(boost::function1<void, const std::string&> handler)
	{
		_errorHandler = handler;
	}

	boost::function1<void, std::string&> _errorHandler;

	bool Success()
	{
		return mysql_errno(m_Con) == 0;
	}

	int ErrorCode()
	{
		return mysql_errno(m_Con);
	}
	std::string GetErrorMsg()
	{
		return  mysql_error(m_Con);
	}

	my_ulonglong insert_id()
	{
		return 	  mysql_insert_id(m_Con);
	}

private:

	void _OnError()
	{
		std::stringstream ss;
		ss << mysql_error(m_Con);
		_errorHandler(ss.str());
		//Disconnect();
		//fprintf(stderr, "%s\n", mysql_error(_con)); 
	}

	MYSQL *m_Con;
	
};