#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "MathSv.h"
#include "SurveyObject.h"
#include "NotifycatorSupport.h"


namespace survey {

	// predefinition
	class SURVEYCORE_API Document;
	class SURVEYCORE_API Image;
	class SURVEYCORE_API BorderElement;

	// ========================================== Coordinate ============================================================================

	struct CoordDat
	{
		CoordDat(double top = 0.0, double left = 0.0, double width = 1.0, double height = 1.0, double rotateAngle = 0.0)
			: m_Top(top)
			, m_Left(left)
			, m_Width(width)
			, m_Height(height)
			, m_RotateAngle(rotateAngle)
		{ }

		CoordDat(const math::Matrix2& mtr, double width, double height)
			: m_Top(0.0)
			, m_Left(0.0)
			, m_Width(width)
			, m_Height(height)
			, m_RotateAngle(0.0)
		{
			m_RotateAngle = math::AngleRotation(mtr);

			math::Vector2 centrPnt(m_Width / 2.0, m_Height / 2.0);
			centrPnt = centrPnt * mtr;

			m_Left = centrPnt.X - m_Width / 2.0;
			m_Top = centrPnt.Y - m_Height / 2.0;
		}

		math::Matrix2 ToMatrix() const
		{
			const math::Matrix2& translateToCenter = math::MatrixTranslation(math::Vector2(-m_Width / 2.0, -m_Height / 2.0));
			const math::Matrix2& rotationMtr = math::MatrixRotation(m_RotateAngle);
			const math::Matrix2& translateFromCenter = math::InverseMatrix(translateToCenter);

			return translateToCenter * rotationMtr * translateFromCenter * math::MatrixTranslation(math::Vector2(m_Left, m_Top));
		}

		double  m_Top, m_Left;
		double  m_Width, m_Height;
		double  m_RotateAngle;
	};


	class SURVEYCORE_API Coordinate : public IXMLObject
	{
	public:

		Coordinate(const CoordDat& coord, Document* pDoc)
			: IXMLObject(pDoc)
			, m_Coordinate(coord)
		{ }

		Coordinate(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(childs, pDoc)
		{
			loadElement(pTiElement);
		}

		const CoordDat& GetCoord() const { return m_Coordinate; }

		void SetCoord(const CoordDat& coord)
		{
			m_Coordinate = coord;
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			assert(pTiElement);

			m_Coordinate = CoordDat();
			loadElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pTiXmlElem = IXMLObject::SaveToXmlElement();
			assert(pTiXmlElem);

			pTiXmlElem->SetDoubleAttribute("top", m_Coordinate.m_Top);
			pTiXmlElem->SetDoubleAttribute("left", m_Coordinate.m_Left);
			pTiXmlElem->SetDoubleAttribute("height", m_Coordinate.m_Height);
			pTiXmlElem->SetDoubleAttribute("width", m_Coordinate.m_Width);
			pTiXmlElem->SetDoubleAttribute("rotate_angle", m_Coordinate.m_RotateAngle);

			return pTiXmlElem;
		}

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("coord"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("coord"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			pTiElement->Attribute("top", &m_Coordinate.m_Top);
			pTiElement->Attribute("left", &m_Coordinate.m_Left);
			pTiElement->Attribute("height", &m_Coordinate.m_Height);
			pTiElement->Attribute("width", &m_Coordinate.m_Width);
			pTiElement->Attribute("rotate_angle", &m_Coordinate.m_RotateAngle);
		}

	protected:

		Coordinate(const Coordinate& obj) : IXMLObject(obj) { }
		Coordinate& operator = (const Coordinate& obj) { return *this; }

		// fields
		CoordDat  m_Coordinate;
	};
 

	// ======================================  Color ===================================================================================

	struct ColorDat
	{
		ColorDat(unsigned char R = 0, unsigned char G = 0, unsigned char B = 0, unsigned char A = 0)
			: m_R(R)
			, m_G(G)
			, m_B(B)
			, m_A(A)
		{ }

		unsigned char m_R, m_G, m_B, m_A;
	};


	class SURVEYCORE_API Color : public IXMLObject
	{
	public:

		Color(const ColorDat& color, Document* pDoc)
			: IXMLObject(pDoc)
			, m_Color(color)
		{ }

		Color(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(childs, pDoc)
		{
			loadElement(pTiElement);
		}

		const ColorDat& GetColor() const { return m_Color; }

		void SetColor(const ColorDat& coord)
		{
			m_Color = coord;
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			assert(pTiElement);

			loadElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pTiXmlElem = IXMLObject::SaveToXmlElement();
			assert(pTiXmlElem);

			pTiXmlElem->SetAttribute("r", m_Color.m_R);
			pTiXmlElem->SetAttribute("g", m_Color.m_G);
			pTiXmlElem->SetAttribute("b", m_Color.m_B);
			pTiXmlElem->SetAttribute("a", m_Color.m_A);

			return pTiXmlElem;
		}

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("color"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("color"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			try
			{
				int attrVl = 0;
				pTiElement->Attribute("r", &attrVl);
				m_Color.m_R = attrVl;

				attrVl = 0;
				pTiElement->Attribute("g", &attrVl);
				m_Color.m_G = attrVl;

				attrVl = 0;
				pTiElement->Attribute("b", &attrVl);
				m_Color.m_B = attrVl;

				attrVl = 0;
				pTiElement->Attribute("a", &attrVl);
				m_Color.m_A = attrVl;
			}
			catch (...)
			{
				// to do log error
			}
		}

	protected:

		Color(const Color& obj) : IXMLObject(obj) { }
		Color& operator = (const Color& obj) { return *this; }

		// fields
		ColorDat  m_Color;
	};


	// ======================================  Image ===================================================================================

	struct Crop
	{
		Crop(double left = 0.0, double top = 0.0, double right = 0.0, double bottom = 0.0)
		: m_Left(left)
		, m_Top(top)
		, m_Right(right)
		, m_Bottom(bottom)
		{ }

		void Clear()
		{
			m_Left = m_Top = m_Right = m_Bottom = 0.0;
		}

		double  m_Left, m_Top;
		double  m_Right, m_Bottom;
	};


	struct ImageProperties
	{
		ImageProperties()
			: m_RotateAngle(0.0)
		{ }

		ImageProperties(const std::string& imageId, const Crop& crop, double rotateAngle)
			: m_ImageId(imageId)
			, m_Crop(crop)
			, m_RotateAngle(rotateAngle)
		{ }

		void Clear()
		{
			m_ImageId.clear();
			m_Crop.Clear();
			m_RotateAngle = 0.0;
		}

		std::string  m_ImageId;
		Crop         m_Crop;
		double       m_RotateAngle;
	};


	class SURVEYCORE_API ImageElement : public IXMLObject, public INotifycatorSupport
	{
	public:

		ImageElement(const ImageProperties& imageProperties, Document* pDoc)
			: IXMLObject(pDoc)
			, m_ImageProperties(imageProperties)
		{ }

		ImageElement(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(childs, pDoc)
		{
			loadElement(pTiElement);
		}

		const ImageProperties& GetImageProperties() const { return m_ImageProperties; }

		void SetImageProperties(const ImageProperties& imageProps)
		{
			m_ImageProperties = imageProps;

			EventArg arg("ImageProperties");
			InvokeChange(this, &arg);
		}

		const std::string& GetIdImage() const
		{
			return m_ImageProperties.m_ImageId;
		}

		void SetIdImage(const std::string& id)
		{
			m_ImageProperties.m_ImageId = id;

			EventArg arg("IdImage");
			InvokeChange(this, &arg);

			arg.m_EventName = "Image";
			InvokeChange(this, &arg);
		}

		Image* GetImage() const;

		double GetRotateAngle() const
		{
			return m_ImageProperties.m_RotateAngle;
		}

		void SetRotateAngle(double rotateAngle)
		{
			m_ImageProperties.m_RotateAngle = rotateAngle;

			EventArg arg("RotateAngle");
			InvokeChange(this, &arg);
		}

		const Crop& GetCrop() const
		{
			return m_ImageProperties.m_Crop;
		}

		void SetCrop(const Crop& crop)
		{
			m_ImageProperties.m_Crop = crop;

			EventArg arg("Crop");
			InvokeChange(this, &arg);
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			assert(pTiElement);

			m_ImageProperties.Clear();
			loadElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pTiXmlElem = IXMLObject::SaveToXmlElement();
			assert(pTiXmlElem);

			pTiXmlElem->SetAttribute("image_id", m_ImageProperties.m_ImageId);
			pTiXmlElem->SetDoubleAttribute("rotate_angle", m_ImageProperties.m_RotateAngle);
			
			// crop
			pTiXmlElem->SetDoubleAttribute("crop_l", m_ImageProperties.m_Crop.m_Left);
			pTiXmlElem->SetDoubleAttribute("crop_r", m_ImageProperties.m_Crop.m_Right);
			pTiXmlElem->SetDoubleAttribute("crop_t", m_ImageProperties.m_Crop.m_Top);
			pTiXmlElem->SetDoubleAttribute("crop_b", m_ImageProperties.m_Crop.m_Bottom);

			return pTiXmlElem;
		}

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("image_element"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("image_element"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			try
			{
				const char* pVl = pTiElement->Attribute("image_id");
				if (pVl)
					m_ImageProperties.m_ImageId = pVl;

				double dblAttrVl = 0;
				pTiElement->QueryDoubleAttribute("rotate_angle", &dblAttrVl);
				m_ImageProperties.m_RotateAngle = dblAttrVl;

				// crop
				dblAttrVl = 0;
				pTiElement->QueryDoubleAttribute("crop_l", &dblAttrVl);
				m_ImageProperties.m_Crop.m_Left = dblAttrVl;

				dblAttrVl = 0;
				pTiElement->QueryDoubleAttribute("crop_r", &dblAttrVl);
				m_ImageProperties.m_Crop.m_Right = dblAttrVl;

				dblAttrVl = 0;
				pTiElement->QueryDoubleAttribute("crop_t", &dblAttrVl);
				m_ImageProperties.m_Crop.m_Top = dblAttrVl;

				dblAttrVl = 0;
				pTiElement->QueryDoubleAttribute("crop_b", &dblAttrVl);
				m_ImageProperties.m_Crop.m_Bottom = dblAttrVl;
			}
			catch (...)
			{
				// to do log error
			}
		}

	protected:

		ImageElement(const ImageElement& obj) : IXMLObject(obj) { }
		ImageElement& operator = (const ImageElement& obj) { return *this; }

		// fields
		ImageProperties  m_ImageProperties;
	};



	// =========================================  BorderSide  ===============================================================================

	enum BorderSideType
	{
		BS_Undefined,
		BS_Left,
		BS_Right,
		BS_Top,
		BS_Bottom,
	};

	
	struct BorderSideDat
	{
		double        m_Thickness;
		unsigned int  m_LineType;
		ColorDat      m_Color;
	};


	class SURVEYCORE_API BorderSideElement : public IXMLObject, public INotifycatorSupport
	{
	public:

		BorderSideElement(BorderSideType bsType, const BorderSideDat& colorDat, Document* pDoc)
			: IXMLObject(pDoc)
			, m_BsType(bsType)
			, m_pColor(NULL)
			, m_Thickness(colorDat.m_Thickness)
			, m_LineType(colorDat.m_LineType)
		{
			AddChild(m_pColor = new Color(colorDat.m_Color, pDoc));
		}

		BorderSideElement(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(childs, pDoc)
			, m_BsType(BorderSideType::BS_Undefined)
			, m_pColor(NULL)
			, m_Thickness(0.0)
			, m_LineType(0)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			assert(pTiElement);

			loadElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pTiXmlElem = IXMLObject::SaveToXmlElement();
			assert(pTiXmlElem);

			pTiXmlElem->SetAttribute("bs_type", m_BsType);
			pTiXmlElem->SetDoubleAttribute("thickness", m_Thickness);
			pTiXmlElem->SetAttribute("line_type", m_LineType);

			return pTiXmlElem;
		}

		// properties
		double GetThickness()
		{
			return m_Thickness;
		}

		void SetThickness(double thickness)
		{
			m_Thickness = thickness;

			EventArg arg("Thickness");
			InvokeChange(this, &arg);
		}

		unsigned int GetLineType()
		{
			return m_LineType;
		}

		void SetLineType(unsigned int lineType)
		{
			m_LineType = lineType;

			EventArg arg("LineType");
			InvokeChange(this, &arg);
		}

		ColorDat GetColorDat()
		{
			return m_pColor->GetColor();
		}

		void SetColorDat(const ColorDat& colorDat)
		{
			m_pColor->SetColor(colorDat);

			EventArg arg("ColorDat");
			InvokeChange(this, &arg);
		}

		// types work
		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("border_side_element"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("border_side_element"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			try
			{
				m_BsType = BorderSideType::BS_Undefined;
				pTiElement->Attribute("bs_type", (int*)&m_BsType);

				m_LineType = 0;
				pTiElement->Attribute("line_type", (int*)&m_LineType);

				m_Thickness = 0.0;
				pTiElement->QueryDoubleAttribute("thickness", &m_Thickness);
			}
			catch (...)
			{
				// to do log error
			}
		}

		void afterTreeUpdate()
		{
			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				if (Color* pColor = SurveyType::Cast<Color>(pObject))
				{
					m_pColor = pColor;
					break;
				}
			}

			if (!m_pColor)
				throw std::exception("Invalid XML");
		}

	protected:

		BorderSideElement(const BorderSideElement& obj) : IXMLObject(obj) { }
		BorderSideElement& operator = (const BorderSideElement& obj) { return *this; }

		// fields
		BorderSideType  m_BsType;
		double          m_Thickness;
		unsigned int    m_LineType;
		Color*          m_pColor;

		// friends
		friend class BorderElement;
	};


	// =========================================  Border  ===============================================================================


	struct BorderDat
	{
		double         m_ShadowSize;
		BorderSideDat  m_Left, m_Right, m_Top, m_Bottom;
	};


	class SURVEYCORE_API BorderElement : public IXMLObject, public INotifycatorSupport
	{
	public:

		BorderElement(const BorderDat& borderDat, Document* pDoc)
			: IXMLObject(pDoc)
			, m_ShadowSize(borderDat.m_ShadowSize)
			, m_pLeftBorderSide(NULL)
			, m_pRightBorderSide(NULL)
			, m_pTopBorderSide(NULL)
			, m_pBottomBorderSide(NULL)
		{
			AddChild(m_pLeftBorderSide = new BorderSideElement(BorderSideType::BS_Left, borderDat.m_Left, pDoc));
			AddChild(m_pRightBorderSide = new BorderSideElement(BorderSideType::BS_Right, borderDat.m_Right, pDoc));
			AddChild(m_pTopBorderSide = new BorderSideElement(BorderSideType::BS_Top, borderDat.m_Top, pDoc));
			AddChild(m_pBottomBorderSide = new BorderSideElement(BorderSideType::BS_Bottom, borderDat.m_Bottom, pDoc));
		}

		BorderElement(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(childs, pDoc)
			, m_ShadowSize(0)
			, m_pLeftBorderSide(NULL)
			, m_pRightBorderSide(NULL)
			, m_pTopBorderSide(NULL)
			, m_pBottomBorderSide(NULL)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}


		// properties
		double GetShadowSize() const
		{
			return m_ShadowSize;
		}

		void SetShadowSize(double shadowSize)
		{
			m_ShadowSize = shadowSize;

			EventArg arg("ShadowSize");
			InvokeChange(this, &arg);
		}

		BorderSideElement* GetLeftBorderSide() const
		{
			return m_pLeftBorderSide;
		}

		BorderSideElement* GetRightBorderSide() const
		{
			return m_pRightBorderSide;
		}

		BorderSideElement* GetTopBorderSide() const
		{
			return m_pTopBorderSide;
		}

		BorderSideElement* GetBottomBorderSide() const
		{
			return m_pBottomBorderSide;
		}

		// xml
		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			assert(pTiElement);

			loadElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pTiXmlElem = IXMLObject::SaveToXmlElement();
			assert(pTiXmlElem);

			pTiXmlElem->SetDoubleAttribute("shadow_size", m_ShadowSize);
			return pTiXmlElem;
		}

		// types
		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("border_side"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("border_side"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			try
			{
				m_ShadowSize = 0;
				pTiElement->QueryDoubleAttribute("shadow_size", &m_ShadowSize);
			}
			catch (...)
			{
				// to do log error
			}
		}

		void afterTreeUpdate()
		{
			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				if (BorderSideElement* pBorderSide = SurveyType::Cast<BorderSideElement>(pObject))
				{
					switch (pBorderSide->m_BsType)
					{
					case BorderSideType::BS_Bottom:
						m_pBottomBorderSide = pBorderSide;
						break;
					case BorderSideType::BS_Top:
						m_pTopBorderSide = pBorderSide;
						break;
					case BorderSideType::BS_Left:
						m_pLeftBorderSide = pBorderSide;
						break;
					case BorderSideType::BS_Right:
						m_pRightBorderSide = pBorderSide;
						break;
					default:
						throw std::exception("Invalid XML");
					}
				}

				pObject = pObject->NextBrother();
			}

			if (!m_pLeftBorderSide || !m_pRightBorderSide || !m_pTopBorderSide || !m_pBottomBorderSide)
				throw std::exception("Invalid XML");
		}

	protected:

		BorderElement(const BorderElement& obj) : IXMLObject(obj) { }
		BorderElement& operator = (const BorderElement& obj) { return *this; }

		// fields
		double              m_ShadowSize;
		BorderSideElement*  m_pLeftBorderSide;
		BorderSideElement*  m_pRightBorderSide;
		BorderSideElement*  m_pTopBorderSide;
		BorderSideElement*  m_pBottomBorderSide;
	};

}