#pragma once
#include "SurveyObject.h"

namespace survey {
	class SURVEYCORE_API WhileShownObject : public IXMLObject
	{
	public:

		// =====================================================================================================
		WhileShownObject(Document* pDoc, const std::string& condition)
			: IXMLObject(pDoc)
			, m_Condition(condition)
		{ }

		WhileShownObject(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(pTiElement, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}


		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);

			// clear data
			m_Condition.clear();

			loadElement(pTiElement);
		}

		// =====================================================================================================

		std::string GetCondition() const { return m_Condition; }

		void SetCondition(const std::string& str)
		{
			m_Condition = str;
		}

		// ============================ XML ======================================================================

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();
			pXmlelement->SetAttribute("if", m_Condition);
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("while_show"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("while_show"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			if (const char* pVl = pTiElement->Attribute("if"))
				m_Condition = pVl;
		}

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		WhileShownObject(const WhileShownObject& obj) : IXMLObject(obj) { }
		WhileShownObject& operator = (const WhileShownObject& obj) { return *this; }

		// fields

		std::string  m_Condition;
	};
}