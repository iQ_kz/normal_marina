#pragma once
#include "Vector2.h"
#include "Matrix2.h"


using namespace survey;


#ifndef PI
	#define PI 3.141592653589793f
#endif


namespace survey {
	namespace math {

		bool Compare(double vl1, double vl2, double tol = DEF_TOL);

		bool Compare(const Vector2& vc1, const Vector2& vc2, double tol = DEF_TOL);

		Vector2 operator * (const Vector2& vc, const Matrix2& mt);

		Vector2& operator *= (Vector2& vc, const Matrix2& mt);

		Vector2 TransformNormal(const Vector2& vc, const Matrix2& mt);

		Matrix2 MatrixTranslation(Vector2 vc);

		Matrix2 MatrixRotation(double angle);

		double AngleRotation(const Matrix2& mtr);

		double Dot(const Vector2& vc1, const Vector2& vc2);

		Matrix2 InverseMatrix(const Matrix2& mtr);

	}
}