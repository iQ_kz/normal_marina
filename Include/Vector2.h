#pragma once
#include "SurveyCore.h"


namespace survey {
	namespace math {

		struct SURVEYCORE_API Vector2
		{
			Vector2()
				: X(0), Y(0), W(1)
			{ }

			Vector2(double x, double y, double w = 1.0)
				: X(x), Y(y), W(w)
			{ }

			// operators
			Vector2 operator + (const Vector2& vc) const;
			Vector2 operator - (const Vector2& vc) const;
			Vector2 operator * (double vl) const;
			Vector2 operator / (double vl) const;

			Vector2& operator += (const Vector2& vc);
			Vector2& operator -= (const Vector2& vc);
			Vector2& operator *= (double vl);
			Vector2& operator /= (double vl);

			// methods
			Vector2  Revers() const;
			double   Length() const;
			double   LenghtSquare() const;
			bool     Normalize(double tol = DEF_TOL);
			bool     IsNormal(double tol = DEF_TOL) const;

			Vector2 OrtX() const;
			Vector2 OrtY() const;

			// statics
			static const Vector2& NaN();
			static const Vector2& Null();
			static const Vector2& OX();
			static const Vector2& OY();

			// fields
			double X, Y, W;
		};


		SURVEYCORE_API Vector2 operator * (const double& vl, const Vector2 vc);

	}
}