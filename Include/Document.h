#pragma once
#include "SurveyCore.h"
#include "ID_Genearator.h"
#include "DocumentObject.h"
#include "Section.h"
#include "Project.h"
#include "Logic.h"
#include "Design.h"
#include "ToDo.h"
#include "RoLs.h"
#include "Texts.h"
#include "Images.h"
#include "Sources.h"
#include "BlockSchemeDesign.h"


namespace survey {


	class SURVEYCORE_API Document : public ISurveyObject, public INotifycatorSupport
	{
	public:

		Document(const std::string& name);

		Document(const DocumentInfo& docInfo);

		~Document()
		{
			clear();
		}

		// sections
		Project* GetProject() const { return m_pProject; }
		Logic* GetLogic() const { return m_pLogic; }
		Design* GetDesign() const { return m_pDesign; }
		ToDo* GetToDo() const { return m_pToDo; }
		RoLs* GetRoLs() const { return m_pRoLs; }
		BlockShemeDesign* GetBlockShemeDesign() const { return m_pBlockShemeDesign; }
		Texts* GetTexts() const { return m_pTexts; }
		Images* GetImages() const { return m_pImages; }
		
		IDocumentObject* DocObjectByID(const ID& id) const
		{
			auto itrt = m_DocObjects.find(id);
			if (itrt == m_DocObjects.end())
			{
				// to do log error
				return NULL;
			}

			return itrt->second;
		}

		std::list<Section*> GetSections() const 
		{ 
			return std::list<Section*> { m_pProject, m_pLogic, m_pDesign, m_pBlockShemeDesign, m_pToDo, m_pRoLs, m_pTexts, m_pImages }; 
		}

		static std::list<std::string> GetNameSections()
		{
			return std::list<std::string> { Project::Type().m_Name, Logic::Type().m_Name, Design::Type().m_Name, BlockShemeDesign::Type().m_Name, ToDo::Type().m_Name, RoLs::Type().m_Name, Texts::Type().m_Name, Images::Type().m_Name };
		}

		// propertyes
		const std::string& Name() const { return m_Name; }
		const std::string& GetID() const { return m_ID; }

		// command supported
		bool Save(const PreviewMap& previews) const;
		bool SaveToCloud() const;

		bool ToNextRevision();
		bool ToPreviosRevision();

		std::list<BackupProjectProperties> GetAllBackupDescriptions() const;
		bool SetBackup(const std::string& id);

		// type supported
		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("document"); }

	private:

		Document(const Document& obj) {}
		Document& operator = (const Document& obj) { return *this; }

		bool set(const DocumentInfo& docInfo);
		bool load(const DocumentInfo& docInfo);
		void clear();
		void removeIDS(const std::list<ID>& ids)
		{
			for (ID id : ids)
			{
				if (!m_ID_GenObjects.RemoveID(id))
				{
					// to do log error
				}

				m_DocObjects.erase(id);
			}
		}

		// fields
		Project*           m_pProject;
		Logic*             m_pLogic;
		Design*            m_pDesign;
		BlockShemeDesign*  m_pBlockShemeDesign;
		Texts*			   m_pTexts;
		ToDo*              m_pToDo;
		RoLs*              m_pRoLs;
		Images*            m_pImages;

		std::string    m_Name;
		std::string    m_ID;

		ID_Genearator  m_ID_GenObjects;
		std::unordered_map<ID, IDocumentObject*>  m_DocObjects;

		// friends
		friend class IDocumentObject;
		friend class DocumentObject;
		friend class Section;
		friend class Project;
	};

	typedef std::unique_ptr<Document> DocumentPtr;
}