#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "SurveyType.h"
#include "CommonSurveyTypes.h"
#include "Section.h"



namespace survey {


	// predefinition
	class SURVEYCORE_API BlockShemeDesign;


	class SURVEYCORE_API BSDesignElement : public IXMLObject, public INotifycatorSupport
	{
	public:

		BSDesignElement(ID id, const CoordDat& coord, const ColorDat& color, Document* pDoc)
			: IXMLObject(pDoc)
			, m_ID(id)
			, m_pCoordinate(NULL)
			, m_pColor(NULL)
		{
			AddChild(m_pCoordinate = new Coordinate(coord, pDoc));
			AddChild(m_pColor = new Color(color, pDoc));
		}

		BSDesignElement(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: IXMLObject(childs, pDoc)
			, m_pCoordinate(NULL)
			, m_pColor(NULL)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		// ==================  DesignParentID ========================================

		ID GetBSDesignParentID() const
		{
			return m_ID;
		}

		// ==================  Color ============================================

		ColorDat GetBSColor() const
		{
			assert(m_pColor);
			return m_pColor->GetColor();
		}

		void SetBSColor(const ColorDat& clr)
		{
			assert(m_pColor);
			m_pColor->SetColor(clr);

			EventArg arg("BSColor");
			InvokeChange(this, &arg);
		}

		// =============== Coordinate =============================================

		CoordDat GetBSCoordinate() const
		{
			assert(m_pCoordinate);
			return m_pCoordinate->GetCoord();
		}

		void SetBSCoordinate(const CoordDat& crd)
		{
			assert(m_pCoordinate);
			m_pCoordinate->SetCoord(crd);

			EventArg arg("BSCoordinate");
			InvokeChange(this, &arg);
		}

		// =======================================================================

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}


		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();
			pXmlelement->SetAttribute("id", m_ID);
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}


		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("bs_design_el"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("bs_design_el"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			assert(pTiElement);

			const char* attr = pTiElement->Attribute("id");
			if (!attr)
				throw std::exception("Invalid XML");

			m_ID = boost::lexical_cast<ID>(attr);
		}

		void afterTreeUpdate()
		{
			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				if (Color* pColor = SurveyType::Cast<Color>(pObject))
					m_pColor = pColor;
				else if (Coordinate* pCoord = SurveyType::Cast<Coordinate>(pObject))
					m_pCoordinate = pCoord;

				pObject = pObject->NextBrother();
			}

			if (!m_pColor || !m_pCoordinate)
				throw std::exception("Invalid XML");
		}

	protected:

		BSDesignElement(const BSDesignElement& obj) : IXMLObject(obj) { }
		BSDesignElement& operator = (const BSDesignElement& obj) { return *this; }

		// fields
		Coordinate*  m_pCoordinate;
		Color*       m_pColor;

		ID  m_ID;

		// friends
		friend class BlockShemeDesign;
	};


	class SURVEYCORE_API BlockShemeDesign : public Section
	{
	public:

		BlockShemeDesign(Document* pDoc)
			: Section(pDoc)
		{ }

		BlockShemeDesign(const TiXmlElement* pTiElement, Document* pDoc)
			: Section(pTiElement, pDoc)
		{
			updateFromXmlElement(pTiElement);
			afterTreeUpdate();
		}

		BSDesignElement* AddDesignElement(ID id, const CoordDat& coord, const ColorDat& color)
		{
			BSDesignElement* pDesignElement = new BSDesignElement(id, coord, color, m_pDocument);
			AddChild(pDesignElement);
			m_DesignElements.emplace(id, pDesignElement);

			// notify
			CollectionEventArg evArg("designs", CollectionEventType::CET_PushBack, pDesignElement);
			InvokeChange(this, &evArg);

			return pDesignElement;
		}

		void RemoveDesignElement(BSDesignElement* pDesignElement)
		{
			if (!pDesignElement)
				return;

			m_DesignElements.erase(pDesignElement->m_ID);

			// notify
			CollectionEventArg evArg("designs", CollectionEventType::CET_Erase, pDesignElement);
			InvokeChange(this, &evArg);

			pDesignElement->KillHimself();
		}

		void RemoveDesignRange(const std::list<BSDesignElement*>& designs)
		{
			std::list<IXMLObject*> xmlObjs;

			for (BSDesignElement* pDesignEl : designs)
			{
				assert(pDesignEl);

				xmlObjs.push_back(pDesignEl);
				m_DesignElements.erase(pDesignEl->m_ID);
				pDesignEl->KillHimself();
			}

			// notify
			CollectionEventArg evArg("designs", CollectionEventType::CET_EraseRange, xmlObjs);
			InvokeChange(this, &evArg);
		}


		std::list<BSDesignElement*>  GetDesignElements() const
		{
			std::list<BSDesignElement*> outList;

			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				BSDesignElement* pDesignElement = SurveyType::Cast<BSDesignElement>(pObject);
				if (pDesignElement)
					outList.push_back(pDesignElement);

				pObject = pObject->NextBrother();
			}

			return outList;
		}

		BSDesignElement* GetDesignElementByID(ID id) const
		{
			auto itrtDes = m_DesignElements.find(id);
			if (itrtDes != m_DesignElements.end())
				return itrtDes->second;

			return NULL;
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			Section::UpdateFromXmlElement(pTiElement);
			updateFromXmlElement(pTiElement);
		}

		// =================================== XML ===============================================

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = Section::SaveToXmlElement();
			// ...
			return pXmlelement;
		}

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateTreeFromXmlElement(pTiElement);
			m_DesignElements.clear();
			afterTreeUpdate();

			// notify
			CollectionEventArg evArg("designs", CollectionEventType::CET_Reset);
			InvokeChange(this, &evArg);
		}

		// ==================================== Types ===========================================

		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("block_sheme_design"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("block_sheme_design"); }

	private:

		void updateFromXmlElement(const TiXmlElement* pTiElement)
		{
			//...
		}

		void afterTreeUpdate()
		{
			IXMLObject* pObject = m_pChildFirst;
			while (pObject)
			{
				BSDesignElement* pDesignEl = SurveyType::Cast<BSDesignElement>(pObject);
				if (pDesignEl)
					m_DesignElements.emplace(pDesignEl->m_ID, pDesignEl);

				pObject = pObject->NextBrother();
			}
		}

	protected:

		BlockShemeDesign(const BlockShemeDesign& obj) : Section(obj) { }
		BlockShemeDesign& operator = (const BlockShemeDesign& obj) { return *this; }

		// fields
		std::unordered_map<ID, BSDesignElement*>  m_DesignElements;
	};



}

