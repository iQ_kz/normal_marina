#include "stdafx.h"
#include "logger.h"

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <string>
#include <ostream>
#include <fstream>
#include <iomanip>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared_object.hpp>
#include <boost/phoenix/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/sources/basic_logger.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/attributes/scoped_attribute.hpp>
#include <boost/log/utility/value_ref.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/thread.hpp>


#include <boost/atomic.hpp>


namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;



using boost::shared_ptr;



typedef sinks::synchronous_sink< sinks::text_file_backend > file_sink;
namespace survey
{

	template< typename CharT, typename TraitsT >
	inline std::basic_ostream< CharT, TraitsT >& operator<< (
		std::basic_ostream< CharT, TraitsT >& strm, SeverityLevel lvl)
	{

		std::string l = Logger::LogLevel2String(lvl);
		if (!l.empty())
			strm << std::setw(9) << l;
		else
			strm << static_cast<int>(lvl);
		return strm;
	}


	BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", SeverityLevel);

	

	boost::atomic <SeverityLevel>  gLogLevel;

	//static SeverityLevel gLogLevel;


	void Logger::Init(const std::string& Name, bool AutoFlush, SeverityLevel Filter)
{
	// Create a text file sink
	gLogLevel = Filter;
	
	shared_ptr< file_sink > FileSink;
	FileSink = shared_ptr< file_sink >(new file_sink(
		keywords::file_name = Name + "_%Y%m%d_%H%M%S_%5N.log",      // file name pattern
		keywords::rotation_size = 16384,                     // rotation size, in characters
		keywords::auto_flush = AutoFlush
		));

	
	FileSink->set_filter(expr::attr< SeverityLevel >("Severity") >= Filter);

	// Set up where the rotated files will be stored
	FileSink->locked_backend()->set_file_collector(sinks::file::make_collector(
		keywords::target = "logs",                          // where to store rotated files
		keywords::max_size = 16 * 1024 * 1024,              // maximum total size of the stored files, in bytes
		keywords::min_free_space = 100 * 1024 * 1024        // minimum free space on the drive, in bytes
		));

	// Upon restart, scan the target directory for files matching the file_name pattern
	FileSink->locked_backend()->scan_for_files();

	FileSink->set_formatter
		(
		expr::format("[ %1% ]%2%: ( %3% ) : %4% -<%5%-> > | %6% | ")
		% expr::attr< SeverityLevel >("Severity")
		% expr::attr< unsigned int >("RecordID")
		% expr::attr< boost::posix_time::ptime >("TimeStamp")
		% expr::attr<boost::log::attributes::current_thread_id::value_type >("ThreadID")
		% expr::format_named_scope("Scope", keywords::format = "%n", keywords::iteration = expr::forward)
		% expr::smessage
		);

	// Add it to the core
	logging::core::get()->add_sink(FileSink);
	
	

	// Add some attributes too
	logging::core::get()->add_global_attribute("TimeStamp", attrs::local_clock());
	logging::core::get()->add_global_attribute("RecordID", attrs::counter< unsigned int >());
	logging::add_common_attributes();
	BOOST_LOG_SCOPED_THREAD_TAG("ThreadID", boost::this_thread::get_id());
	logging::core::get()->add_global_attribute("Scope", attrs::named_scope());
}

	void Logger::SetFilter(SeverityLevel Filter)
{
	gLogLevel = Filter;
	logging::core::get()->set_filter(expr::attr< SeverityLevel >("Severity") >= Filter);
}

	survey::SeverityLevel Logger::GetLogLevel()
	{
		return gLogLevel;
	}

	const char* Logger::LogLevel2String(SeverityLevel Level)
	{
		static const char* const str[] =
		{
			"normal",
			"notify",
			"warning",
			"error",
			"critical"
		};
		if (static_cast<std::size_t>(Level) < (sizeof(str) / sizeof(*str)))
			return 	str[Level];
		else return "";
	}



}//!namespace survey