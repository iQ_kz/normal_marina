#pragma once
#include "DocumentObject.h"


namespace survey {


	class SURVEYCORE_API Block : public DocumentObject
	{
	public:

		Block(Document* pDoc, const ColorDat& bsColor, const CoordDat& bsCoord, const ColorDat& color, const CoordDat& coord, const std::string& name = "")
			: DocumentObject(pDoc, bsColor, bsCoord, color, coord, name)
		{ }

		Block(const TiXmlElement* pTiElement, const std::list<IXMLObject*>& childs, Document* pDoc)
			: DocumentObject(pTiElement, childs, pDoc)
		{
			loadElement(pTiElement);
			afterTreeUpdate();
		}

		// ===================  XML ======================================

		virtual void AddChild(IXMLObject* pXmlObject, IXMLObject* pAfterXmlObject = NULL) override
		{
			if (pXmlObject->GetType() != Qa::Type() && pXmlObject->GetType() != Header::Type())
				throw std::exception("Invalid type of the added object");

			DocumentObject::AddChild(pXmlObject, pAfterXmlObject);
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateFromXmlElement(pTiElement);
			loadElement(pTiElement);
		}


		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = DocumentObject::SaveToXmlElement();
			// ....
			return pXmlelement;
		}


		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement) override
		{
			DocumentObject::UpdateTreeFromXmlElement(pTiElement);
			afterTreeUpdate();
		}


		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("block"); }

		static const SurveyType& Type() { return SurveyType::s_Types.at("block"); }

	private:

		void loadElement(const TiXmlElement* pTiElement)
		{
			// ....
		}

		void afterTreeUpdate()
		{
			// ...
		}

	protected:

		Block(const Block& obj) : DocumentObject(obj) { }
		Block& operator = (const Block& obj) { return *this; }

		// fields

	};


}