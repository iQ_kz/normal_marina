#pragma once

#include <stdexcept>
#include <string>
#include <iostream>

#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/common.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_file_backend.hpp>

#include <common_types.h>



namespace logging = boost::log;
namespace attrs = boost::log::attributes;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;



namespace survey
{
	// log severity level
	enum SeverityLevel
	{
		normal,
		notification,
		warning,
		error,
		critical,
		UnknownSeverity
	};

	/*usage 
	//src::severity_logger< SeverityLevel > slg;
	//BOOST_LOG_NAMED_SCOPE("command processor: GetCommand");
	//BOOST_LOG_SEV(slg, critical) << "command: " << CommandStr;	
	
	*/
	class Logger
	{
	public:

		enum { LOG_RECORDS_TO_WRITE = 10000 };

		static void Init(const std::string& Name, bool AutoFlush = true, SeverityLevel Filter = normal);

		static void SetFilter(SeverityLevel Filter);
		static SeverityLevel GetLogLevel();

		static const char* LogLevel2String(SeverityLevel Level);
	};

}//!namespace survey
