#pragma once
#include "SurveyCore.h"
#include "FileManager.h"
#include "DocumentObject.h"


namespace survey {

	// predifinition
	struct SURVEYCORE_API FileManager;


	class SURVEYCORE_API Section : public IXMLObject, public virtual INotifycatorSupport
	{
	protected:

		Section(Document* pDoc)
			: IXMLObject(pDoc)
		{ }

		Section(const TiXmlElement* pTiElement, Document* pDoc)
			: IXMLObject(pTiElement, pDoc)
		{
			updateFromXmlElement(pTiElement);
			afterUpdateTree();
		}

		virtual ~Section()
		{
			releaseChildTree();
		}

	public:

		bool CanRead() const
		{
			// to do reflection
			return true;
		}

		bool CanWrite() const
		{
			return true;
		}

		virtual void UpdateFromXmlElement(const TiXmlElement* pTiElement) override
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			updateFromXmlElement(pTiElement);
		}

		virtual TiXmlElement* SaveToXmlElement() const override
		{
			TiXmlElement* pXmlelement = IXMLObject::SaveToXmlElement();
			// ...
			return pXmlelement;
		}

		virtual void UpdateTreeFromXmlElement(const TiXmlElement* pTiElement)
		{
			IXMLObject::UpdateFromXmlElement(pTiElement);
			afterUpdateTree();
		}

		bool SaveToDocument(DocumentSectionInfo& toStruct) const;

	private:

		void updateFromXmlElement(const TiXmlElement* pTiElement)
		{
			// ...
		}

		void afterUpdateTree()
		{
			// ...
		}

	protected:

		Section(const Section& obj) : IXMLObject(obj) { }
		Section& operator = (const Section& obj) { return *this; }

		virtual void KillHimself() override
		{ }

		// fields

		// friends
	};

}