#pragma once
#include "common_types.h"
#include "SurveyCore.h"
#include "Section.h"


namespace survey {

	class SURVEYCORE_API Sources : public Section
	{
	public:

		Sources(Document* pDoc)
			: Section(pDoc)
		{ }


		virtual const SurveyType& GetType() const override { return SurveyType::s_Types.at("Sources"); }

	protected:

		Sources(const Sources& obj) : Section(obj) { }
		Sources& operator = (const Sources& obj) { return *this; }
	};

}