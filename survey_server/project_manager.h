#pragma once
#include "../include/common_types.h"
#include "DAL.h"
#include <boost/shared_ptr.hpp>
#include "../include/transport_client_interface.h"
#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include "../include/logger.h"
#include "../include/transport/transport_types.h"

#include <boost/algorithm/hex.hpp >

namespace fs = boost::filesystem;

namespace survey
{
	namespace server
	{
		using namespace transport;
		typedef transport::Result Result;
		using namespace transport;
		class ProjectManager
		{
		public:
			void Init(IDALPtr DALObj, const std::string& RootDir)
			{
				m_IDALPtr = DALObj;
				m_RootDir = RootDir;
			}

			transport::Result CreateProject(const std::string& Name, ProjectId&  Id, ID OwnerId)
			{
				BOOST_LOG_NAMED_SCOPE("ProjectManager: CreateProject");

				BOOST_LOG_SEV(slg, normal) << "Name: " << Name << ";  OwnerId: " << OwnerId;
				IDAL::ResType res = m_IDALPtr->CreateProject( Name,  OwnerId, Id);
				Result ret;
				if (res != transport::Success)
				{
					
					BOOST_LOG_SEV(slg, error) << " DB error";
					ret.RetCode = transport::DBError;
				}
				else
				{
					BOOST_LOG_SEV(slg, normal) << "OK!    Name: " << Name << ";  OwnerId: " << OwnerId << ";  Id: " << Id;
				}
				return ret;
			}

			transport::Result UploadResource(const transport::ResourceData& data, const transport::ByteBuffer& buff, ID UploaderId)
			{
				BOOST_LOG_NAMED_SCOPE("ProjectManager: UploadResource");
				Result ret;
				IDAL::ResType res = m_IDALPtr->RegisterResouce(UploaderId, data);
				if (res == IDAL::eFailed)
				{
					ret.RetCode = transport::DBError;
					BOOST_LOG_SEV(slg, error) << " DB error" ;
				}

				fs::path  p(m_RootDir);
				std::ofstream ofs;
				std::string fullPath = (p / boost::algorithm::hex(data.Resource.Hash) ).string();

				FILE* f = fopen(fullPath.c_str(), "w + b");
				if (f)
				{
					if (fwrite(buff.Data(), 1, buff.GetSize(), f) < buff.GetSize())
					{
						BOOST_LOG_SEV(slg, error) << " can not file write  " << fullPath;
						ret.RetCode = transport::CommonServerError;
					}
					else
					{
						BOOST_LOG_SEV(slg, normal) << "OK!    ";
					}
					fclose(f);
				}
				else
				{
					BOOST_LOG_SEV(slg, error) << " can not open file " << fullPath;
					ret.RetCode = transport::CommonServerError;
				}
				return ret;
			}

			transport::Result  GetResources(ResourceInfoV& resources, ProjectId&  Id, ID  UploaderId)
			{
				BOOST_LOG_NAMED_SCOPE("ProjectManager: GetResources");
				Result ret;
				IDAL::ResType res = m_IDALPtr->GetResources(resources, Id, UploaderId);
				if (res != transport::Success)
				{
					ret.RetCode = transport::DBError;
					BOOST_LOG_SEV(slg, error) << " DB error";
				}
				return ret;
			}

			transport::Result DownloadResource(const transport::ResourceInfo& data, transport::ByteBuffer& buff)
			{
				BOOST_LOG_NAMED_SCOPE("ProjectManager: DownloadResource");
				Result ret;
				fs::path  p(m_RootDir);
				std::ifstream ifs;
				std::string fullPath = (p / boost::algorithm::hex(data.Hash)).string();

				FILE* f = fopen(fullPath.c_str(),"rb");
				if (f)
				{
					fseek(f, 0, SEEK_END); 
					size_t size = ftell(f); // TODO big file
					fseek(f, 0, SEEK_SET); 

					BOOST_LOG_SEV(slg, normal) << "file size: " << size;
					buff.Resize(size);
					
					if (fread(buff.Data(), 1, size, f) < size)
					{
						BOOST_LOG_SEV(slg, error) << " can not file write  " << fullPath;
						ret.RetCode = transport::CommonServerError;
					}
					else
					{
						BOOST_LOG_SEV(slg, normal) << "OK!    ";
					}

					fclose(f);
				}
				else
				{
					BOOST_LOG_SEV(slg, error) << " can not open file " << fullPath;
					ret.RetCode = transport::CommonServerError;
				}
			
				return ret;
			}

		private:
			IDALPtr m_IDALPtr;
			std::string m_RootDir;
			src::severity_logger< SeverityLevel > slg;
		};

		typedef boost::shared_ptr <ProjectManager> ProjectManagerPtr;

	}//!namespace server

}//!namespace survey