#pragma once
#include "DAL.h"
#include "session_manager.h"
#include "project_manager.h"




namespace survey
{
	namespace transport
	{
		class CommonServiceAccess
		{
		public:

			IDALPtr GetDAL()
			{
				return m_DALPtr;
			}

			void SetDAL(IDALPtr DALObj)
			{
				m_DALPtr = DALObj;
			}

			void SetSessionManager(SessionManagerPtr SMPtr)
			{
				m_SessionManagerPtr = SMPtr;
			}

			SessionManagerPtr GetSessionManager()
			{
				return m_SessionManagerPtr;
			}

			void SetPM(server::ProjectManagerPtr PMPtr)
			{
				m_ProjectManagerPtr = PMPtr;
			}

			server::ProjectManagerPtr GetPM()
			{
				return m_ProjectManagerPtr;
			}

		private:
			IDALPtr m_DALPtr;
			SessionManagerPtr m_SessionManagerPtr;
			server::ProjectManagerPtr m_ProjectManagerPtr;
		};
		typedef boost::shared_ptr <CommonServiceAccess>  CommonServiceAccessPtr;

	}//!namespace transport

}//!namespace survey
