#pragma once
#include <string>
#include "../../Include/transport/transport_types.h"

//
//#pragma warning(push)
//#pragma warning(disable : 4512)
//#include <boost/program_options.hpp>
//#pragma warning(pop)
////#include <fstream>
////#include <boost/filesystem.hpp>
#include "../../Include/transport/transport_settings.h"


namespace survey
{


	class DBSettings
	{
		
	public:
		//"localhost", 3306, "root", "1", "test"
		DBSettings() :
			m_Host("localhost")
			, m_Port(3306)
			, m_Login("root")
			, m_Password("1111")
			, m_DBName("test")
		{}


		DBSettings(const DBSettings& obj)
		{
			operator = (obj);
		}

		DBSettings& operator =(const DBSettings& obj)
		{
			if(&obj != this )
			{
				m_Host = obj.m_Host;
				m_Port = obj.m_Port;
				m_Login = obj.m_Login;
				m_Password = obj.m_Password;
				m_DBName = obj.m_DBName;
			}
			return *this;
		}

		const std::string& Host(){ return m_Host; }
		int Port(){ return m_Port; }
		const std::string& Login(){ return m_Login; }
		const std::string& Password(){ return m_Password; }
		const std::string& DBName(){ return m_DBName; }


		bool Load(std::string FileName);

		
	private:
		std::string m_Host;
		int			m_Port;
		std::string m_Login;
		std::string m_Password;
		std::string m_DBName;

	};



	namespace transport
	{

		class ServerSettings
		{
		public:
			ServerSettings() :m_FileStorage("file_storage"){}
			bool Load(std::string FileName);

			TransportSettings m_TransportSettings;
			DBSettings m_DBSettings;
			std::string m_FileStorage;
		};

	}//!namespace transport

}//!namespace survey

