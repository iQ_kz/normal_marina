#include "stdafx.h"
#include "transport_client_core.h"
#include <boost/algorithm/hex.hpp >



namespace  survey
{
	namespace transport
	{


		void ClientTransport::Init(ITransportClientRep* RepObj, EConnectionType ConnType /*= eNative*/) // 
		{
			TransportSettings settings;
			settings.SetConnectionType(ConnType);
			InitInternal(RepObj, settings);
		}

		void ClientTransport::InitInternal(ITransportClientRep* RepObj, const TransportSettings& settings)
		{
		
			ITransportClientRepWrapper::Init(RepObj);
			if (!m_ThreadManagerPtr)
			{
				m_ThreadManagerPtr = boost::make_shared<ThreadManager>();

				m_WorkManager.Init(m_ThreadManagerPtr, 2);

				m_TransportCore.Init(m_ThreadManagerPtr, 1);

				m_TCP_Connector.Init(m_ThreadManagerPtr, 1);

				m_TCP_Connector.m_WorkerPtr->SetHandler(m_TransportCore.m_WorkerPtr.get());

			}

			m_TransportCore.m_WorkerPtr->Init(settings);


			m_TransportCore.m_WorkerPtr->SetHandler(this);

			m_TimerManager.Init(m_ThreadManagerPtr, 1);
			m_TimerManager.m_WorkerPtr->SetHandler(boost::bind(&ClientTransport::OnTimer, this, _1));
		}

		bool ClientTransport::SetTempDirectory(const std::string& tempDirectory) // 
		{
			m_TempDirectory = tempDirectory;
			
			return true;
		}

		void ClientTransport::SetDownloadDir(const std::string& downloadDirectory)
		{
			m_DownloadDir = downloadDirectory;
			m_TransportCore.m_WorkerPtr->SetDownloadDir(downloadDirectory);
		}

		void ClientTransport::Connect(const char* host, long Port) //
		{
			if (m_TCP_Connector.m_WorkerPtr)
			{
				std::string PortStr = boost::lexical_cast<std::string>(Port);

				m_TCP_Connector.m_WorkerPtr->Connect(host, PortStr);
			}
			else
			{
				BOOST_LOG_SEV(slg, critical) << " ClientTransport:  It requires initialization";
			}
		}

		void ClientTransport::Login(const char* login, const char* password)
		{
			if (m_TransportCore.m_WorkerPtr)
			{
				ServerProxy<> Serv(m_TransportCore.m_WorkerPtr->GetSession());
				Serv.Login(login, password, 0);//TODO Request ID;
			}
			else
			{
				BOOST_LOG_SEV(slg, critical) << " Login:  It requires initialization";
			}
		}

		void ClientTransport::RegisterTimeoutEvent(unsigned int milliseconds, ID eventId)
		{
			m_TimerManager.m_WorkerPtr->RegisterTimer(milliseconds, eventId);
		}

		bool ClientTransport::IsConnected() // 
		{
			return m_TransportCore.m_WorkerPtr->IsConnected();
		}

		bool ClientTransport::LoggedOn() //
		{
			return m_TransportCore.m_WorkerPtr->LoggedOn();
		}

		void ClientTransport::CreateProject(const std::string& Name)
		{
			ServerProxy<> Serv(m_TransportCore.m_WorkerPtr->GetSession());
			Serv.CreateProject(Name, 0);//TODO Request ID;
		}

		bool ClientTransport::UploadFile(const std::string& hash, const std::string& Name, const boost::filesystem::path& FilePath, UploadProjID Id)
		{
			bool ret = true;

			boost::filesystem::path srcFileName =  FilePath.filename();

			boost::filesystem::path tempPathFile = boost::filesystem::path(m_TempDirectory)/srcFileName;

			boost::filesystem::copy_file(FilePath, tempPathFile);

			BOOST_LOG_NAMED_SCOPE("ClientTransport: UploadFile");

			std::ifstream ifs;
			std::string fullPath = (tempPathFile).string();/// boost::algorithm::hex( hash)
			ifs.open(fullPath, std::ios::binary);
			if (ifs.is_open())
			{
				size_t Size = fs::file_size(fullPath); //TODO
				ByteBufferPtr Buff = ByteBuffer::Create();
				Buff->Resize(Size);
				ifs.read((char*)Buff->Data(), Size);

				ID RequestID = NextRequestId();
				m_SynchronizationManager.AddRequest(hash, Name, tempPathFile, Id);
			//	m_ID2HashMap[RequestID] = hash;

				ServerProxy<> Serv(m_TransportCore.m_WorkerPtr->GetSession());
				ResourceData data;
				data.Resource.Hash = hash;
				data.Resource.Name = Name;
				data.Id = Id.ServerID();
				Serv.UploadResource(data, *Buff, RequestID);
			}
			else
			{
				BOOST_LOG_SEV(slg, error) << " can not open file " << fullPath;
				ret = false;
			}
			return ret;
		}

		survey::ID ClientTransport::NextRequestId()
		{
			static std::atomic <ID> Id = 0;
			return  ++Id;
		}

		void ClientTransport::OnTimer(ID TimerId)
		{
			ITransportClientRepWrapper::m_Rep->OnTimeoutEvent(TimerId);
		}

		void ClientTransport::OnUploadFile(const Result& res, ID ProjId, const std::string& fileHash, const std::string& Name)
		{
			if (Success == res.RetCode)
			{
				m_SynchronizationManager.RemoveRequest(ProjId,fileHash,  Name);
			}
			
			RepWrap::OnUploadFile(res, ProjId, fileHash, Name);
		}

		bool ClientTransport::ProjectQueueIsEmpty(GlobalID ProjId)
		{
			return m_SynchronizationManager.TaskIsEmpty(ProjId);
		}

		void ClientTransport::GetFile(ID ProjId, const std::string& fileHash, const std::string& Name)
		{
			ResourceData data;
			data.Id = ProjId;
			data.Resource.Hash = fileHash;
			data.Resource.Name = Name;

			ServerProxy<> Serv(m_TransportCore.m_WorkerPtr->GetSession());

			ID RequestID = NextRequestId();

			Serv.DownloadResource(data, RequestID);
		}

		//void ClientTransport::OnGetFile(const Result& res, const std::string& hash, const std::string& Name, const boost::filesystem::path& FilePath, UploadProjID Id)
		//{


		//}

		void ClientTransport::GetProjectList()
		{
			ServerProxy<> Serv(m_TransportCore.m_WorkerPtr->GetSession());
			ID RequestID = NextRequestId();
			Serv.GetProjectList(RequestID);
		}

		void ClientTransport::GetUserList()
		{
			ServerProxy<> Serv(m_TransportCore.m_WorkerPtr->GetSession());
			ID RequestID = NextRequestId();

			Serv.GetUserList(RequestID);

		}

	} //!transport

};	//!survey