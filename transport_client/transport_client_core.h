#pragma once

#include "transport_client_interface.h"
#include "client_side.h"
#include <boost/lexical_cast.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <unordered_map>

#include "../include/logger.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <atomic>

#include "timer_manager.h"
#include "SynchronizationManager.h"

namespace fs = boost::filesystem;



namespace  survey
{
	namespace transport
	{



		class ITransportClientRepWrapper : public ITransportClientRep
		{
		public:
			ITransportClientRepWrapper() :m_Rep(nullptr){}


			void Init(ITransportClientRep* RepObj)
			{
				m_Rep = RepObj;
			}
			ITransportClientRep* m_Rep;

			virtual void OnConnect()
			{
				m_Rep->OnConnect();
			}

			virtual void OnDisconnect()
			{
				m_Rep->OnDisconnect();
			}

			virtual void OnError(const std::string msg)
			{
				m_Rep->OnError(msg);
			}

			//////////////////////////////////////////////////////////////////////////

			virtual void OnLogin(const Result& res)
			{
				m_Rep->OnLogin(res);
			}

			virtual void OnCreateProject(const Result& res, const ProjectId& Id)
			{
				m_Rep->OnCreateProject(res, Id);
			}


			void OnUploadFile(const Result& res, ID ProjId, const std::string& fileHash, const std::string& Name)
			{
				m_Rep->OnUploadFile(res,  ProjId,  fileHash, Name);
			}

			virtual void OnTimeoutEvent(ID eventId)
			{
				m_Rep->OnTimeoutEvent(eventId);
			}

			virtual void OnGetFile(const Result& res, const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath)
			{
				m_Rep->OnGetFile(res, hash, Name, FilePath);
			}

			virtual void OnGetProjectList(const Result& res, const ProjectInfoV& pr)
			{
				m_Rep->OnGetProjectList(res, pr);
			}

			virtual void OnGetUserList(const Result& res, const UserInfoV& ui)
			{
				m_Rep->OnGetUserList(res, ui);
			}

		};

		/*
		*# ���� ����������� ����������. ������������ ����� � �������� �� ���������� ITransportClient
		*
		*/
		class ClientTransport : public ITransportClient, public 	ITransportClientRepWrapper
			{
		public:
			ClientTransport() :ITransportClientRepWrapper(){}
			typedef ITransportClientRepWrapper RepWrap;

			typedef std::unordered_map<ID, std::string> ID2HashMap;

			//* ITransportClient::Init  implementation

			virtual void Init(ITransportClientRep* RepObj, EConnectionType ConnType = eNative);// override;

			virtual void InitInternal(ITransportClientRep* RepObj, const TransportSettings& settings);

			virtual bool SetTempDirectory(const std::string& tempDirectory); // override;

			void SetDownloadDir(const std::string& downloadDirectory);

			virtual void Connect(const char* host, long Port);	//override;

			void ClientTransport::OnConnect()
			{
				ConnectionPtr conn = m_TransportCore.m_WorkerPtr->GetSession();
				if (conn)

				{
					conn->DoRead();
				}
				else
				{
					BOOST_LOG_SEV(slg, error) << " no connection " ;
				}
				ITransportClientRepWrapper::OnConnect();
				
			}

			/*#�����������
			*/
			virtual void Login(const char* login, const char* password);

			virtual void Commit(const CommitData& commit, Version& versCommit)
			{

			}

			virtual void RegisterTimeoutEvent(unsigned int milliseconds, ID eventId);

			virtual bool IsConnected();

			virtual bool LoggedOn();

			virtual void OpenProject(const ProjectId& Id)
			{

			}

			virtual void CreateProject(const std::string& Name );

			bool UploadFile(const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath, UploadProjID Id);

			virtual bool ProjectQueueIsEmpty(GlobalID ProjId);

			void GetFile(ID ProjId, const std::string& fileHash, const std::string& Name);

		//	virtual void OnGetFile(const Result& res, const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath, UploadProjID Id);




			virtual void GetProjectList();

			virtual void GetUserList();



		private:		

			void OnUploadFile(const Result& res, ID ProjId, const std::string& fileHash, const std::string& Name);


			ID NextRequestId();

			void OnTimer(ID TimerId);

			IOWorker <ClientTransportCore>	   m_TransportCore;

			IOWorker <TCP_Connector> m_TCP_Connector;

			ThreadManagerPtr m_ThreadManagerPtr;

			IOWorker <WorkManager>	m_WorkManager;

			IOWorker <TimerManager> m_TimerManager;

			src::severity_logger< SeverityLevel > slg;

			std::string m_TempDirectory;
			std::string m_DownloadDir;

			SynchronizationManager m_SynchronizationManager;
			//ID2HashMap m_ID2HashMap;
		};
	} //!transport

};	//!survey
