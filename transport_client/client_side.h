#pragma once


#include "../Include/transport_client_interface.h"

#include <iostream>

#include <vector>
#include <boost/shared_ptr.hpp>
#include <deque>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/thread.hpp>
#include <boost/asio.hpp>

#include <boost/timer.hpp>
#include <boost/chrono.hpp>
#include <boost/make_shared.hpp>
#include <boost/function.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>

#include "../Include/ID_Genearator.h"
#include "../Include/transport_client_interface.h"
#include "../Include/transport/transport_types.h"
#include "../Include/transport/survey_serialization.h"
#include "../Include/transport/transport_core.h"
#include "../Include/transport/transport_settings.h"
#include <boost/algorithm/hex.hpp >

#include "../include/logger.h"

namespace fs = boost::filesystem;


//
//using namespace  survey;
//
//using namespace  survey::transport;

namespace survey
{
	namespace transport
	{
		/*#������- ������ ��� ������ ������� �������.
		*/
		template <typename ConnectionPtrType = ConnectionPtr >
		class ServerProxy
		{
		public:

			void Send(ByteBufferPtr Buff)
			{
				if (m_ConnectionPtr)
				{
					m_ConnectionPtr->Send(Buff);
				}
				else
				{
					BOOST_LOG_SEV(slg, error) << "no connection ";
				}
			}

			typedef ConnectionPtrType ConnectionPtrType;
			ServerProxy(ConnectionPtrType SPtr) :m_ConnectionPtr(SPtr){}
			ServerProxy(){}

			ServerProxy& operator =(const ServerProxy& obj)
			{
				if (this != &obj)
				{
					m_ConnectionPtr = obj.m_ConnectionPtr;
				}
				return *this;
			}

			void Login(const std::string& login, const std::string&  password, ID RequestID)
			{
				BOOST_LOG_NAMED_SCOPE("ServerProxy: Login");
				try
				{
					ITransportClient::MethodId MID = ITransportClient::eLogin;
					ByteBufferPtr Buff = ByteBuffer::Create();
					Write(*Buff, MID);
					Write(*Buff, login);
					Write(*Buff, password);

					//////////////////////////////////////////////////////////////////////////
					Write(*Buff, RequestID);

					Send(Buff);
					
				}
				catch (std::exception& e)
				{
					BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
					throw;
				}
				catch (...)
				{
					BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
					throw;
				}
			}

			void CreateProject(const std::string& Name, ID RequestID)
			{
				BOOST_LOG_NAMED_SCOPE("ServerProxy: CreateProject");
				try
				{
					ITransportClient::MethodId MID = ITransportClient::eCreateProject;
					ByteBufferPtr Buff = ByteBuffer::Create();
					Write(*Buff, MID);
					Write(*Buff, Name);
					
					//////////////////////////////////////////////////////////////////////////
					Write(*Buff, RequestID);

					Send(Buff);
				}
				catch (std::exception& e)
				{
					BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
					throw;
				}
				catch (...)
				{
					BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
					throw;
				}
			}

			void UploadResource(const ResourceData& data, const ByteBuffer& buff, ID RequestID)
			{
				BOOST_LOG_NAMED_SCOPE("ServerProxy: UploadResource");
				try
				{
					ITransportClient::MethodId MID = ITransportClient::eUploadFile;
					ByteBufferPtr Buff = ByteBuffer::Create();
					Write(*Buff, MID);
					Write(*Buff, data);
					Write(*Buff, buff);
					
					//////////////////////////////////////////////////////////////////////////
					Write(*Buff, RequestID);
					
					Send(Buff);
				}
				catch (std::exception& e)
				{
					BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
					throw;
				}
				catch (...)
				{
					BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
					throw;
				}
			}

			void DownloadResource(const ResourceData& data, ID RequestID)
			{
				BOOST_LOG_NAMED_SCOPE("ServerProxy: DownloadResource");
				try
				{
					ITransportClient::MethodId MID = ITransportClient::eGetFile;
					ByteBufferPtr Buff = ByteBuffer::Create();
					Write(*Buff, MID);
					Write(*Buff, data);

					//////////////////////////////////////////////////////////////////////////
					Write(*Buff, RequestID);
					
					Send(Buff);
				}
				catch (std::exception& e)
				{
					BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
					throw;
				}
				catch (...)
				{
					BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
					throw;
				}
			}


			void GetProjectList(ID RequestID)
			{
				BOOST_LOG_NAMED_SCOPE("ServerProxy: GetProjectList");
				try
				{
					ITransportClient::MethodId MID = ITransportClient::eGetProjectList;
					ByteBufferPtr Buff = ByteBuffer::Create();
					Write(*Buff, MID);
	
					//////////////////////////////////////////////////////////////////////////
					Write(*Buff, RequestID);
					
					Send(Buff);
				}
				catch (std::exception& e)
				{
					BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
					throw;
				}
				catch (...)
				{
					BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
					throw;
				}
			}


			void GetUserList(ID RequestID)
			{
				BOOST_LOG_NAMED_SCOPE("ServerProxy: GetUserList");
				try
				{
					ITransportClient::MethodId MID = ITransportClient::eGetUserList;
					ByteBufferPtr Buff = ByteBuffer::Create();
					Write(*Buff, MID);

					//////////////////////////////////////////////////////////////////////////
					Write(*Buff, RequestID);
					
					Send(Buff);
				}
				catch (std::exception& e)
				{
					BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
					throw;
				}
				catch (...)
				{
					BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
					throw;
				}
			}

		private:
			ConnectionPtrType m_ConnectionPtr;
			src::severity_logger< SeverityLevel > slg;
		};

		/*#���������� ��������� �������, ����� �������������� �������� ������*/
		//������ ��� ��������� ����-������������
		template <typename ConnectionPtrType = ConnectionPtr >
		class ClientHandler 
		{
		public:
			typedef ConnectionPtrType ConnectionPtrType;
			ClientHandler() : m_pExternalHandler(NULL), m_bLoggedOn(false){}

			//����������� �������� ������� ����� �����.
			void Init(ConnectionPtrType SPtr)
			{
				m_ConnectionPtr = SPtr;
				m_bLoggedOn = false;
			}

			virtual void OnLogin(const Result& res, ID RequestID)
			{
				m_bLoggedOn = (Success == res.RetCode);
				m_pExternalHandler->OnLogin(res);
			}

			virtual void OnCreateProject(const Result& res, const ProjectId& Id, ID RequestID)
			{
				m_pExternalHandler->OnCreateProject(res, Id);
			}

			void OnUploadFile(const Result& res, ID ProjId, const std::string& fileHash, const std::string& Name, ID RequestID)
			{
				m_pExternalHandler ->OnUploadFile( res,  ProjId,  fileHash,  Name);
			}

			void OnGetFile(const Result& res, ResourceData data, ByteBufferPtr body, ID RequestID)
			{
				Result newRes = res;
				fs::path  p(m_DownloadDir);
				std::ofstream ofs;
				std::string fullPath = (p / boost::algorithm::hex(data.Resource.Hash)).string();

				FILE* f = fopen(fullPath.c_str(), "w + b");
				if (f)
				{
					if (fwrite(body->Data(), 1, body->GetSize(), f) < body->GetSize())
					{
						BOOST_LOG_SEV(slg, error) << " can not file write  " << fullPath;
						newRes.RetCode = transport::ClientFilesyStemError;
					}
					else
					{
						BOOST_LOG_SEV(slg, normal) << "OK!    ";
					}
					fclose(f);
				}
				else
				{
					BOOST_LOG_SEV(slg, error) << " can not open file " << fullPath;
					newRes.RetCode = transport::ClientFilesyStemError;
				}
				m_pExternalHandler->OnGetFile(res, data.Resource.Hash, data.Resource.Name, fullPath); 
			}

			void SetExternHandler(ITransportClientRep* ExternHandler)
			{
				m_pExternalHandler = ExternHandler;
			}
			void Logout()
			{
				m_bLoggedOn = false;
			}

			bool LoggedOn()
			{
				return m_bLoggedOn;
			}
			void SetDownloadDir(const std::string& downloadDir)
			{
				m_DownloadDir = downloadDir;
			}

		private:
			ITransportClientRep* m_pExternalHandler;
			ConnectionPtrType m_ConnectionPtr;
			bool m_bLoggedOn;
			std::string m_DownloadDir;
			src::severity_logger< SeverityLevel > slg;
		};

		/*#��������� ������� �������, ���������� �������� ����� � ������ ������ �����������.*/
		class ClientStub : public StubBase<ClientHandler <> >
		{
		public:
			typedef StubBase<ClientHandler<> > Base;

			void OnRead(ByteBufferPtr BuffPtr);

			void SetHandler(ITransportClientRep* Handler)
			{
				Base::m_Handler.SetExternHandler(Handler);
			}
			bool LoggedOn(){ return Base::m_Handler.LoggedOn(); }
		};

		/*#������������� ��������� ���������� �� ����������� �������.
		*/
		class ClientCryptContect : public CryprContextManager
		{
		public:
			ClientCryptContect() {}

			void Init()
			{
				CryprContextManager::m_Context->load_verify_file("rootca.crt");
			}

		private:
			std::string get_password() const
			{
				return "";
			}
		};

		/*#���������� ����, ���������� �������������� ����������� �������.*/
		class ClientTransportCore : public IO_Handler, public IConnectManager
		{
		public:		   

			typedef boost::unique_lock<boost::mutex> Lock;
			ClientTransportCore(IO_Svc& IO) :IO_Handler(IO), m_pNativeStubAccess(nullptr), m_ExternHandler(nullptr){}

			void Init(const TransportSettings& settings);

			void OnConnect(ConnectionPtr SPtr);

			bool IsConnected()
			{
				Lock Locker(m_mutex);
				return m_Session.get() != NULL;
			}

			bool LoggedOn()
			{
				Lock Locker(m_mutex);
				bool ret = false;
				if (m_pNativeStubAccess)
				{
					ret = m_pNativeStubAccess->LoggedOn();
				}
				return ret;
			}

			ConnectionPtr GetSession()
			{
				Lock Locker(m_mutex);
				return m_Session;
			}

			void SetHandler(ITransportClientRep* Handler);
			
			void SetDownloadDir(const std::string& downloadDirectory)
			{
				m_downloadDirectory = downloadDirectory;
			}

			virtual void OnDisconnect(ConnectionPtr SPtr)override;

			virtual void OnError(const std::string msg, const boost::system::error_code& Error, ID Id)override;

			virtual ConnectionPtr CreateSession()	override;

			void OnConnectWilligness(ConnectionPtr SPtr);

		private:

			StubPtr m_StubHolder;
			ConnectionPtr m_Session;
			ClientStub* m_pNativeStubAccess;
			ITransportClientRep* m_ExternHandler;
			TransportSettings m_Settings;
			ClientCryptContect m_ClientCryptContect;

			src::severity_logger< SeverityLevel > slg;

			boost::mutex m_mutex;

			std::string m_downloadDirectory;
		};

	}//!namespace transport

}//!namespace survey


