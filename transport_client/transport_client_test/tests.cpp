#include "stdafx.h"
#include "tests.h"

#include "../client_side.h"


#include "../transport_client_core.h"
#include "../timer_manager.h"
#include "test_utils.h"
#include "../../include/transport/survey_serialization.h"

namespace survey
{
	namespace transport
	{


		void survey::transport::LoginSerializationTest()
		{
			ByteBufferPtr Buff = ByteBuffer::Create();

			std::string Login = "test";
			std::string Password = "111111";
			ITransportClient::MethodId id = ITransportClient::eLogin;
			size_t id2 = 1;


			{

				std::cout << "LoginSerinalizationTest" << std::endl;

				Write(*Buff, id);
				Write(*Buff, Login);
				Write(*Buff, Password);
				Write(*Buff, id2);


				std::string Login_ = "";
				std::string Password_ = "";
				ITransportClient::MethodId id_ = ITransportClient::ePing;
				size_t id2_ = 0;
				Buff->SetStart();

				Read(*Buff, id_);
				Read(*Buff, Login_);
				Read(*Buff, Password_);
				Read(*Buff, id2_);
				CHECK(id, id, id_);
				CHECK(Login, Login, Login_);
				CHECK(Password, Password, Password_);
				CHECK(id2, id2, id2_);
			}
			std::cout << std::endl << std::endl;
		}


		void OnTimer(size_t id)
		{
			std::cout << "OnTimer, id = " << id << std::endl;

		}

		void survey::transport::TestTimer()
		{
			ThreadManagerPtr m_ThreadManagerPtr;
			m_ThreadManagerPtr = boost::make_shared<ThreadManager>();
			IOWorker <TimerManager> m_TimerManager;
			m_TimerManager.Init(m_ThreadManagerPtr, 1);
			m_TimerManager.m_WorkerPtr->SetHandler(OnTimer);
			//m_TimerManager.m_WorkerPtr->OnTimeout
			m_TimerManager.m_WorkerPtr->RegisterTimer(10000, 1);
			m_TimerManager.m_WorkerPtr->RegisterTimer(3000, 2);
			std::cin.get();
		}

		void ProjectListSerializationTest()
		{
			ProjectInfo info;

			info.Autor = 10;
			info.Name = "testName";
			info.Id = 12;
			ProjectInfoV container1, container2;
			container1.push_back(info);

			info.Name = "1testName1";
			info.Autor = 111;
			info.Id = 44;
			container1.push_back(info);

			ByteBufferPtr Buff = ByteBuffer::Create();

			Write(*Buff, container1);

			Buff->SetStart();

			
			Read(*Buff,container2);

			CHECK(ProjectListSerializationTest,container1, container2);

		}

		void UserListSerializationTest()
		{
			UserInfo info1, info2;
			info1.Id = 1;
			info1.Login = "testLogin";
			info1.Name = "testName";
			info1.Password = "testPassword";
			info1.State = 121;


			info1.Id = 1;
			info1.Login = "2testLogin2";
			info1.Name = "2testName2";
			info1.Password = "2testPassword2";
			info1.State = 343;

			UserInfoV container1, container2;

			container1.push_back(info1);
			container1.push_back(info2);

			ByteBufferPtr Buff = ByteBuffer::Create();


			Write(*Buff, container1);

			Buff->SetStart();


			Read(*Buff, container2);

			CHECK(UserListSerializationTest, container1, container2);
		}

	}//!namespace transport

}//!namespace survey
