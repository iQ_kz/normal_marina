#include "stdafx.h"
#include "net_tests.h"
#include <iostream>
#include <exception>

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp> 

#include "boost/thread/thread.hpp"
#include "boost/thread/xtime.hpp"

#include "../client_side.h"

#include "../transport_client_core.h"
#include "../../include/common_types.h"
#include "../../survey_server/server_handler.h"
#include "../../survey_server/server_side.h"

#include "..\..\Include\test_utils.h"
#include "mock_objects.h"


#include "../../include/Cryptography.h"






namespace fs = boost::filesystem;

using namespace survey;

using namespace survey::transport;

void TestClientCore()
{
	std::cout << "TestClientCore: begin " << std::endl;
	std::cin.get();

	ClientTransport Ct;
	TestClientHandler  ClientHandlerObj;

	TransportSettings s;
	s.SetPort("801");


	Ct.InitInternal(&ClientHandlerObj, s);
	Ct.Connect("localhost", 801);


	while (!ClientHandlerObj.m_IsConnected)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting connection " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
	}

	std::string Login = "user";
	std::string Password = "pass1";
	size_t RequestCounter = 0;;
	++RequestCounter;
	std::cout << "thread: " << boost::this_thread::get_id() << "; send Login request: Login =  " << Login << "; Password = " << Password << "; RequestId = " << RequestCounter << std::endl;
	Ct.Login(Login.c_str(), Password.c_str());


	while (ClientHandlerObj.m_AnswerCounter < 1)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting Login answer..... " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
	}
	std::cout << "thread: " << boost::this_thread::get_id() << ";LastResult.RetCode  = " << CodeInfo::Convert(ClientHandlerObj.m_LastResult.RetCode) << std::endl;
	CHECK(LoginFailed, ClientHandlerObj.m_LastResult.RetCode, WrongPassword);


	Password = "pass";
	++RequestCounter;
	std::cout << "thread: " << boost::this_thread::get_id() << "; send Login request: Login =  " << Login << "; Password = " << Password << "; RequestId = " << RequestCounter << std::endl;
	Ct.Login(Login.c_str(), Password.c_str());

	while (ClientHandlerObj.m_AnswerCounter < 2)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting Login answer..... " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
	}

	std::cout << "thread: " << boost::this_thread::get_id() << ";LastResult.RetCode  = " << CodeInfo::Convert(ClientHandlerObj.m_LastResult.RetCode) << std::endl;

	std::cout << "-------------------------------------------------" << std::endl;

	std::cout << "TestClientCore: end" << std::endl;
	CHECK(LoginSuccess, ClientHandlerObj.m_LastResult.RetCode, Success);

	std::cin.get();
}

void LocalNetLoginTest()
{
	std::cout << "start client" << std::endl;
	std::cin.get();

	boost::shared_ptr<IO_Svc> IOClient(new IO_Svc);
	boost::shared_ptr <IO_Svc::work> ClientWork(new IO_Svc::work(*IOClient));
	ClientTransportCore ClientCore(*IOClient);
	TestClientHandler ClientHandlerObj;
	ClientCore.SetHandler(&ClientHandlerObj);

	TCP_Connector ClientConnector(*IOClient);


	ClientConnector.SetHandler(&ClientCore);

	ClientConnector.Connect("localhost", "801");
	std::cout << "client started" << std::endl;

	boost::thread ClientNetThread(boost::bind(&IO_Svc::run, IOClient));

	//��� ��������
	while (!ClientCore.IsConnected())
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting connection " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
	}

	ServerProxy<> Server(ClientCore.GetSession());
	std::string Login = "test";
	std::string Password = "111112";
	size_t RequestCounter = 0;;
	++RequestCounter;
	std::cout << "thread: " << boost::this_thread::get_id() << "; send Login request: Login =  " << Login << "; Password = " << Password << "; RequestId = " << RequestCounter << std::endl;
	Server.Login(Login, Password, RequestCounter);

	//��� ������
	while (ClientHandlerObj.m_AnswerCounter < 1)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting Login answer..... " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
	}
	std::cout << "thread: " << boost::this_thread::get_id() << ";LastResult.RetCode  = " << CodeInfo::Convert(ClientHandlerObj.m_LastResult.RetCode) << std::endl;


	Password = "111111";
	++RequestCounter;
	std::cout << "thread: " << boost::this_thread::get_id() << "; send Login request: Login =  " << Login << "; Password = " << Password << "; RequestId = " << RequestCounter << std::endl;
	Server.Login(Login, Password, RequestCounter);

	//��� ������� ������
	while (ClientHandlerObj.m_AnswerCounter < 2)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting Login answer..... " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
	}

	std::cout << "thread: " << boost::this_thread::get_id() << ";LastResult.RetCode  = " << CodeInfo::Convert(ClientHandlerObj.m_LastResult.RetCode) << std::endl;

	std::cout << "-------------------------------------------------" << std::endl;
}

void UploadFileNetTest(fs::path& rootPath)
{
	std::cout << "UploadFileNetTest: begin " << std::endl;
	std::cin.get();

	ClientTransport Ct;
	TestClientHandler  ClientHandlerObj;

	TransportSettings s;
	s.SetPort("801");


	Ct.InitInternal(&ClientHandlerObj, s);
	Ct.Connect("localhost", 801);


	while (!ClientHandlerObj.m_IsConnected)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting connection " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
	}

	std::string Login = "user";
	std::string Password = "pass";
	size_t RequestCounter = 0;;
	++RequestCounter;
	std::cout << "thread: " << boost::this_thread::get_id() << "; send Login request: Login =  " << Login << "; Password = " << Password << "; RequestId = " << RequestCounter << std::endl;
	Ct.Login(Login.c_str(), Password.c_str());


	while (ClientHandlerObj.m_AnswerCounter < 1)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting Login answer..... " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
	}
	std::cout << "thread: " << boost::this_thread::get_id() << ";LastResult.RetCode  = " << CodeInfo::Convert(ClientHandlerObj.m_LastResult.RetCode) << std::endl;




	std::cout << "thread: " << boost::this_thread::get_id() << ";LastResult.RetCode  = " << CodeInfo::Convert(ClientHandlerObj.m_LastResult.RetCode) << std::endl;

	std::cout << "-------------------------------------------------" << std::endl;

	std::cout << "TestClientCore: end" << std::endl;

	//������ ������.

	Ct.CreateProject("Project1");
	//��� ������
	while (ClientHandlerObj.m_AnswerCounter < 2)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting CreateProject answer..... " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
	}


	// ������� �������� ����.
	std::ofstream ofs;
	std::string TestFileName = "testFile";
	fs::path fullTestFile = (rootPath / TestFileName);
	ofs.open(fullTestFile.string(), std::ios::binary);
	std::string fileData = " qwertyuiop[]asdfghjkl;'";
	std::string FileHash = CalculateMd5(fileData);


	if (!ofs.is_open())
	{
		std::cout << " can not open file  " << fullTestFile.string() << std::endl;

		std::cout << "END" << std::endl;

		std::cout << "-------------------------------------------------" << std::endl;

		return;
	}

	ofs.write(fileData.c_str(), fileData.size());
	ofs.close();
	Ct.UploadFile(FileHash, "", fullTestFile, ClientHandlerObj.m_ProjectId);


	//��� ������
	while (ClientHandlerObj.m_AnswerCounter < 3)
	{
		std::cout << "thread: " << boost::this_thread::get_id() << "; waiting UploadFile answer..... " << std::endl;

		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
	}



	//	Ct.UploadFile();




	CHECK(LoginSuccess, ClientHandlerObj.m_LastResult.RetCode, Success);

	std::cin.get();
}

void CreateFile(const std::string& fullFileName, const std::string& fileBody )
{
	std::ofstream ofs;

	ofs.open(fullFileName, std::ios::trunc | std::ios::binary);
	if (ofs.is_open())
	{
		ofs << fileBody;
	}
	else
	{
		throw std::logic_error("can not open file " + fullFileName);
	}
}

void  CreateTestFile(fs::path& outFileName, std::string& fileHash, const fs::path& rootPath)
{

	std::string testFileName = "testfile1";
	std::string testFileBody = "test file body.";
	std::string str2Text = Cryptography::UUID_Generate();

	testFileBody += str2Text;
	fileHash = CalculateMd5(testFileBody);

	outFileName = (rootPath / boost::algorithm::hex(fileHash));

	// ������� �������� ����.
	CreateFile(outFileName.string(), testFileBody);

}

void DownloadNetTest(fs::path& rootPath)
{


	size_t uploadCount = 100;
	std::cout << "DownloadNetTest: begin " << std::endl;
	std::cin.get();

	try
	{
		//////////////////////////////////////////////////////////////////////////

		ClientTransport Ct;
		Ct.SetTempDirectory((rootPath / "client_storage").string()); 

		Ct.SetDownloadDir((rootPath / "client_download_storage").string());
		TestClientHandler  ClientHandlerObj;

		TransportSettings s;
		s.SetPort("801");


		Ct.InitInternal(&ClientHandlerObj, s);
		Ct.Connect("localhost", 801);


		while (!ClientHandlerObj.m_IsConnected)
		{
			std::cout << "thread: " << boost::this_thread::get_id() << "; waiting connection " << std::endl;

			boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
		}

		std::string Login = "test";
		std::string Password = "test1";
		size_t RequestCounter = 0;;
		++RequestCounter;
		std::cout << "thread: " << boost::this_thread::get_id() << "; send Login request: Login =  " << Login << "; Password = " << Password << "; RequestId = " << RequestCounter << std::endl;
		Ct.Login(Login.c_str(), Password.c_str());


		while (ClientHandlerObj.m_AnswerCounter < 1)
		{
			std::cout << "thread: " << boost::this_thread::get_id() << "; waiting Login answer..... " << std::endl;

			boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
		}
		std::cout << "thread: " << boost::this_thread::get_id() << ";LastResult.RetCode  = " << CodeInfo::Convert(ClientHandlerObj.m_LastResult.RetCode) << std::endl;


		if (Success != ClientHandlerObj.m_LastResult.RetCode)
		{
			std::cout << "autorization error: end" << std::endl;
			std::cin.get();
			return;
		}

		std::cout << "-------------------------------------------------" << std::endl;

		std::cout << "TestClientCore: end" << std::endl;

		//������ ������.
		std::string projectName = Cryptography::UUID_Generate();
		
		Ct.CreateProject(projectName);
		//��� ������
		while (ClientHandlerObj.m_AnswerCounter < 2)
		{
			std::cout << "thread: " << boost::this_thread::get_id() << "; waiting CreateProject answer..... " << std::endl;

			boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
		}

		std::string FileHash;

		fs::path fullTestFile;
		CreateTestFile(fullTestFile, FileHash, rootPath);
		Ct.UploadFile(FileHash, "", fullTestFile, ClientHandlerObj.m_ProjectId);

		fs::remove(fullTestFile);
	


		//��� ������
		while (ClientHandlerObj.m_AnswerCounter < 3)
		{
			std::cout << "thread: " << boost::this_thread::get_id() << "; waiting UploadFile answer..... " << std::endl;

			boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
		}

		CHECK(UploadFile, ClientHandlerObj.m_LastResult.RetCode, Success);
		 
		Ct.GetFile(ClientHandlerObj.m_ProjectId, FileHash, "" );
		

		//��� ������
		while (ClientHandlerObj.m_AnswerCounter < 4)
		{
			std::cout << "thread: " << boost::this_thread::get_id() << "; waiting GetFile answer..... " << std::endl;

			boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
		}


		CHECK(GetFile, ClientHandlerObj.m_LastResult.RetCode, Success);


		std::cout << "group upload; uploadCount = " << uploadCount << std::endl;
		for (size_t i = 0; i < uploadCount; ++i)
		{
			std::string FileHash;

			fs::path fullTestFile;
			CreateTestFile(fullTestFile, FileHash, rootPath);
			Ct.UploadFile(FileHash, "", fullTestFile, ClientHandlerObj.m_ProjectId);

			fs::remove(fullTestFile);

		}

		while (ClientHandlerObj.m_AnswerCounter < 3 + uploadCount)
		{
			std::cout << "thread: " << boost::this_thread::get_id() << "; waiting UploadFile answer..... " << std::endl;

			boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
		}

		bool queueEmpty = Ct.ProjectQueueIsEmpty(ClientHandlerObj.m_ProjectId);

		CHECK(group_upload, queueEmpty, true);

	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "unknown error" << std::endl;
	}	
}
