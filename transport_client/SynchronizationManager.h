#pragma once

#include "transport_client_interface.h"
#include <unordered_map>
#include <unordered_set>

#include "../include/logger.h"

#include <boost/filesystem.hpp>
#include <boost/filesystem/operations.hpp>
#include <atomic>
#include <memory>

namespace fs = boost::filesystem;

namespace  survey
{
	namespace transport
	{
		
		class SynchronizationManager
		{
		public:

			class UploadRequest
			{
			public:
				UploadRequest(const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath):
					m_hash(hash), m_Name(Name), m_FilePath(FilePath)
				{}

				UploadRequest(const UploadRequest& obj)
				{
					operator =(obj);
				}
				UploadRequest& operator =(const UploadRequest& obj)
				{
					m_hash = obj.m_hash;
					m_Name = obj.m_Name;
					m_FilePath = obj.m_FilePath;
					return *this;
				}
				
				std::string  m_hash;
				std::string m_Name;
				boost::filesystem::path  m_FilePath; 
			};


			//����� �������� ��� ����������� �������  IDType ���������� ��c�� ���� guid, �.�. ���������, ���� ����������  �� �������
			//template <typename IDType = ID>
			class Channel
			{
			public:
				//typedef IDType IDType;

				typedef std::unordered_map <std::string, UploadRequest> RequestContainer;
				RequestContainer m_UploadRequestMap;

				void AddRequest(const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath)
				{
					if (Name.empty())
					{
						m_UnnamedRequests.emplace(hash, UploadRequest(hash, Name, FilePath));
					}
					else
					{
						m_NamedRequests.emplace(hash, UploadRequest(hash, Name, FilePath));
					}
				}

				bool Get(UploadRequest& r)
				{

				}
				void RemoveRequest(const std::string&  hash, const std::string& Name)
				{
					if (Name.empty())
					{
						auto it = m_UnnamedRequests.find(hash);
						if (it != m_UnnamedRequests.end())
						{
							boost::filesystem::remove(it->second.m_FilePath);
							m_UnnamedRequests.erase(it);
						}
						else
						{
							BOOST_LOG_SEV(slg, critical) << "request not found:  " << hash;
						}
					}
					else
					{
						//TODO �� �����, ������ �� �������
						m_NamedRequests.erase(hash);
					}
				}

				bool IsEmpty()
				{
					return m_NamedRequests.empty() && m_UnnamedRequests.empty();
				}


				bool Save(const std::string& FileName)
				{
					bool ret = false;
					return ret;
				}

				bool Load(const std::string& FileName)
				{
					bool ret = false;
					return ret;
				}

				RequestContainer m_NamedRequests;
				RequestContainer m_UnnamedRequests;

				Channel(){}
				Channel(const Channel& obj){ operator =(obj); }
				Channel& operator = (const Channel& obj)
				{
					m_NamedRequests = obj.m_NamedRequests;
					m_UnnamedRequests = obj.m_UnnamedRequests;
					return *this;
				}


				src::severity_logger< SeverityLevel > slg;

			};


			typedef std::unordered_map <ID, Channel > ServerChannels;

			typedef std::unordered_map <std::string, Channel > ClientChannels;



			void AddRequest(const std::string&  hash, const std::string& Name, const boost::filesystem::path&  FilePath, UploadProjID Id)
			{
				if (Id.IsServerID())
				{
					auto it = m_ServerChannels.find(Id.ServerID());
					if (it == m_ServerChannels.end())
					{
						it = m_ServerChannels.insert(std::make_pair(Id.ServerID(), Channel())).first;
					}

					Channel& CurrChannel = it->second;
					CurrChannel.AddRequest(hash, Name, FilePath);
				}
				else
				{
					auto it = m_ClientChannels.find(Id.ClientID());
					if (it == m_ClientChannels.end())
					{
						it = m_ClientChannels.insert(std::make_pair(Id.ClientID(), Channel())).first;
					}

					Channel& CurrChannel = it->second;
					CurrChannel.AddRequest(hash, Name, FilePath);
				}
			}


			void RemoveRequest(ID ProjId, const std::string& fileHash, const std::string& Name)
			{
				auto it = m_ServerChannels.find(ProjId);
				if (it != m_ServerChannels.end())
				{
					Channel& CurrChannel = it->second;
					CurrChannel.RemoveRequest(fileHash, Name);
				}
			}

			void MoveTask(const LocalID& ClientId, GlobalID ServId)
			{
				BOOST_LOG_NAMED_SCOPE("SynchronizationManager: MoveTask");

				auto itc = m_ClientChannels.find(ClientId);
				auto it = m_ServerChannels.find(ServId);
				if (it == m_ServerChannels.end())
				{
					it = m_ServerChannels.insert(std::make_pair(ServId, Channel())).first;
				}
				else
				{
					BOOST_LOG_SEV(slg, critical) << "channel already exists:  " << ClientId;
				}
				Channel& clientChannel = itc->second;
				Channel& serverChannel = it->second;

				serverChannel = serverChannel;
			}

			bool TaskIsEmpty(GlobalID Id)
			{
				bool ret = true;

				auto it = m_ServerChannels.find(Id);
				if (it != m_ServerChannels.end())
				{
					Channel& CurrChannel = it->second;
					ret = CurrChannel.IsEmpty();
				}
				return ret;
			}

			private:

			ServerChannels m_ServerChannels;
			ClientChannels m_ClientChannels;
			src::severity_logger< SeverityLevel > slg;
		};

	} //!transport

};	//!survey