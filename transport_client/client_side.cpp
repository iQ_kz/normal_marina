#include "stdafx.h"


#include "client_side.h"

namespace survey
{
	namespace transport
	{

		void ClientStub::OnRead(ByteBufferPtr BuffPtr)
		{

			BOOST_LOG_NAMED_SCOPE("ClientStub: OnRead");
			try
			{
				ID RequestID = 0;
				ITransportClientRep::MethodId MID;
				Read(*BuffPtr, MID);
				switch (MID)
				{
				case survey::transport::ITransportClientRep::ePing:
					break;
				case survey::transport::ITransportClientRep::ePong:
					break;
				case survey::transport::ITransportClientRep::eOnLogin:
				{
					BOOST_LOG_SEV(slg, normal) << "Connection: " << Base::GetID()<< " -  OnLogin: ";
					Result Res;
					Read(*BuffPtr, Res);
					Read(*BuffPtr, RequestID);

					Base::m_Handler.OnLogin(Res, RequestID);
				}
				break;
				case survey::transport::ITransportClientRep::eOnCreateProject:
				{
					BOOST_LOG_SEV(slg, normal) << "Connection: " << Base::GetID() << " -  OnCreateProject: ";
					Result Res;
					ProjectId Id = 0;
					Read(*BuffPtr, Res);

					Read(*BuffPtr, Id);
					
					Read(*BuffPtr, RequestID);
					Base::m_Handler.OnCreateProject(Res, Id, RequestID);

				}
				break;
				case survey::transport::ITransportClientRep::eOnUploadFile:
				{
					BOOST_LOG_SEV(slg, normal) << "Connection: " << Base::GetID() << " -  OnUploadFile: ";
					Result Res;

					ID ProjId;
					std::string fileHash;
					std::string Name;

					
					Read(*BuffPtr, Res);

					Read(*BuffPtr, ProjId);
					Read(*BuffPtr, fileHash);
					Read(*BuffPtr, Name);

					Read(*BuffPtr, RequestID);

					Base::m_Handler.OnUploadFile(Res, ProjId, fileHash, Name, RequestID);

				}
					break;


				case survey::transport::ITransportClientRep::eOnCommit:
					break;
				case survey::transport::ITransportClientRep::eOnGetRevision:
					break;
				case survey::transport::ITransportClientRep::eOnSendFile:
					break;
				case survey::transport::ITransportClientRep::eOnGetFile:
				{
					Result Res;
					ResourceData data;
					ByteBufferPtr body = ByteBuffer::Create();
					Read(*BuffPtr, Res);

					Read(*BuffPtr, data);
					Read(*BuffPtr, *body);
//////////////////////////////////////////////////////////////////////////
					Read(*BuffPtr, RequestID);
					Base::m_Handler.OnGetFile(Res, data, body, RequestID);
				}

					break;
				case survey::transport::ITransportClientRep::eGetVersionList:
					break;
				default:
					BOOST_LOG_SEV(slg, critical) << " wrong message: " << MID;
					Base::Disconnect();
					break;
				}
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}


		void ClientTransportCore::OnConnect(ConnectionPtr SPtr)
		{
			BOOST_LOG_NAMED_SCOPE("ClientTransportCore: OnConnect");
			try
			{
				{
					Lock Locker(m_mutex);
					m_Session = SPtr;// Session::Create(IO_Handler::m_IO, SPtr, 0);
					m_pNativeStubAccess = new ClientStub;
					m_pNativeStubAccess->Handler().SetDownloadDir(m_downloadDirectory);
					m_pNativeStubAccess->Init(m_Session);
					m_StubHolder = m_Session->GetStub();
					m_Session->SetEventHandler(this);
					m_pNativeStubAccess->SetHandler(m_ExternHandler);
					



					m_Session->SetWillignessHandler(boost::bind(&ClientTransportCore::OnConnectWilligness, this, _1));
				}
				//#��� �������, ����� ��� ����������
				m_Session->ClientStart();
			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

		void ClientTransportCore::SetHandler(ITransportClientRep* Handler)
		{
			m_ExternHandler = Handler;
			if (m_pNativeStubAccess)
			{
				m_pNativeStubAccess->SetHandler(m_ExternHandler);
			}
		}

		void ClientTransportCore::OnDisconnect(ConnectionPtr SPtr)
		{
			{
				Lock Locker(m_mutex);
				if (m_Session == SPtr)
				{
					m_StubHolder.reset();
					m_Session.reset();
					m_pNativeStubAccess = nullptr;
				}
			}
			m_ExternHandler->OnConnect();
		}

		void ClientTransportCore::OnError(const std::string msg, const boost::system::error_code& Error, ID Id)
		{
			BOOST_LOG_NAMED_SCOPE("ClientTransportCore: OnConnect");
			std::stringstream ss;
			ss << msg << "; ";
			if (Error)
			{
				ss << Error.message();
				BOOST_LOG_SEV(slg, error) << " connection: " << Id << "; err: " << Error.message();
				{
					Lock Locker(m_mutex);
					if (m_Session)
					{
						if (m_Session->GetID() == Id)
						{
							m_Session->Disconnect();
						}
					}
				}
			}
			std::cout << ss.str() << std::endl;

			m_ExternHandler->OnError(ss.str());
		}

		void ClientTransportCore::OnConnectWilligness(ConnectionPtr SPtr)
		{
			m_ExternHandler->OnConnect();
		}

		survey::transport::ConnectionPtr ClientTransportCore::CreateSession()
		{
			BOOST_LOG_NAMED_SCOPE("ClientTransportCore: CreateSession");
			try
			{
				ConnectionPtr ret;
				if (m_Settings.GetConnectionType() == eNative)
				{
					ret = Connection::Create(IO_Handler::m_IO);
				}
				else
				{
					ret = Connection::CreateCrypt(IO_Handler::m_IO, m_ClientCryptContect.Context());
				}
				return ret;

			}
			catch (std::exception& e)
			{
				BOOST_LOG_SEV(slg, critical) << " - exception: " << e.what();
				throw;
			}
			catch (...)
			{
				BOOST_LOG_SEV(slg, critical) << " - unexpected exception";
				throw;
			}
		}

		void ClientTransportCore::Init(const TransportSettings& settings)
		{
			m_Settings = settings;
			if (m_Settings.GetConnectionType() == eSSL)
			{
				m_ClientCryptContect.Init();
			}
		}

	}//!namespace transport

}//!namespace survey
