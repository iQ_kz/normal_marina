#pragma once


#include <unordered_map>

#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/thread/thread.hpp>
#include <boost/timer.hpp>
#include <boost/chrono.hpp>
#include <boost/make_shared.hpp>
#include <boost/function.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "../Include/transport/transport_types.h"


namespace  survey
{
	namespace transport
	{

		class TimerManager : public  IO_Handler, public  boost::enable_shared_from_this<TimerManager>
		{
		public:
			typedef boost::function<void(ID)> HandlerType;

			typedef boost::shared_ptr <boost::asio::deadline_timer> TimerPtr;
			typedef std::unordered_map<ID, TimerPtr> TimerContainer;

			typedef boost::lock_guard<boost::mutex> Guard;


			TimerManager(IO_Svc& IO) :IO_Handler(IO){}

			void RegisterTimer(size_t ms, ID Id);

			void RunTimer(TimerPtr TimerObj, size_t ms, ID Id);

			void OnTimeout(size_t ms, ID Id);

			void SetHandler(HandlerType Handler);

			void Stop();

		private:
			HandlerType m_Handler;
			TimerContainer m_TimerContainer;
			boost::mutex m_mutex;
		};







	} //!transport

};	//!survey