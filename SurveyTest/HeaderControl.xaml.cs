﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;


namespace SurveyTest
{
	/// <summary>
	/// Interaction logic for HeaderControl.xaml
	/// </summary>
	public partial class HeaderControl : UserControl, IDisposable
	{
		public HeaderControl(Canvas canvas, Survey.HeaderWrpp hdr)
		{
			InitializeComponent();

			if (canvas == null || hdr == null)
				throw new ArgumentException("Invalid arguments");

			m_HeaderData = new HeaderData(hdr);	

			m_Canvas = canvas;
			m_Canvas.Children.Add(this);

			OnHeaderDataChanged(null, null);
			m_HeaderData.m_Hdr.DesignElement.PropertyChanged += OnHeaderDataChanged;
		}

		public void Dispose()
		{
			m_HeaderData.m_Hdr.DesignElement.PropertyChanged -= OnHeaderDataChanged;

			m_Canvas = null;
			m_HeaderData.Dispose();
			m_HeaderData = null;

			GC.SuppressFinalize(this);
		}

		~HeaderControl()
		{
			Dispose();
		}

		public void OnHeaderDataChanged(object sender, PropertyChangedEventArgs e)
		{
			// reset data context
			DataContext = m_HeaderData;
			this.RenderTransform = new RotateTransform(m_HeaderData.AngleRotation);

			// reset canvas position
			Canvas.SetLeft(this, m_HeaderData.DesignElementLeft);
			Canvas.SetTop(this, m_HeaderData.DesignElementTop);
		}

		// fields
		public HeaderData  m_HeaderData = null;
		Canvas m_Canvas = null;

		bool m_IsDragging = false;

		// start rotation
		private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
		{
			m_IsDragging = true;
		}

		// end rotation
		private void Rectangle_MouseUp(object sender, MouseButtonEventArgs e)
		{
			m_IsDragging = false;
		}

		// mouse move rotation
		private void Rectangle_MouseMove(object sender, MouseEventArgs e)
		{
			if (!m_IsDragging)
				return;
			
			Point currentPosition = e.GetPosition(this.Parent as UIElement);
			Point centrPoint = m_HeaderData.CentrPoint();

			double angle = Math.Atan2(currentPosition.X - centrPoint.X, centrPoint.Y - currentPosition.Y);
			if (double.IsNaN(angle))
				return;

			m_HeaderData.AngleRotation = Survey.Helper.Converter.RadToGradus(angle);
		}
	}

	public class HeaderData : INotifyPropertyChanged, IDisposable
	{
		public HeaderData(Survey.HeaderWrpp hdr)
		{
			if (hdr == null)
				throw new NullReferenceException("hdr");

			m_Hdr = hdr;
		}

		public void Dispose()
		{
			m_Hdr = null;
		}

		~HeaderData()
		{
			m_Hdr = null;
		}

		// Notify prop changed
		public event PropertyChangedEventHandler PropertyChanged;

		void NotifyPropertyChanged(String arg)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(arg));
		}

		// fields
		public Survey.HeaderWrpp m_Hdr = null;

		public Point CentrPoint()
		{
			Survey.CoordDatWrpp absoluteCoord = m_Hdr.DesignElement.AbsoluteCoordinate;
			return new Point(absoluteCoord.m_Left + absoluteCoord.m_Width / 2.0, absoluteCoord.m_Top + absoluteCoord.m_Height / 2.0);
		}

		public double DesignElementLeft
		{
			get { return m_Hdr.DesignElement.AbsoluteCoordinate.m_Left; }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Hdr.DesignElement.AbsoluteCoordinate;
				
				m_Hdr.DesignElement.AbsoluteCoordinate
					= new Survey.CoordDatWrpp(
						absoluteCoord.m_Top
						, value, absoluteCoord.m_Width
						, absoluteCoord.m_Height
						, absoluteCoord.m_Angle
						);
			}
		}

		public double DesignElementTop
		{
			get { return m_Hdr.DesignElement.AbsoluteCoordinate.m_Top; }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Hdr.DesignElement.AbsoluteCoordinate;

				m_Hdr.DesignElement.AbsoluteCoordinate
					= new Survey.CoordDatWrpp(
						value, absoluteCoord.m_Left
						, absoluteCoord.m_Width
						, absoluteCoord.m_Height
						, absoluteCoord.m_Angle
						);
			}

		}

		public double DesignElementWidth
		{
			get { return m_Hdr.DesignElement.AbsoluteCoordinate.m_Width; }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Hdr.DesignElement.AbsoluteCoordinate;

				m_Hdr.DesignElement.AbsoluteCoordinate 
					= new Survey.CoordDatWrpp(
						absoluteCoord.m_Top
						, absoluteCoord.m_Left
						, value, absoluteCoord.m_Height
						, absoluteCoord.m_Angle
						);
			}
		}

		public double DesignElementHeight
		{
			get { return m_Hdr.DesignElement.AbsoluteCoordinate.m_Height; }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Hdr.DesignElement.AbsoluteCoordinate;

				m_Hdr.DesignElement.AbsoluteCoordinate
					= new Survey.CoordDatWrpp(
						absoluteCoord.m_Top
						, absoluteCoord.m_Left
						, absoluteCoord.m_Width, value
						, absoluteCoord.m_Angle
						);
			}
		}

		public double AngleRotation
		{
			get { return Survey.Helper.Converter.RadToGradus(m_Hdr.DesignElement.AbsoluteCoordinate.m_Angle); }
			set
			{
				Survey.CoordDatWrpp absoluteCoord = m_Hdr.DesignElement.AbsoluteCoordinate;

				double validGradus = value;

				m_Hdr.DesignElement.AbsoluteCoordinate
					= new Survey.CoordDatWrpp(
						absoluteCoord.m_Top
						, absoluteCoord.m_Left
						, absoluteCoord.m_Width
						, absoluteCoord.m_Height
						, Survey.Helper.Converter.GradusToRad(validGradus)
						);
			}
		}

	}
}
