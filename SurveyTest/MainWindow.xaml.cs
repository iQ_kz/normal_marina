﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SurveyTest
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			Survey.FileManagerWrapper.Instance(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\work_dir", "temp_dir");
			Survey.DocumentManagerWrapper.Instance(this.Dispatcher);
		}

		Survey.DocumentWrapper m_Doc = null;
		HeaderControl m_HeaderControl = null;
		QuestionControl m_QuestionControl = null;

		private void Window_Closed(object sender, EventArgs e)
		{
			m_HeaderControl.Dispose();
			m_QuestionControl.Dispose();

			Survey.DocumentManagerWrapper.Clear();
			Survey.FileManagerWrapper.Clear();	
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			m_Doc = Survey.DocumentManagerWrapper.GetInstance().CreateDocument("test canvas doc");

			Survey.HeaderWrpp hdr = new Survey.HeaderWrpp(
				m_Doc,
				new Survey.ColorDatWrpp(100, 100, 100, 100),
				new Survey.CoordDatWrpp(20, 20, 100, 100, 0),
				new Survey.ColorDatWrpp(100, 100, 100, 100),
				new Survey.CoordDatWrpp(148, 248, 100, 50, 0),
				"abra cadabra",
				new Survey.ImageProperties(),
				new Survey.BorderDat(),
				"name");

			Survey.QuestionWrpp quest = new Survey.QuestionWrpp(
				m_Doc,
				new Survey.ColorDatWrpp(100, 100, 100, 100),
				new Survey.CoordDatWrpp(20, 20, 100, 100, 0),
				new Survey.ColorDatWrpp(100, 100, 100, 100),
				new Survey.CoordDatWrpp(20, 20, 250, 150, 0),
				"name"
				);

			quest.AddChild(hdr);

			m_HeaderControl = new HeaderControl(m_Test_canvas, hdr);
			m_QuestionControl = new QuestionControl(m_Test_canvas, quest, m_HeaderControl);


			Survey.ImageWrpp addImg = m_Doc.Images.AddImage("gif-01.gif");
			System.Drawing.Image bmpImg = addImg.ImageData;
			BitmapImage bmpSmallImg = addImg.PreviewImageData;

			m_PictureBox.Image = bmpImg;

		}

		private void Window_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Up)
				m_HeaderControl.m_HeaderData.DesignElementTop -= 1;
			else if (e.Key == Key.Down)
				m_HeaderControl.m_HeaderData.DesignElementTop += 1;
			else if (e.Key == Key.Right)
				m_HeaderControl.m_HeaderData.DesignElementLeft += 1;
			else if (e.Key == Key.Left)
				m_HeaderControl.m_HeaderData.DesignElementLeft -= 1;
			else if (e.Key == Key.W)
				m_QuestionControl.m_QuestionData.DesignElementTop -= 1;
			else if (e.Key == Key.S)
				m_QuestionControl.m_QuestionData.DesignElementTop += 1;
			else if (e.Key == Key.D)
				m_QuestionControl.m_QuestionData.DesignElementLeft += 1; 
			else if(e.Key == Key.A)
				m_QuestionControl.m_QuestionData.DesignElementLeft -= 1; 

		}


	}
}
