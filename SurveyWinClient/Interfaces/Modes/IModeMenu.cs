﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Base.Interfaces
{
    public interface IModeMenu
    {
        string Title { get; set; }
    }
}
