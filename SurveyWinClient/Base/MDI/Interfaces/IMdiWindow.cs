﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace MDI
{
    public interface IMdiWindow
    {
        string Title { get; set; }

        double Top { get; set; }
        double Left { get; set; }
        double Height { get; set; }
        double Width { get; set; }

        double PreviousTop { get; set; }
        double PreviousLeft { get; set; }
        double PreviousHeight { get; set; }
        double PreviousWidth { get; set; }

        int ZIndex { get; set; }

        WindowState WindowState { get; set; }

        bool IsSelected { get; set; }

        bool IsDisplayed { get; set; }

        bool IsClosable { get; set; }
    }
}
