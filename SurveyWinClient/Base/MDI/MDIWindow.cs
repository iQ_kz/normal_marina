﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

using MDI.Extensions;
using MDI.WindowControls;
using MDI.Events;

namespace MDI
{
   using System.ComponentModel;
   using System.Diagnostics;  

   [TemplatePart(Name = "PART_Border", Type = typeof(Border))]
   [TemplatePart(Name = "PART_BorderGrid", Type = typeof(Grid))]
   [TemplatePart(Name = "PART_Header", Type = typeof(Border))]
   [TemplatePart(Name = "PART_ButtonBar", Type = typeof(StackPanel))]
   [TemplatePart(Name = "PART_ButtonBar_CloseButton", Type = typeof(WindowButton))]
   [TemplatePart(Name = "PART_ButtonBar_MaximizeButton", Type = typeof(WindowButton))]
   [TemplatePart(Name = "PART_ButtonBar_MinimizeButton", Type = typeof(WindowButton))]
   [TemplatePart(Name = "PART_BorderContent", Type = typeof(Border))]
   [TemplatePart(Name = "PART_Content", Type = typeof(ContentPresenter))]
   [TemplatePart(Name = "PART_MoverThumb", Type = typeof(MoveThumb))]   
   [TemplatePart(Name = "PART_ResizerThumb", Type = typeof(ResizeThumb))]
   [TemplatePart(Name = "PART_Thumblr", Type = typeof(Image))]
   [DebuggerDisplay("{Title}")]
   public sealed class MDIWindow : ContentControl
   {
      public MDIContainer Container { get; private set; }

      public bool InDragging = false;
      Point InitDragMousePoint;

      const double restrictBounds = 50.0;
      
      public Image Tumblr { get; private set; }

      private WindowButton closeButton;
      private WindowButton maximizeButton;
      private WindowButton minimizeButton;

      static MDIWindow()
      {          
         DefaultStyleKeyProperty.OverrideMetadata(typeof(MDIWindow), new FrameworkPropertyMetadata(typeof(MDIWindow)));
      }

      public void Initialize(MDIContainer container)
      {
         this.Container = container;
         this.Container.SizeChanged += OnContainerSizeChanged;
      }

      public override void OnApplyTemplate()
      {
          this.closeButton = this.GetTemplateChild("PART_ButtonBar_CloseButton") as WindowButton;
          if (this.closeButton != null)
          {
              this.closeButton.Click += CloseWindowButtonClick;
          }

          this.maximizeButton = this.GetTemplateChild("PART_ButtonBar_MaximizeButton") as WindowButton;
          if (this.maximizeButton != null)
          {
              this.maximizeButton.Click += MaximizeWindowButtonClick;
          }

          this.minimizeButton = this.GetTemplateChild("PART_ButtonBar_MinimizeButton") as WindowButton;
          if (this.minimizeButton != null)
          {
              this.minimizeButton.Click += MinimizeWindowButtonClick;
          }

          this.Tumblr = this.GetTemplateChild("PART_Tumblr") as Image;
      } 

      private void OnContainerSizeChanged(object sender, SizeChangedEventArgs e)
      {
         if (this.WindowState == WindowState.Maximized)
         {
            this.Width += e.NewSize.Width - e.PreviousSize.Width;
            this.Height += e.NewSize.Height - e.PreviousSize.Height;

             //todo previous sizes!!!
             //тоже самое что и с нормалайзом, только касаемо предыдущих размеров
             //гипотетические размеры
            this.RemoveWindowLock();
         }

         if (this.WindowState == WindowState.Normal)
         {

             var percentDeltaWidth = e.NewSize.Width / e.PreviousSize.Width;
             var percentDeltaHeight = e.NewSize.Height / e.PreviousSize.Height;

             if (double.IsInfinity(percentDeltaWidth) || double.IsInfinity(percentDeltaHeight)) return;
             //if (double.IsInfinity(percentDeltaWidth) || double.IsInfinity(percentDeltaHeight)) return;


             //this.Width *= percentDeltaWidth;
             //this.Height *= percentDeltaHeight;

             //todo - resizing

             var FrameSizeWidth = e.NewSize.Width;
             var FrameSizeHeight = e.NewSize.Height;

             var FrameSizeWidthPrev = e.PreviousSize.Width;
             var FrameSizeHeightPrev = e.PreviousSize.Height;

             double ZapasLeft = 20.0, ZapasRight = 20.0, ZapasTop = 20.0, ZapasBottom = 20.0;

             var Left = this.Left;
             var Top = this.Top;

             var Width = this.Width;
             var Height = this.Height;

             double MinWidth = 200, MinHeight = 200, MaxWidth = 1000, MaxHeight = 700;

             var DeltaFrameSizeWidth = FrameSizeWidth / FrameSizeWidthPrev;
             var DeltaFrameSizeHeight = FrameSizeHeight / FrameSizeHeightPrev;

             var _zapasLeft = ZapasLeft; // const
             var _zapasTop = ZapasTop; // const
             var _zapasRight = FrameSizeWidth - ZapasRight; // после изменения надо посчитать
             var _zapasBottom = FrameSizeHeight - ZapasBottom; // после изменения надо посчитать

             var Width1 = Width * DeltaFrameSizeWidth;
             var Height1 = Height * DeltaFrameSizeHeight;

             Width1 = (Width1 < MinWidth) ? MinWidth : ((Width1 > MaxWidth) ? MaxWidth : Width1);
             Height1 = (Height1 < MinHeight) ? MinHeight : ((Height1 > MaxHeight) ? MaxHeight : Height1);

             var Left1 = Left * DeltaFrameSizeWidth;
             var Top1 = Top * DeltaFrameSizeHeight;

             var _right = Left1 + Width1;
             var _bottom = Top1 + Height1;

             Left1 = (_right < _zapasLeft) ? (_zapasLeft - Width1) : ((Left1 > _zapasRight) ? _zapasRight : Left1);
             Top1 = (_bottom < _zapasTop) ? (_zapasTop - Height1) : ((Top1 > _zapasBottom) ? _zapasBottom : Top1);

             this.Top = Top1;
             Canvas.SetTop(this, Top1);
             this.Left = Left1;
             Canvas.SetLeft(this, Left);

             this.Width = Width1;
             this.Height = Height1;

             string title = string.Format("Cont[{0}:{1}] Wnd[{2}:{3}] PrvContH[{4}]", (int)this.Container.ActualWidth, (int)this.Container.ActualHeight,
                                                                                (int)(this.Left + this.Width), (int)(this.Top + this.Height),
                                                                                (int)e.PreviousSize.Height);

             //this.Title = title;


             //             /*
              
            //              /*
            //то, что нам надо иметь для рассчётов
            //FrameSizeWidth,FrameSizeHeight - размеры фрейма
            //FrameSizeWidthPrev,FrameSizeHeightPrev - предыдущие размеры фрейма
            //ZapasLeft,ZapasTop,ZapasRight,ZapasBottom - отступы, чтобы хвататься мышкой (left, top, right, bottom)
            //Left,Top,Width,Height - left, top, width, height конкретного окна
            //MinWidth,MinHeight,MaxWidth,MaxHeight - минимальная, максимальная длина/ширина конкретного окна
            //*/

            //// дальше считаем. 
            //// 1. дельты. может быть 0..1 (уменьшение) 1+ (увеличение)
            //DeltaFrameSizeWidth = FrameSizeWidth/FrameSizeWidthPrev;
            //DeltaFrameSizeHeight = FrameSizeHeight/FrameSizeHeightPrev;

            //// 2. запас, который нужен для left, top, right, bottom - чтобы они не ушли за экран
            //_zapasLeft = ZapasLeft; // const
            //_zapasTop = ZapasTop; // const
            //_zapasRight = FrameSizeWidth-ZapasRight; // после изменения надо посчитать
            //_zapasBottom = FrameSizeHeight-ZapasBottom; // после изменения надо посчитать

            //// 3. ресайзим окно.
            //Width1 = Width*DeltaFrameSizeWidth ;
            //Height1 = Height*DeltaFrameSizeHeight ;

            //// 4. проверяем на минимальную/максимальную высоту/ширину
            //Width1 = (Width1<MinWidth)? MinWidth : ( (Width1>MaxWidth)? MaxWidth : Width1 );
            //Height1 = (Height1<MinHeight)? MinHeight : ( (Height1>MaxHeight)? MaxHeight : Height1 );

            //// 5. делаем гипотетический новый верхний левый угол окна
            //Left1 = Left*DeltaFrameSizeWidth; 
            //Top1 = Top*DeltaFrameSizeHeight;

            //// 6. считаем нижнюю правую координату
            //_right = Left1+Width1;
            //_bottom = Top1+Height1;

            //// 7. смотрим, не ушло ли окно слишком далеко
            //Left1 = (_right<_zapasLeft) ? _zapasLeft : ( (Left1>_zapasRight)? _zapasRight : Left1 );
            //Top1 = (_bottom<_zapasTop) ? _zapasTop : ( (Top1>_zapasBottom)? _zapasBottom : Top1 );

            ///*
            //на выходе имеем Left1,Top1 - новые left, top окна; Width1,Height1 - новые height, width окна
            //*/
            //7 ошибся.
            //// 7. смотрим, не ушло ли окно слишком далеко
            //Left1 = (_right<_zapasLeft) ? (_zapasLeft-Width1) : ( (Left1>_zapasRight)? _zapasRight : Left1 );
            //Top1 = (_bottom<_zapasTop) ? (_zapasTop-Height1) : ( (Top1>_zapasBottom)? _zapasBottom : Top1 );
              
            //              */
             
            
         }

         if (this.WindowState == WindowState.Minimized)
         {
             //todo previous sizes!!!
            this.Top = this.Container.ActualHeight - 32;
            Canvas.SetTop(this, this.Top);            
         }
      }     

      protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
      {
          if (this.IsSelected == false)
              this.IsSelected = true;

          bool allowDrag = (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt)) && this.WindowState == WindowState.Normal;

          if (allowDrag)
          {
              InDragging = true;

              InitDragMousePoint = e.GetPosition(this);

              this.CaptureMouse();

              e.Handled = true;
          }
          else base.OnPreviewMouseDown(e);    
              
     }

      protected override void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs e)
      {
          if (InDragging)
          {
              InDragging = false;

              this.ReleaseMouseCapture();
          }
          else base.OnPreviewMouseLeftButtonUp(e);
      }

      protected override void OnPreviewMouseMove(MouseEventArgs e)
      {
          if (InDragging)
          {
              Point currDragMousePoint = e.GetPosition(this);

              double deltaX = currDragMousePoint.X - InitDragMousePoint.X;
              double deltaY = currDragMousePoint.Y - InitDragMousePoint.Y;                          

              this.Top += deltaY;
              this.Left += deltaX;
                  
              Canvas.SetTop(this, this.Top);
              Canvas.SetLeft(this, this.Left);
             
              e.Handled = true;
          }
          else base.OnMouseMove(e);
      }                

      #region Focus
      protected override void OnLostFocus(RoutedEventArgs e)
      {
          base.OnLostFocus(e);
          RaiseEvent(new RoutedEventArgs(FocusChangedEvent, this.DataContext));          
      }
      
      protected override void OnGotFocus(RoutedEventArgs e)
      {
          base.OnGotFocus(e);
          RaiseEvent(new RoutedEventArgs(FocusChangedEvent, this.DataContext));          
      }

      public static readonly RoutedEvent FocusChangedEvent = EventManager.RegisterRoutedEvent(
        "FocusChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MDIWindow));

      public event RoutedEventHandler FocusChanged
      {
          add { AddHandler(FocusChangedEvent, value); }
          remove { RemoveHandler(FocusChangedEvent, value); }
      }
      #endregion

      #region WindowButtons
      private void NormalizeWindowButtonClick(object sender, RoutedEventArgs e)
      {
          this.WindowState = WindowState.Normal;
      }

      private void MaximizeWindowButtonClick(object sender, RoutedEventArgs e)
      {          
          this.WindowState = WindowState.Maximized;
      }

      private void MinimizeWindowButtonClick(object sender, RoutedEventArgs e)
      {          
          this.WindowState = WindowState.Minimized;
      }
      
      private void CloseWindowButtonClick(object sender, RoutedEventArgs e)      
      {
          this.IsDisplayed = false;
          //RaiseEvent(new RoutedEventArgs(ClosingEvent));               
      }
      #endregion

      #region Properties
      public string Title
      {
          get { return (string)GetValue(TitleProperty); }
          set { SetValue(TitleProperty, value); }
      }

      public static readonly DependencyProperty TitleProperty =
          DependencyProperty.Register("Title", typeof(string), typeof(MDIWindow), new PropertyMetadata(string.Empty));

      public bool HasDropShadow
      {
          get { return (bool)GetValue(HasDropShadowProperty); }
          set { SetValue(HasDropShadowProperty, value); }
      }

      public static readonly DependencyProperty HasDropShadowProperty =
          DependencyProperty.Register("HasDropShadow", typeof(bool), typeof(MDIWindow), new UIPropertyMetadata(true));

      #region PositionSize
      public double Top
      {
          get { return (double)GetValue(TopProperty); }
          set { SetValue(TopProperty, value);  }
      }

      public static readonly DependencyProperty TopProperty =
          DependencyProperty.Register("Top", typeof(double), typeof(MDIWindow), new FrameworkPropertyMetadata(0.0));

      public double Left
      {          
          get { return (double)GetValue(LeftProperty); }
          set { SetValue(LeftProperty, value); }
      }

      public static readonly DependencyProperty LeftProperty =
          DependencyProperty.Register("Left", typeof(double), typeof(MDIWindow), new FrameworkPropertyMetadata(0.0));
       
      public double PreviousTop
      {
          get { return (double)GetValue(PreviousTopProperty); }
          set { SetValue(PreviousTopProperty, value); }
      }

      public static readonly DependencyProperty PreviousTopProperty =
          DependencyProperty.Register("PreviousTop", typeof(double), typeof(MDIWindow), new FrameworkPropertyMetadata(0.0));

      public double PreviousLeft
      {
          get { return (double)GetValue(PreviousLeftProperty); }
          set { SetValue(PreviousLeftProperty, value); }
      }

      public static readonly DependencyProperty PreviousLeftProperty =
          DependencyProperty.Register("PreviousLeft", typeof(double), typeof(MDIWindow), new FrameworkPropertyMetadata(0.0));

      public double PreviousWidth
      {
          get { return (double)GetValue(PreviousWidthProperty); }
          set { SetValue(PreviousWidthProperty, value); }
      }

      public static readonly DependencyProperty PreviousWidthProperty =
          DependencyProperty.Register("PreviousWidth", typeof(double), typeof(MDIWindow), new FrameworkPropertyMetadata(200.0));

      public double PreviousHeight
      {
          get { return (double)GetValue(PreviousHeightProperty); }
          set { SetValue(PreviousHeightProperty, value); }
      }

      public static readonly DependencyProperty PreviousHeightProperty =
          DependencyProperty.Register("PreviousHeight", typeof(double), typeof(MDIWindow), new FrameworkPropertyMetadata(200.0));
      #endregion

      #region ZIndex
      public int ZIndex
      {
          get { return (int)GetValue(ZIndexProperty); }
          set { SetValue(ZIndexProperty, value); }
      }

      public static readonly DependencyProperty ZIndexProperty =
          DependencyProperty.Register("ZIndex", typeof(int), typeof(MDIWindow), new PropertyMetadata(0, OnZIndexChanged));

      public static readonly RoutedEvent ZIndexChangedEvent = EventManager.RegisterRoutedEvent(
      "ZIndexChanged", RoutingStrategy.Bubble, typeof(ZIndexChangedRoutedEventHandler), typeof(MDIWindow));

      public event ZIndexChangedRoutedEventHandler ZIndexChanged
      {
          add { AddHandler(ZIndexChangedEvent, value); }
          remove { RemoveHandler(ZIndexChangedEvent, value); }
      }

      private static void OnZIndexChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
      {
          var window = obj as MDIWindow;
          var args = new ZIndexChangedEventArgs(ZIndexChangedEvent, (int)e.OldValue, (int)e.NewValue);
          window.RaiseEvent(args);
      }
      #endregion

      #region WindowState
      public WindowState WindowState
      {
          get { return (WindowState)GetValue(WindowStateProperty); }
          set { SetValue(WindowStateProperty, value); }
      }

      public static readonly DependencyProperty WindowStateProperty =
          DependencyProperty.Register("WindowState", typeof(WindowState), typeof(MDIWindow), new PropertyMetadata(WindowState.Normal, OnWindowStateChanged));

      public static readonly RoutedEvent WindowStateChangedEvent = EventManager.RegisterRoutedEvent(
        "WindowStateChanged", RoutingStrategy.Bubble, typeof(WindowStateChangedRoutedEventHandler), typeof(MDIWindow));

      public event WindowStateChangedRoutedEventHandler WindowStateChanged
      {
          add { AddHandler(WindowStateChangedEvent, value); }
          remove { RemoveHandler(WindowStateChangedEvent, value); }
      }

      private static void OnWindowStateChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
      {
          var window = obj as MDIWindow;
          var args = new WindowStateChangedEventArgs(WindowStateChangedEvent, (WindowState)e.OldValue, (WindowState)e.NewValue);
          window.RaiseEvent(args);
      }
      #endregion

      #region IsSelected
      public bool IsSelected
      {
          get { return (bool)GetValue(IsSelectedProperty); }
          set { SetValue(IsSelectedProperty, value); }
      }

      public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected", typeof(bool), typeof(MDIWindow), new UIPropertyMetadata(false, OnIsSelectedChanged));

      public static readonly RoutedEvent IsSelectedChangedEvent = EventManager.RegisterRoutedEvent(
       "IsSelectedChanged", RoutingStrategy.Bubble, typeof(IsSelectedChangedRoutedEventHandler), typeof(MDIWindow));

      public event IsSelectedChangedRoutedEventHandler IsSelectedChanged
      {
          add { AddHandler(IsSelectedChangedEvent, value); }
          remove { RemoveHandler(IsSelectedChangedEvent, value); }
      }

      private static void OnIsSelectedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
      {
          var window = obj as MDIWindow;
          var args = new IsSelectedChangedEventArgs(IsSelectedChangedEvent, (bool)e.OldValue, (bool)e.NewValue);
          window.RaiseEvent(args);
      }

      #endregion      

      #region IsDisplayed
      public bool IsDisplayed
      {
          get { return (bool)GetValue(IsDisplayedProperty); }
          set { SetValue(IsDisplayedProperty, value); }
      }

      public static readonly DependencyProperty IsDisplayedProperty =
          DependencyProperty.Register("IsDisplayed", typeof(bool), typeof(MDIWindow), new UIPropertyMetadata(false, OnIsDisplayedChanged));

      public static readonly RoutedEvent IsDisplayedChangedEvent = EventManager.RegisterRoutedEvent(
       "IsDisplayedChanged", RoutingStrategy.Bubble, typeof(IsDisplayedChangedRoutedEventHandler), typeof(MDIWindow));

      public event IsDisplayedChangedRoutedEventHandler IsDisplayedChanged
      {
          add { AddHandler(IsDisplayedChangedEvent, value); }
          remove { RemoveHandler(IsDisplayedChangedEvent, value); }
      }

      private static void OnIsDisplayedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
      {
          var window = obj as MDIWindow;
          var args = new IsDisplayedChangedEventArgs(IsDisplayedChangedEvent, (bool)e.OldValue, (bool)e.NewValue);
          window.RaiseEvent(args);
      }

      #endregion   

      #region IsClosable
      public bool IsClosable
      {
          get { return (bool)GetValue(IsClosableProperty); }
          set { SetValue(IsClosableProperty, value); }
      }

      public static readonly DependencyProperty IsClosableProperty =
                             DependencyProperty.Register("IsClosable", typeof(bool), typeof(MDIWindow), new UIPropertyMetadata(true));

       #endregion

       #endregion

   }
}
