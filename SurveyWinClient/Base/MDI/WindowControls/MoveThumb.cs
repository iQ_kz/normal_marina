﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

using MDI.Extensions;

namespace MDI.WindowControls
{
   public sealed class MoveThumb : Thumb
   {
      static MoveThumb()
      {
         DefaultStyleKeyProperty.OverrideMetadata(typeof(MoveThumb), new FrameworkPropertyMetadata(typeof(MoveThumb)));
      }

      public MoveThumb()
      {         
         this.DragDelta += this.OnMoveThumbDragDelta;
      }      

      protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
      {
         var window = VisualTreeExtension.FindMDIWindow(this);         

         if (window != null && window.Container != null)
         {
            switch (window.WindowState)
            {
               case WindowState.Maximized:
                  window.WindowState = WindowState.Normal;
                  break;
               case WindowState.Normal:
                  window.WindowState = WindowState.Maximized;
                  break;
               case WindowState.Minimized:
                  window.WindowState = WindowState.Normal;
                  break;
               default:
                  throw new InvalidOperationException("Unsupported WindowsState mode");
            }
         }

         e.Handled = true;
      }

      private void OnMoveThumbDragDelta(object sender, DragDeltaEventArgs e)
      {
         var window = VisualTreeExtension.FindMDIWindow(this);
          
         if (window != null)                  
         {
            if (window.WindowState == WindowState.Normal)
            {
                window.Top += e.VerticalChange;
                window.Left += +e.HorizontalChange;

                Canvas.SetLeft(window, window.Left);
                Canvas.SetTop(window, window.Top);                
            }
         }
      }
   }
}
