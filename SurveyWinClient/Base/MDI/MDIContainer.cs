﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

using MDI.Events;
using MDI.Extensions;

namespace MDI
{   
   public sealed class MDIContainer : System.Windows.Controls.Primitives.Selector
   {
      private IList ItemSourceReference { get; set; }

      public int MinimizedWindowsCount { get; set; }

      static MDIContainer()
      {
         DefaultStyleKeyProperty.OverrideMetadata(typeof(MDIContainer), new FrameworkPropertyMetadata(typeof(MDIContainer)));
      }

      protected override DependencyObject GetContainerForItemOverride()
      {
         return new MDIWindow();
      }

      private int MaxZIndex
      {
          get
          {             
              int mzi = -1;
              
              foreach (IMdiWindow wnd in ItemSourceReference)
                  if (wnd.ZIndex > mzi && wnd.ZIndex != 100)
                      mzi = wnd.ZIndex;

              return mzi;              
          }
      }

      public override void EndInit()
      {
          base.EndInit();
          this.SelectedItem = null;
      }

      protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
      {
         var window = element as MDIWindow;
         
         if (window != null)
         {
             if (window.IsSelected)
             {
                 window.ZIndex = 100;
                 this.SelectedItem = window.DataContext;
             }            

             if (window.WindowState == WindowState.Maximized)
             {
                 window.Width = this.ActualWidth;
                 window.Height = this.ActualHeight;                 
             }             

             Canvas.SetTop(window, window.Top);
             Canvas.SetLeft(window, window.Left);

             Canvas.SetZIndex(window, window.ZIndex);

             window.FocusChanged += OnWindowFocusChanged;
             window.IsSelectedChanged += OnIsSelectedChanged;
             window.IsDisplayedChanged += OnIsDisplayedChanged;
             window.ZIndexChanged += OnZIndexChanged;
             window.WindowStateChanged += OnWindowStateChanged;
             
             window.Initialize(this);

         }

         base.PrepareContainerForItemOverride(element, item);
      }           

      protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
      {
          base.OnItemsSourceChanged(oldValue, newValue);

          if (newValue != null && newValue is IList)
          {
              this.ItemSourceReference = newValue as IList;
          }          
      }

      #region WindowEvents

      private void OnZIndexChanged(object sender, ZIndexChangedEventArgs e)
      {
          var window = sender as MDIWindow;

          if (window != null)
          {
              if (e.NewValue == 100)
              {
                  //shifting indexes                  
                  for (int i = 0; i < ItemSourceReference.Count; i++)
                  {
                      var currWnd = ItemSourceReference[i] as IMdiWindow;
                      if (currWnd.ZIndex > e.OldValue && currWnd.ZIndex != 100)
                          currWnd.ZIndex--;
                  }
                  
              }
              
              Canvas.SetZIndex(window, e.NewValue);
              
          }
      }     
           
      private void OnIsSelectedChanged(object sender, IsSelectedChangedEventArgs e)
      {
          var window = sender as MDIWindow;
          
          if (window != null)
          {
              if (e.NewValue == true) //selectedWnd
              {
                  window.ZIndex = 100;

                  if (this.SelectedItem != null)
                     (this.SelectedItem as IMdiWindow).IsSelected = false;

                  this.SelectedItem = window.DataContext;                           
              }

              if (e.NewValue == false) //prevSelectedWnd
              {                                   
                  window.ZIndex = MaxZIndex + 1;
              }
          }

          //window.Title = string.Join(" ", ItemSourceReference.Cast<IMdiWindow>().Select(x => x.ZIndex));

      }

      private void OnIsDisplayedChanged(object sender, IsDisplayedChangedEventArgs e)
      {
          var window = sender as MDIWindow;
          
          if (ItemSourceReference.Count == 1 && e.NewValue == false) { window.IsDisplayed = true; return; }

          if (window != null && e.NewValue == false)
          {
              window.IsSelected = false;

              window.FocusChanged -= OnWindowFocusChanged;
              window.WindowStateChanged -= OnWindowStateChanged;
              window.IsSelectedChanged -= OnIsSelectedChanged;
              window.IsDisplayedChanged -= OnIsDisplayedChanged;
              window.ZIndexChanged -= OnZIndexChanged;              

              if (this.ItemSourceReference != null)
                  this.ItemSourceReference.Remove(window.DataContext);
              
              window.DataContext = null;

              if (ItemSourceReference.Count == 1)
              {
                  var toWnd = ItemSourceReference[0] as IMdiWindow;
                  toWnd.IsClosable = false;
                  toWnd.IsSelected = true;
                  return;
              }
              else
              {
                  var toWnd = ItemSourceReference.Cast<IMdiWindow>().FirstOrDefault(x => x.ZIndex == MaxZIndex);
                  toWnd.IsSelected = true;
              }
              
          }

      }
     
      private void OnWindowFocusChanged(object sender, RoutedEventArgs e)
      {
          var window = sender as MDIWindow;
          
          if (window != null)
          {
              if (window.IsFocused)
              {
                 
              }        
              else
              {
                  
              }
          }          
      }

      private void OnWindowStateChanged(object sender, WindowStateChangedEventArgs e)
      {
          var window = sender as MDIWindow;

          if (window != null)
          {
              if (e.OldValue == WindowState.Minimized)
                  this.MinimizedWindowsCount--;

              switch ((WindowState)e.NewValue)
              {
                  case WindowState.Maximized: {
                      window.Maximize();                        
                      break; 
                  }
                  case WindowState.Minimized: {
                      window.Minimize(); 
                      this.MinimizedWindowsCount++;                       
                      break; 
                  }
                  case WindowState.Normal: {
                      window.Normalize();                       
                      break; 
                  }
                  default: { throw new ArgumentException("Undef win state value!"); }
              }
          }

      }               

      #endregion
   }
}
