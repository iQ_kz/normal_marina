﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class RulesViewModel : ViewModelBase, IMode
    {
        public RulesViewModel(bool _enabled, RulesMenuViewModel _menu, RulesWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        RulesMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (RulesMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        RulesWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (RulesWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
