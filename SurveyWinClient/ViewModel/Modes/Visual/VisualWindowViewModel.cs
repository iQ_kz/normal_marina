﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using SurveyWinClient.View;

using MVVM;

using Base.Interfaces;
using System.Collections.ObjectModel;

namespace SurveyWinClient.ViewModel
{
    class VisualWindowViewModel : ViewModelBase, IModeWindow
    {
        string title;
        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }

        WindowState windowState;
        public WindowState WindowState
        {
            get { return windowState; }
            set { windowState = value; OnPropertyChanged("WindowState"); }
        }

        bool isDisplayed;
        public bool IsDisplayed
        {
            get { return isDisplayed; }
            set { isDisplayed = value; OnPropertyChanged("IsDisplayed"); }
        }

        bool isClosable;
        public bool IsClosable
        {
            get { return isClosable; }
            set { isClosable = value; OnPropertyChanged("IsClosable"); }
        }

        bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set { isSelected = value; OnPropertyChanged("IsSelected"); }
        }

        public RelayCommand NormalizeWindow
        {
            get;
            private set;
        }
        
        void normalizeWindow(object _param)
        {
            WindowState = WindowState.Normal;            
        }

        public RelayCommand CloseWindow
        {
            get;
            private set;
        }

        void closeWindow(object _param)
        {            
            this.IsDisplayed = false;
        }



        public VisualWindowViewModel()
        {
            IsInvalid = false;
            Zoom = 100;            
            fontSize = 14;
            IsButtonsShowing = true;
            pagesPanelVisibility = Visibility.Visible;
            picturesPanelVisibility = Visibility.Visible;
            Title = "Visual mode";
            IsBordersShowing = true;
            IsMovingOperation = false;
            IsScalingOperation = false;
            //ListViewColletion = new ObservableCollection<VEPage>();
            NormalizeWindow = new RelayCommand(normalizeWindow, p => WindowState == WindowState.Maximized);
            CloseWindow = new RelayCommand(closeWindow, p => IsDisplayed && IsClosable);
            ZoomPlus = new RelayCommand(zoomPlus, p => true);
            ZoomMinus = new RelayCommand(zoomMinus, p => true);
            UnZoom = new RelayCommand(unZoom, p => true);
            //AddBlock = new RelayCommand(addBlock, p => true);
            SetMovingOperation = new RelayCommand(setMovingOperation, p => true);
            SetScalingOperation = new RelayCommand(setScalingOperation, p => true);
            SetFillColorTransparent = new RelayCommand(setFillColorTransparent, p => true);
            SetBorderColorBlack = new RelayCommand(setBorderColorBlack, p => true);
            //AddPage = new RelayCommand(addPage, p => true);
            TogglePagesPanelVisibility = new RelayCommand(togglePagesPanelVisibility, p => true);

            TogglePicturesPanelVisibility = new RelayCommand(togglePicturesPanelVisibility, p => true);
        }

       /* VEPage selectedPage;
        public VEPage SelectedPage
        {
            get { return selectedPage; }
            set { selectedPage = value; OnPropertyChanged("SelectedPage"); }
        }*/

        int selectedPage;
        public int SelectedPage
        {
            get { return selectedPage; }
            set { selectedPage = value; OnPropertyChanged("SelectedPage"); }
        }

        int zIndex;
        public int ZIndex
        {
            get { return zIndex; }
            set { zIndex = value; OnPropertyChanged("ZIndex"); }
        }
        bool isInvalid;
        public bool IsInvalid
        {
            get { return isInvalid; }
            set { isInvalid = value; OnPropertyChanged("IsInvalid"); }
        }

        #region MDIPositionSize
        double top;
        public double Top
        {
            get { return top; }
            set { top = value; OnPropertyChanged("Top"); }
        }

        double left;
        public double Left
        {
            get { return left; }
            set { left = value; OnPropertyChanged("Left"); }
        }

        double width;
        public double Width
        {
            get { return width; }
            set { width = value; OnPropertyChanged("Width"); }
        }

        double height;
        public double Height
        {
            get { return height; }
            set { height = value; OnPropertyChanged("Height"); }
        }

        double previousTop;
        public double PreviousTop
        {
            get { return previousTop; }
            set { previousTop = value; OnPropertyChanged("PreviousTop"); }
        }

        double previousLeft;
        public double PreviousLeft
        {
            get { return previousLeft; }
            set { previousLeft = value; OnPropertyChanged("PreviousLeft"); }
        }

        double previousWidth;
        public double PreviousWidth
        {
            get { return previousWidth; }
            set { previousWidth = value; OnPropertyChanged("PreviousWidth"); }
        }

        double previousHeight;
        public double PreviousHeight
        {
            get { return previousHeight; }
            set { previousHeight = value; OnPropertyChanged("PreviousHeight"); }
        }

        #endregion

        #region VisualEditor
       

        public RelayCommand AddBlock
        {
            get;
            private set;
        }

        /*void addBlock(object _param)
        {
            VisualEditorViewSingleton.VE.AddBlock();
        }*/

        double fontSize;
        public double FontSize
        {
            get { return fontSize; }
            set { fontSize = value; OnPropertyChanged("FontSize"); }
        }
        bool fontItalic;
        public bool FontItalic
        {
            get { return fontItalic; }
            set { fontItalic = value; OnPropertyChanged("FontItalic"); }
        }
        bool fontBold;
        public bool FontBold
        {
            get { return fontBold; }
            set { fontBold = value; OnPropertyChanged("FontBold"); }
        }
        bool fontUnderline;
        public bool FontUnderline
        {
            get { return fontUnderline; }
            set { fontUnderline = value;
            if (fontUnderline) FontStrikethrough = false; 
                OnPropertyChanged("FontUnderline"); }
        }
        bool fontStrikethrough;
        public bool FontStrikethrough
        {
            get { return fontStrikethrough; }
            set { fontStrikethrough = value;
            if (fontStrikethrough) FontUnderline = false; 
                OnPropertyChanged("FontStrikethrough");}
        }
        bool fontSubscript;
        public bool FontSubscript
        {
            get { return fontSubscript; }
            set { fontSubscript = value;
            if (fontSubscript) FontSuperscript = false; 
                OnPropertyChanged("FontSubscript"); }
        }

        bool fontSuperscript;
        public bool FontSuperscript
        {
            get { return fontSuperscript; }
            set
            {
                fontSuperscript = value;
                if (fontSuperscript) FontSubscript = false;
                OnPropertyChanged("FontSuperscript");
            }
        }

        Color selectedBorderColor;
        public Color SelectedBorderColor
        {
            get { return selectedBorderColor; }
            set
            {
                selectedBorderColor = value; OnPropertyChanged("SelectedBorderColor");
            }
        }

        Color selectedFillColor;
        public Color SelectedFillColor
        {
            get { return selectedFillColor; }
            set {
                selectedFillColor = value; OnPropertyChanged("SelectedFillColor"); }
        }

        Color selectedTextColor;
        public Color SelectedTextColor
        {
            get { return selectedTextColor; }
            set { selectedTextColor = value; OnPropertyChanged("SelectedTextColor"); }
        }

        bool isRotatingOperation;
        public bool IsRotatingOperation
        {
            get { return isRotatingOperation; }
            set { isRotatingOperation = value; if (value == true) { IsScalingOperation = false; IsMovingOperation = false; } OnPropertyChanged("IsRotatingOperation"); }
        }

        bool isMovingOperation;
        public bool IsMovingOperation
        {
            get { return isMovingOperation; }
            set { isMovingOperation = value; if (value == true) { IsScalingOperation = false; IsRotatingOperation = false; } OnPropertyChanged("IsMovingOperation"); }
        }

        bool isBordersShowing;
        public bool IsBordersShowing
        {
            get { return isBordersShowing; }
            set { isBordersShowing = value; OnPropertyChanged("IsBordersShowing"); }
        }
        bool isButtonsShowing;
        public bool IsButtonsShowing
        {
            get { return isButtonsShowing; }
            set { isButtonsShowing = value; OnPropertyChanged("IsButtonsShowing"); }
        }
        bool isParentsShowing;
        public bool IsParentsShowing
        {
            get { return isParentsShowing; }
            set { isParentsShowing = value; OnPropertyChanged("IsParentsShowing"); }
        }

        bool isScalingOperation;
        public bool IsScalingOperation
        {
            get { return isScalingOperation; }
            set { isScalingOperation = value; if (value == true) { IsMovingOperation = false; IsRotatingOperation = false; } OnPropertyChanged("IsScalingOperation"); }
        }

        public RelayCommand SetBorderColorBlack
        {
            get;
            private set;
        }

        void setBorderColorBlack(object _param)
        {
            SelectedBorderColor = Colors.Black;
        }


        public RelayCommand SetFillColorTransparent
        {
            get;
            private set;
        }

        void setFillColorTransparent(object _param)
        {
            SelectedFillColor = Colors.Transparent;
        }


        public RelayCommand SetMovingOperation
        {
            get;
            private set;
        }

        void setMovingOperation(object _param)
        {
            if (!IsMovingOperation)
                IsMovingOperation = true;
            else
                IsMovingOperation = false;
            IsScalingOperation = false;
        }

        public RelayCommand SetScalingOperation
        {
            get;
            private set;
        }

        void setScalingOperation(object _param)
        {
            if (!IsScalingOperation)
                IsScalingOperation = true;
            else
                IsScalingOperation = false;
            IsMovingOperation = false;
        }
        
        #endregion

        #region Zoom

        double zoom;
        public double Zoom
        {
            get { return zoom; }
            set
            {
                if (value < 10)
                {
                    zoom = 10;
                }
                else
                {
                    zoom = value;
                } 
                OnPropertyChanged("Zoom");
            }
        }

        public RelayCommand ZoomPlus
        {
            get;
            private set;
        }

        void zoomPlus(object _param)
        {
            Zoom += 10;
        }

        public RelayCommand ZoomMinus
        {
            get;
            private set;
        }

        void zoomMinus(object _param)
        {
            Zoom -= 10;
        }
        public RelayCommand UnZoom
        {
            get;
            private set;
        }

        void unZoom(object _param)
        {
            Zoom = 100;
        }
        
        #endregion zoom

        #region Panels


        //public ObservableCollection<VEPage> ListViewColletion { get; set; }

       /* public RelayCommand AddPage
        {
            get;
            private set;
        }

        void addPage(object _param)
        {
            VEPage temp = new VEPage();
            ListViewColletion.Add(temp);
            SelectedPage = temp;
        }*/

        
        private Visibility pagesPanelVisibility;
        public Visibility PagesPanelVisibility
        {
            get { return pagesPanelVisibility; }
            set { pagesPanelVisibility = value; OnPropertyChanged("PagesPanelVisibility"); }
        }
        public RelayCommand TogglePagesPanelVisibility
        {
            get;
            private set;
        }

        void togglePagesPanelVisibility(object _param)
        {
            if (PagesPanelVisibility != Visibility.Collapsed)
                PagesPanelVisibility = Visibility.Collapsed;
            else
                PagesPanelVisibility = Visibility.Visible;
        }

        private Visibility picturesPanelVisibility;
        public Visibility PicturesPanelVisibility
        {
            get { return picturesPanelVisibility; }
            set { picturesPanelVisibility = value; OnPropertyChanged("PicturesPanelVisibility"); }
        }
        public RelayCommand TogglePicturesPanelVisibility
        {
            get;
            private set;
        }





        void togglePicturesPanelVisibility(object _param)
        {
            if (PicturesPanelVisibility != Visibility.Collapsed)
                PicturesPanelVisibility = Visibility.Collapsed;
            else
                PicturesPanelVisibility = Visibility.Visible;
        }
        #endregion
    }
}
