﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class LogicViewModel : ViewModelBase, IMode
    {
        public LogicViewModel(bool _enabled, LogicMenuViewModel _menu, LogicWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        LogicMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (LogicMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        LogicWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (LogicWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
