﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class StatementOfWorkViewModel : ViewModelBase, IMode
    {
        public StatementOfWorkViewModel(bool _enabled, StatementOfWorkMenuViewModel _menu, StatementOfWorkWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        StatementOfWorkMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (StatementOfWorkMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        StatementOfWorkWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (StatementOfWorkWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
