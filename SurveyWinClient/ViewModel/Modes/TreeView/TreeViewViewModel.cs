﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

using MVVM;

using Base.Interfaces;

namespace SurveyWinClient.ViewModel
{
    class TreeViewViewModel : ViewModelBase, IMode
    {
        public TreeViewViewModel(bool _enabled, TreeViewMenuViewModel _menu, TreeViewWindowViewModel _window)
        {
            IsEnabled = _enabled;
            Menu = _menu;
            Window = _window;
        }

        bool isEnabled;
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        TreeViewMenuViewModel menu;
        public IModeMenu Menu
        {
            get { return menu; }
            set
            {
                menu = (TreeViewMenuViewModel)value;
                OnPropertyChanged("Menu");
            }
        }

        TreeViewWindowViewModel window;
        public IModeWindow Window
        {
            get { return window; }
            set
            {
                window = (TreeViewWindowViewModel)value;
                OnPropertyChanged("Window");
            }
        }
    }
}
