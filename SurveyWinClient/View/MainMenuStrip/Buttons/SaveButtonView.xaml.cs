﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SurveyWinClient.ViewModel;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for MainMenuStripSaveButton.xaml
    /// </summary>
    public partial class SaveButtonView : UserControl
    {
        public SaveButtonView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            /*Survey.DocumentManagerWrapper _doc2 = Survey.DocumentManagerWrapper.GetInstance();
            Survey.DocumentWrapper wrpp = ProjectsTabControlView.GetActiveProject();
            int temp = _doc2.m_Documents.IndexOf(wrpp);
            _doc2.m_Documents[temp].Save(new Dictionary<string, Dictionary<string, BitmapImage>>());
            wrpp.Save(new Dictionary<string, Dictionary<string, BitmapImage>>() { });*/
            ProjectWorkSpaceViewModel vm = ProjectsTabControlView.GetActiveProjectViewModel();
            if (vm != null)
            {
                vm.SaveProject();
            }
            //_doc2.CloseDocument(wrpp);
        }
    }
}
