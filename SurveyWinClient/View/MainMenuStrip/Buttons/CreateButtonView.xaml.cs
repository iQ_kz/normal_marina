﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Survey;
using SurveyWinClient.ViewModel;
namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for MainMenuStripCreateButton.xaml
    /// </summary>
    public partial class CreateButtonView : UserControl
    {
        public CreateButtonView()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsEnabled = true;
            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            System.Windows.MessageBox.Show("CreateMenuItem 2 clicked.");
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            ChooseProjectTemplateWindowView modal = new ChooseProjectTemplateWindowView();
            modal.ShowDialog();
            //((ProjectWorkSpaceViewModel)ProjectsTabControlView.GlobalTabControlView.MainTabControl.Items[ProjectsTabControlView.GlobalTabControlView.MainTabControl.Items.Count - 1]).Name = wrpp.Name;
            //wrpp.Images.AddImage("sto5ry192422.bmp");
        } 
    }
}
