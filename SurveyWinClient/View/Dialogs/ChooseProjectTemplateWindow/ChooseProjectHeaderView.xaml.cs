﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for ChooseProjectHeaderView.xaml
    /// </summary>
    public partial class ChooseProjectHeaderView : UserControl
    {
        Window thisWindow;

        public ChooseProjectHeaderView()
        {
            InitializeComponent();
        }
        private void CloseLocalSurveyHeaderView_Loaded(object sender, RoutedEventArgs e)
        {
            thisWindow = GetParentWindow(this);
        }
        private void Header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                thisWindow.DragMove();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            thisWindow.Close();
        }
        public static Window GetParentWindow(DependencyObject child)
        {
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            if (parentObject == null)
            {
                return null;
            }

            Window parent = parentObject as Window;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                return GetParentWindow(parentObject);
            }
        }
    }
}
