﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SurveyWinClient.ViewModel;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for ChooseProjectTemplateWindow.xaml
    /// </summary>
    public partial class ChooseProjectTemplateWindowView : Window
    {
        public ChooseProjectTemplateWindowView()
        {
            InitializeComponent();
            ProjectName.Text = "New Project (" + Survey.DocumentManagerWrapper.GetInstance().Documents.Count.ToString() +")";
        }

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            MainViewModel vm = MainViewModel.GetMainViewModel;
            if (ProjectName.Text != "")
                vm.AddProject2(ProjectName.Text);
            else
                System.Windows.MessageBox.Show("Error: Project name is empty");
            this.Close();
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
