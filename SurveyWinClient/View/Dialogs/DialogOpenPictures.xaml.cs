﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SurveyWinClient.View.Dialogs
{
    /// <summary>
    /// Interaction logic for DialogOpenPictures.xaml
    /// </summary>
    public partial class DialogOpenPictures : Window
    {
        public DialogOpenPictures()
        {
            InitializeComponent();
        }
    }
}
