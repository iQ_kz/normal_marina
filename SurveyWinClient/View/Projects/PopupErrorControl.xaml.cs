﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace SurveyWinClient.View
{
    public partial class PopupErrorControl : UserControl
    {
        public ObservableCollection<ErrorInstance> errors { get { return _errors; } }
        ObservableCollection<ErrorInstance> _errors = new ObservableCollection<ErrorInstance>();
        public PopupErrorControl()
        {            
            InitializeComponent();
            ErrorList.ItemsSource = errors;
            errors.Add(new ErrorInstance(@"\Resources\ModesTabControl\Icons\ErrorImage.png", "First error"));
            errors.Add(new ErrorInstance(@"\Resources\ModesTabControl\Icons\ErrorImage.png", "Second error"));
            errors.Add(new ErrorInstance(@"\Resources\ModesTabControl\Icons\ErrorImage.png", "Another error"));            
        }
        
        public class ErrorInstance
        {
            public string ImagePath { get; set; }
            public string MainLabel { get; set; }
           
            public ErrorInstance(string ImagePath, string MainLabel)
            {
                this.ImagePath = ImagePath;
                this.MainLabel = MainLabel;                
            } 
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {            
            int ToIndex = ErrorList.Items.IndexOf(((FrameworkElement)sender).DataContext);
            errors.RemoveAt(ToIndex);
        }
    }
}
