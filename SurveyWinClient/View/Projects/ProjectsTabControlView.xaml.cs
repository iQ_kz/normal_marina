﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Survey;
using SurveyWinClient.ViewModel;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for ProjectsTabControl.xaml
    /// </summary>
    public partial class ProjectsTabControlView : UserControl
    {
        public static ProjectsTabControlView GlobalTabControlView;

        public TabControl MainTabControl { get { return mainTabControl; } }

        public static DocumentWrapper GetActiveProject()
        {
            return ((ProjectWorkSpaceViewModel)GlobalTabControlView.MainTabControl.SelectedItem).Document;
        }
        public static ProjectWorkSpaceViewModel GetActiveProjectViewModel()
        {
            return ((ProjectWorkSpaceViewModel)(GlobalTabControlView.MainTabControl.SelectedItem));
        }

        public ProjectsTabControlView()
        {
            GlobalTabControlView = this;
            InitializeComponent();
        }

    }
}
