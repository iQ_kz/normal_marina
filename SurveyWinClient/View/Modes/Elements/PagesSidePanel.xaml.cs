﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Survey;
using SurveyWinClient;

namespace SurveyWinClient.View
{
    /*public class PagesSidePanelBlockInfo
    {
        public RenderTargetBitmap MainImage { get; set; }
        public string MainLabel { get; set; }
        public PagesSidePanelBlockInfo(RenderTargetBitmap MainImageSource, string MainLabel)
        {
            this.MainImage = MainImageSource;
            this.MainLabel = MainLabel;
        }
    };*/

   /* public class VEPage
    {
        public string Label { get; set; }
        public List<BlockBorder> Collection { get; set; }
        public double LastBlockY { get; set; }
        public List<FrameworkElement> Scalables { get; set; }
        public VEPage()
        {
            LastBlockY = 0.1;
            Collection = new List<BlockBorder>();
            Scalables = new List<FrameworkElement>();
            Label = "Page";
        }
    }*/
    public partial class PagesSidePanel : UserControl
    {
        public Survey.ObservableRangeCollection<PageWrpp> ListViewColletion { get; set; }
        public PagesSidePanel()
        {            
            InitializeComponent();
            Survey.DocumentManagerWrapper.Instance(this.Dispatcher);

            DocumentWrapper _doc = Survey.DocumentManagerWrapper.GetInstance().Documents[ProjectsTabControlView.GlobalTabControlView.MainTabControl.SelectedIndex + 1];
            ListViewColletion = _doc.Project.Chapters[0].Pages;
            //_doc.Project.Chapters[0].Pages[0].Miniature = ProjectsTabControlView.GetActiveProject().Images.AddImage("sto5ry19242.png").ImageData;
            ListViewDragNDrop.SelectedIndex = 1;
        }        

        void VisualWindowView_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void DropSwap(object sender, DragEventArgs e)
        {
            string[] temp = e.Data.GetFormats();
            PageWrpp temp2 = (Survey.PageWrpp)e.Data.GetData("Survey.PageWrpp");
            ObservableCollection<PageWrpp> collection = (ObservableCollection<PageWrpp>)ListViewDragNDrop.ItemsSource;
            int FromIndex = ListViewDragNDrop.Items.IndexOf(e.Data.GetData("Survey.PageWrpp"));
            int ToIndex = ListViewDragNDrop.Items.IndexOf(((FrameworkElement)sender).DataContext);
            ObservableCollectionExtensions.Swap<PageWrpp>(collection, FromIndex, ToIndex);
        }
        private void DropBetween(object sender, DragEventArgs e)
        {
            ObservableCollection<PageWrpp> collection = (ObservableCollection<PageWrpp>)ListViewDragNDrop.ItemsSource;
            int FromIndex = ListViewDragNDrop.Items.IndexOf(e.Data.GetData("Survey.PageWrpp"));
            int ToIndex = ListViewDragNDrop.Items.IndexOf(((FrameworkElement)sender).DataContext);
            ObservableCollectionExtensions.ChangePos<PageWrpp>(collection, FromIndex, ToIndex);
        }
        private void DropLast(object sender, DragEventArgs e)
        {
            ObservableCollection<PageWrpp> collection = (ObservableCollection<PageWrpp>)ListViewDragNDrop.ItemsSource;
            int FromIndex = ListViewDragNDrop.Items.IndexOf(e.Data.GetData("Survey.PageWrpp"));
            int ToIndex = collection.Count - 1;
            ObservableCollectionExtensions.ChangePos<PageWrpp>(collection, FromIndex, ToIndex);
        }

        private void AddPage_Click(object sender, RoutedEventArgs e)
        {
            Survey.DocumentManagerWrapper.Instance(this.Dispatcher);
            DocumentWrapper _doc = Survey.DocumentManagerWrapper.GetInstance().Documents[ProjectsTabControlView.GlobalTabControlView.MainTabControl.SelectedIndex + 1];
            PageWrpp pageOne = new PageWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0), new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1000, 500, 0), "Page " + (_doc.Project.Chapters[0].Childs.Count + 1));
            _doc.Project.Chapters[0].AddChild(pageOne);
            ListViewDragNDrop.SelectedIndex = - 1;
            ListViewDragNDrop.SelectedIndex = _doc.Project.Chapters[0].Pages.Count - 1;
        }

        /*private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            VEPage temp = new VEPage();
            ListViewColletion.Add(temp);
            ListViewDragNDrop.SelectedItem = temp;
        }*/

    }
}
