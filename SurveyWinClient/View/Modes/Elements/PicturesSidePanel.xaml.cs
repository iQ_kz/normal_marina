﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using Microsoft.Win32;
using System.Drawing.Imaging;

namespace SurveyWinClient.View
{
    public class PicturesSidePanelBlockInfo
    {
        public BitmapImage MainImage { get; set; }
        public string MainLabel { get; set; }
        public PicturesSidePanelBlockInfo(BitmapImage MainImageSource, string MainLabel)
        {
            this.MainImage = MainImageSource;
            this.MainLabel = MainLabel;
        }
    };
    public partial class PicturesSidePanel : UserControl
    {
        public ObservableCollection<Survey.ImageWrpp> ListViewColletion { get; set; }
        public PicturesSidePanel()
        {
            
            ListViewColletion = ProjectsTabControlView.GetActiveProject().Images.Images;
            /*ListViewColletion = new ObservableCollection<PicturesSidePanelBlockInfo>();
            BitmapImage temp = new BitmapImage(new Uri(@"\Resources\MainMenuStrip\Icons\Create_Icon.png",UriKind.Relative));
            BitmapImage temp2 = new BitmapImage(new Uri(@"\Resources\MainMenuStrip\Icons\sto5ry1942.png", UriKind.Relative));
            ListViewColletion.Add(new PicturesSidePanelBlockInfo(temp, "Label"));
            ListViewColletion.Add(new PicturesSidePanelBlockInfo(temp2, "Label"));*/
            InitializeComponent();
        }

        void VisualWindowView_Loaded(object sender, RoutedEventArgs e)
        {
            
           
            //ListViewColletion.Add(new DragNDropBlockInfo(canvasOverrided.GetCurrentPreview(), "Label3"));
        }
         
        private void DropSwap(object sender, DragEventArgs e)
        {
            ObservableCollection<Survey.ImageWrpp> collection = (ObservableCollection<Survey.ImageWrpp>)ListViewDragNDrop.ItemsSource;
            int FromIndex = ListViewDragNDrop.Items.IndexOf(e.Data.GetData("SurveyWinClient.View.PicturesSidePanelBlockInfo"));
            int ToIndex = ListViewDragNDrop.Items.IndexOf(((FrameworkElement)sender).DataContext);
            ObservableCollectionExtensions.Swap<Survey.ImageWrpp>(collection, FromIndex, ToIndex);
        }
        private void DropBetween(object sender, DragEventArgs e)
        {
            ObservableCollection<Survey.ImageWrpp> collection = (ObservableCollection<Survey.ImageWrpp>)ListViewDragNDrop.ItemsSource;
            int FromIndex = ListViewDragNDrop.Items.IndexOf(e.Data.GetData("SurveyWinClient.View.PicturesSidePanelBlockInfo"));
            int ToIndex = ListViewDragNDrop.Items.IndexOf(((FrameworkElement)sender).DataContext);
            ObservableCollectionExtensions.ChangePos<Survey.ImageWrpp>(collection, FromIndex, ToIndex);
        }
        private void DropLast(object sender, DragEventArgs e)
        {
            ObservableCollection<Survey.ImageWrpp> collection = (ObservableCollection<Survey.ImageWrpp>)ListViewDragNDrop.ItemsSource;
            int FromIndex = ListViewDragNDrop.Items.IndexOf(e.Data.GetData("SurveyWinClient.View.PicturesSidePanelBlockInfo"));
            int ToIndex = collection.Count - 1;
            ObservableCollectionExtensions.ChangePos<Survey.ImageWrpp>(collection, FromIndex, ToIndex);
        }

        /*private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
           // openFileDialog.Filter = "Image Files|*.jpg;*.bmp;*.png";

            //openFileDialog.Filter = "";
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            String codecName="";
            foreach (ImageCodecInfo codec in codecs)
            {
                //codecName = codec.CodecName.Substring(8).Replace("Codec", "Files").Trim();
                codecName += codec.FormatDescription + " Files(" + codec.FilenameExtension + ")|" + codec.FilenameExtension +"|";
                
                //openFileDialog.Filter = String.Format("{0}{1}{2} ({3})|{3}", openFileDialog.Filter, "|", codecName, codec.FilenameExtension);
            }
            codecName.Trim('|');
            
            // ?.Filter = String.Format("{0}{1}{2} ({3})|{3}", ?.Filter,"|", "All Files", "*.*");
            openFileDialog.Filter = codecName.Substring(0,codecName.Length-1);

            if (openFileDialog.ShowDialog() == true)
                ProjectsTabControlView.GetActiveProject().Images.AddImage(openFileDialog.FileName);
        }
        */
        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            String codecName = "";
            openFileDialog.Filter = "All Image files(*.*)|";
            foreach (ImageCodecInfo codec in codecs)
            {
                openFileDialog.Filter += "" + codec.FilenameExtension +";";
                codecName += "|" + codec.FormatDescription + " Files (" + codec.FilenameExtension + ")|" + codec.FilenameExtension;
            }
            openFileDialog.Filter += codecName;
            if (openFileDialog.ShowDialog() == true)
                ProjectsTabControlView.GetActiveProject().Images.AddImage(openFileDialog.FileName);
        }
    }
}
