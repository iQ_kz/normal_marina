﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Survey;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for TypeWriterField.xaml
    /// </summary>
    public partial class TypeWriterField : UserControl
    {
        TextPointer pointer; //на начало рана
        TextWrpp wrpp;
        TextRange tr;
        public TypeWriterField(TextRange _tr, TextPointer _pointer, TextWrpp _wrpp)
        {
            tr = _tr;
            wrpp = _wrpp;
            pointer = _pointer;
            InitializeComponent();
            mainTextbox.Text = _pointer.GetTextInRun(LogicalDirection.Forward);
        }

        private void mainTextbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            pointer.DeleteTextInRun(pointer.GetTextRunLength(LogicalDirection.Forward));
            pointer.InsertTextInRun(mainTextbox.Text);
            VisualEditorBorder.SaveTextRangeToTextWrpp(tr, wrpp);
        }
        
    }
}
