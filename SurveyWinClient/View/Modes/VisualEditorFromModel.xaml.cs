﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Survey;
using SurveyWinClient;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for VisualEditorFromModel.xaml
    /// </summary>
    public partial class VisualEditorFromModel : UserControl
    {
        public DocumentWrapper _doc;
        private PageWrpp _page;
        private MDI.MDIWindow MDIparent;
        private bool isDragging;
        private bool isRotating;
        public VisualEditorBorder activeElement;
        public static readonly DependencyProperty ActiveElementProperty =
        DependencyProperty.Register("ActiveElement", typeof(VisualEditorBorder), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public VisualEditorBorder ActiveElement
        {
            get { return (VisualEditorBorder)GetValue(ActiveElementProperty); }
            set { SetValue(ActiveElementProperty, value); }
        }



        public static readonly DependencyProperty IsInvalidProperty =
        DependencyProperty.Register("IsInvalid", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool IsInvalid
        {
            get { return (bool)GetValue(IsInvalidProperty); }
            set { SetValue(IsInvalidProperty, value); }
        }

        public static readonly DependencyProperty ActivePageProperty =
        DependencyProperty.Register("ActivePage", typeof(int), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public int ActivePage
        {
            get { return (int)GetValue(ActivePageProperty); }
            set { SetValue(ActivePageProperty, value); }
        }

        public static readonly DependencyProperty FontSizeProperty =
        DependencyProperty.Register("FontSize", typeof(double), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }
        public static readonly DependencyProperty FontItalicProperty =
        DependencyProperty.Register("FontItalic", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool FontItalic
        {
            get { return (bool)GetValue(FontItalicProperty); }
            set { SetValue(FontItalicProperty, value); }
        }

        public static readonly DependencyProperty FontBoldProperty =
        DependencyProperty.Register("FontBold", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool FontBold
        {
            get { return (bool)GetValue(FontBoldProperty); }
            set { SetValue(FontBoldProperty, value); }
        }
        
        public static readonly DependencyProperty FontUnderlineProperty =
        DependencyProperty.Register("FontUnderline", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool FontUnderline
        {
            get { return (bool)GetValue(FontUnderlineProperty); }
            set { SetValue(FontUnderlineProperty, value); }
        }

        public static readonly DependencyProperty FontStrikethroughProperty =
        DependencyProperty.Register("FontStrikethrough", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool FontStrikethrough
        {
            get { return (bool)GetValue(FontStrikethroughProperty); }
            set { SetValue(FontStrikethroughProperty, value); }
        }
        
        public static readonly DependencyProperty FontSubscriptProperty =
        DependencyProperty.Register("FontSubscript", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool FontSubscript
        {
            get { return (bool)GetValue(FontSubscriptProperty); }
            set { SetValue(FontSubscriptProperty, value); }
        }

        public static readonly DependencyProperty FontSuperscriptProperty =
        DependencyProperty.Register("FontSuperscript", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool FontSuperscript
        {
            get { return (bool)GetValue(FontSuperscriptProperty); }
            set { SetValue(FontSuperscriptProperty, value); }
        }

        public static readonly DependencyProperty SelectedBorderColorProperty =
        DependencyProperty.Register("SelectedBorderColor", typeof(Color), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public Color SelectedBorderColor
        {
            get { return (Color)GetValue(SelectedBorderColorProperty); }
            set { SetValue(SelectedBorderColorProperty, value); }
        }

        public static readonly DependencyProperty SelectedFillColorProperty =
        DependencyProperty.Register("SelectedFillColor", typeof(Color), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public Color SelectedFillColor
        {
            get { return (Color)GetValue(SelectedFillColorProperty); }
            set { SetValue(SelectedFillColorProperty, value); }
        }

        public static readonly DependencyProperty SelectedTextColorProperty =
        DependencyProperty.Register("SelectedTextColor", typeof(Color), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public Color SelectedTextColor
        {
            get { return (Color)GetValue(SelectedTextColorProperty); }
            set { SetValue(SelectedTextColorProperty, value); }
        }

        public static readonly DependencyProperty ZoomProperty =
        DependencyProperty.Register("VEMZoom", typeof(double), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public double Zoom
        {
            get
            {
                double toret = (double)GetValue(ZoomProperty);
                if (toret < 10)
                    return 0.1;
                else
                    return toret / 100;
            }
            set { SetValue(ZoomProperty, value); }
        }
        public static readonly DependencyProperty IsButtonsShowingProperty =
        DependencyProperty.Register("IsButtonsShowing", typeof(bool), typeof(VisualEditorFromModel), new UIPropertyMetadata(null));
        public bool IsButtonsShowing
        {
            get { return (bool)GetValue(IsButtonsShowingProperty); }
            set { SetValue(IsButtonsShowingProperty, value); }
        }

        private Point lowestPoint;
        public Point LowestPoint
        {
            get { return lowestPoint; }
            set { lowestPoint = value; _page.DesignElement.Coordinate= new CoordDatWrpp(_page.DesignElement.Coordinate.m_Top,_page.DesignElement.Coordinate.m_Left,_page.DesignElement.Coordinate.m_Width, value.Y,_page.DesignElement.Coordinate.m_Angle); ActualizeCanvas(); }
        } 


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == SelectedFillColorProperty)
            {
                if (ActiveElement != null)
                {
                    ActiveElement.SetFillColor(SelectedFillColor);
                }
            }
            if (e.Property == ZoomProperty)
            {
                mainCanvas.LayoutTransform = new ScaleTransform(Zoom, Zoom);
            }
            if (e.Property == ActivePageProperty)
            {
                if (ActivePage >= 0)
                {
                    _page = _doc.Project.Chapters[0].Pages[ActivePage];
                    mainCanvas.Children.Clear();
                    SetElements();
                }
            }
            if (e.Property == IsButtonsShowingProperty)
            {
                if(addButton!=null)
                    if (IsButtonsShowing)
                    {
                        addButton.Visibility = Visibility.Visible;
                    }
                    else
                        addButton.Visibility = Visibility.Collapsed;
            }

            if (e.Property == ActiveElementProperty)
            {
                if(e.OldValue!=null)
                    ((VisualEditorBorder)e.OldValue).IsActiveInEditor = false;
            }
            if (e.Property == FontSizeProperty)
            {
                if (ActiveElement != null)
                    ActiveElement.SetFontSize(FontSize);
            }
            if (e.Property == SelectedTextColorProperty)
            {
                if (ActiveElement != null)
                    ActiveElement.SetTextColor(SelectedTextColor);
            }
            if (e.Property == SelectedBorderColorProperty)
            {
                if (ActiveElement != null)
                    ActiveElement.CreateBorderVisual(SelectedBorderColor);
            }


            if (e.Property == FontItalicProperty || e.Property == FontBoldProperty||
                e.Property == FontSubscriptProperty || e.Property == FontSuperscriptProperty||
                e.Property == FontUnderlineProperty || e.Property == FontStrikethroughProperty)
            {
                
                if (ActiveElement != null)
                {
                    FontWeight fontwg = FontWeights.Normal;
                    FontStyle fontst = FontStyles.Normal;
                    TextDecorationCollection fontdec = null;
                    FontVariants fontvar = System.Windows.FontVariants.Normal;
                    if (FontItalic)
                        fontst = FontStyles.Italic;
                    if (FontUnderline)
                        fontdec = TextDecorations.Underline;
                    if (FontStrikethrough)
                        fontdec = TextDecorations.Strikethrough;
                    if (FontBold)
                        fontwg = FontWeights.Bold;
                    if (FontSuperscript)
                        fontvar = System.Windows.FontVariants.Superscript;
                    if (FontSubscript)
                        fontvar = System.Windows.FontVariants.Subscript;
                    
                    //underline, strikethrough
                    
                    ActiveElement.SetFontStyle(fontwg, fontst, fontdec, fontvar);
                }
                
                
            }
            if (e.Property == IsInvalidProperty)
            {
                if (IsInvalid)
                {
                    SetElements();
                    IsInvalid = false;
                }
            }

            base.OnPropertyChanged(e);
        }
        Dictionary<uint, VisualEditorBorder> myDictionary = new Dictionary<uint, VisualEditorBorder>();

        public double VEEffectiveWidth
        {
            get { return mainScrollViewer.ActualWidth - SystemParameters.VerticalScrollBarWidth; }
        }

        public double WindowWidth
        {
            get { return MDIparent.Width; }
        }

      
        public void ActualizeCanvas()
        {

            if (VEEffectiveWidth > 1)
            {
                Scale = VEEffectiveWidth / _page.DesignElement.Coordinate.m_Width;
                mainCanvas.Width = VEEffectiveWidth;
                mainCanvas.Height = VEEffectiveWidth * _page.DesignElement.Coordinate.m_Height / _page.DesignElement.Coordinate.m_Width;
                for (int i = 0; i < mainCanvas.Children.Count; i++)
                {
                    if (mainCanvas.Children[i] is VisualEditorBorder)
                        ((VisualEditorBorder)(mainCanvas.Children[i])).ActualizeToCoords();
                }
            }
        }

        public RenderTargetBitmap SetCurrentPreview()
        {
            bool prevButtShowing = IsButtonsShowing;
            IsButtonsShowing = false;
            FrameworkElement parent = this.Parent as FrameworkElement;
            mainViewbox.RemoveChild(mainCanvas);
            Viewbox viewbox = new Viewbox();


            double bmpHeight = 80;
            double bmpWidth = 80;
            viewbox.Height = bmpHeight;
            viewbox.Width = bmpWidth;
            viewbox.Child = mainCanvas; //control to render

            viewbox.Measure(new System.Windows.Size(bmpWidth, bmpHeight));
            viewbox.Arrange(new Rect(0, 0, bmpWidth, bmpHeight));

            viewbox.UpdateLayout();

            RenderTargetBitmap render = new RenderTargetBitmap((int)bmpWidth, (int)bmpHeight, 96, 96, PixelFormats.Pbgra32);

            render.Render(viewbox);
            //var stream2 = new FileStream("C:/Games/test2.jpg", FileMode.Create);

            //BitmapEncoder encoder = new JpegBitmapEncoder();

            //encoder.Frames.Add(BitmapFrame.Create(render));

            //encoder.Save(stream2);

            //stream2.Close();
            viewbox.RemoveChild(mainCanvas);
            //mainViewbox.Height = double.NaN;
            //mainViewbox.Width = double.NaN;
            IsButtonsShowing = true;
            mainViewbox.Child = mainCanvas;
            return render;
            /*this.Measure(new Size(500,500));
            this.Arrange(new Rect(new Size(500, 500)));

            var bitmap = new RenderTargetBitmap(
                (int)new Size(500, 500).Width, (int)new Size(500, 500).Height, 96, 96, PixelFormats.Default);

            bitmap.Render(this);

            */
        }
        AddButton addButton;
        public void SetElements()
        {
            LowestPoint = new Point(0,_page.DesignElement.Coordinate.m_Height);
            mainCanvas.Children.Clear();
            addButton = new AddButton();
            Canvas.SetTop(addButton, 0);
            Canvas.SetLeft(addButton, 0);
            mainCanvas.Children.Add(addButton);
            addButton.mainButton.Click += butt_Click;
            addButton.mainText.Text = "    Add Block";
            if(IsButtonsShowing&&_page.Childs.Count<1)
                addButton.Visibility = Visibility.Visible;
            else
                addButton.Visibility = Visibility.Collapsed;
            myDictionary.Clear();
            ActualizeCanvas();
            for (int i = 0; i < _page.ChildsTree.Count; i++)
            {

                    //(_page.ChildsTree[i] as BlockWrpp).BSDesignElement.BSCoordinate = new CoordDatWrpp(0.2,0.2,0.6,0.6,0,0);
                    DocumentObject current = (DocumentObject)(_page.ChildsTree[i]);

                    VisualEditorBorder bord = new VisualEditorBorder(ref current, this);
                    myDictionary.Add(current.Id, bord);
                    mainCanvas.Children.Add(bord);
                    bord.ActualizeToCoords();

               /* if (_page.ChildsTree[i] is QaWrpp)
                {
                    //(_page.ChildsTree[i] as BlockWrpp).BSDesignElement.BSCoordinate = new CoordDatWrpp(0.2,0.2,0.6,0.6,0,0);
                    QaWrpp current = (QaWrpp)(_page.ChildsTree[i]);

                    VisualEditorBorder bord = new VisualEditorBorder(false, mainCanvas, ref current, this);
                    myDictionary.Add(current.Id, bord);
                    mainCanvas.Children.Add(bord);
                    bord.ActualizeToCoords();
                }
                if (_page.ChildsTree[i] is QuestionWrpp)
                {
                    //(_page.ChildsTree[i] as BlockWrpp).BSDesignElement.BSCoordinate = new CoordDatWrpp(0.2,0.2,0.6,0.6,0,0);
                    QuestionWrpp current = (QuestionWrpp)(_page.ChildsTree[i]);

                    VisualEditorBorder bord = new VisualEditorBorder(false, mainCanvas, ref current, this);
                    myDictionary.Add(current.Id, bord);
                    mainCanvas.Children.Add(bord);
                    bord.ActualizeToCoords();
                }
                if (_page.ChildsTree[i] is HeaderWrpp)
                {

                }*/
            }

            ActualizeCanvas();
            _page.Miniature.Clear();
            _page.Miniature.Add(SetCurrentPreview());
        }

        public void EstablishBindings()
        {
            var binding = new Binding("Zoom");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(ZoomProperty, binding);
            binding = new Binding("IsInvalid");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(IsInvalidProperty, binding);
            binding = new Binding("FontSize");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(FontSizeProperty, binding);
            binding = new Binding("FontItalic");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(FontItalicProperty, binding);
            binding = new Binding("FontBold");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(FontBoldProperty, binding);

            binding = new Binding("FontUnderline");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(FontUnderlineProperty, binding);
            binding = new Binding("FontStrikethrough");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(FontStrikethroughProperty, binding);
            binding = new Binding("FontSuperscript");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(FontSuperscriptProperty, binding);
            binding = new Binding("FontSubscript");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(FontSubscriptProperty, binding);

            binding = new Binding("IsButtonsShowing");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(IsButtonsShowingProperty, binding);
            binding = new Binding("SelectedFillColor");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(SelectedFillColorProperty, binding);
            binding = new Binding("SelectedTextColor");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(SelectedTextColorProperty, binding);
            binding = new Binding("SelectedBorderColor");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(SelectedBorderColorProperty, binding);
        }

        public void AddImageBlock(DragEventArgs e, uint id, string imgId, double widthpix, double heightpix)
        {
            
            for (int i = 0; i < _page.ChildsTree.Count; i++)
            {
                DocumentObject cur =((_page.ChildsTree[i]) as DocumentObject);
                if (cur.Id == id)
                {
                    ImageProperties imgProps = new ImageProperties();
                    imgProps.m_ImageId = imgId;
                    Point point = e.GetPosition(myDictionary[id]);
                    point.X /= Scale;
                    point.Y /= Scale;
                    ImageWrpp img = ProjectsTabControlView.GetActiveProject().Images.ImageByID(imgId);
					System.Drawing.Image imgData = img.ImageData;

					BorderDat borderDat = new BorderDat();
                    HeaderWrpp headerOne = new HeaderWrpp(_doc, new ColorDatWrpp(128, 128, 128, 0), new CoordDatWrpp(0, 0, 1, 1, 0), 
                        new ColorDatWrpp(128, 128, 128, 0),
                        new CoordDatWrpp(point.Y, point.X, imgData.Width, imgData.Height, 0)
								, @""
								, imgProps, borderDat
								, "Image");
                    cur.AddChild(headerOne);
                    ProjectsTabControlView.GetActiveProjectViewModel().InvalidateEditors();
                    break;
                }
            }
            
        }

        public VisualEditorFromModel()
        {
            
            InitializeComponent();
            Canvas.SetLeft(mainCanvas, 0);
            Canvas.SetTop(mainCanvas, 0);
            EstablishBindings();
            this.Unloaded += VisualEditorFromModel_Unloaded;
            this.Loaded += VisualEditorFromModel_Loaded;
           
            mainCanvas.Background = Brushes.White;
            Survey.DocumentManagerWrapper.Instance(this.Dispatcher);
            _doc = Survey.DocumentManagerWrapper.GetInstance().Documents[ProjectsTabControlView.GlobalTabControlView.MainTabControl.SelectedIndex + 1];
            
        }

        void VisualEditorFromModel_Loaded(object sender, RoutedEventArgs e)
        {
            MDIparent = VisualTreeExtensions.FindParent<MDI.MDIWindow>(this);
            /*if (MDIparent.Width<500)
                MDIparent.Width = 500;
            if (MDIparent.Height < 500)
                MDIparent.Height = 500;
            MDIparent.UpdateLayout();*/
            if (_doc.Project.Chapters[0].Pages.Count > 0)
            {
                _page = _doc.Project.Chapters[0].Pages[ActivePage];
                mainCanvas.Children.Clear();
                SetElements();
            }
            
        }

        void butt_Click(object sender, RoutedEventArgs e)
        {
            _page.AddChild(new BlockWrpp(_doc,new ColorDatWrpp(0,0,0,255),new CoordDatWrpp(0,0,0,0,0),new ColorDatWrpp(0,0,0,0),new CoordDatWrpp(0,0,900,200,0), "Block" + _page.Childs.Count));
            ProjectsTabControlView.GetActiveProjectViewModel().InvalidateEditors();
        }

        void VisualEditorFromModel_Unloaded(object sender, RoutedEventArgs e)
        {
            mainCanvas.Children.Clear();
        }

        public double Scale { get; set; }
        private void mainScrollViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            
            if (_page != null)
            {
                ActualizeCanvas();
            }
        }
        public void InitiateDragging(VisualEditorBorder _activeElement)
        {
            isDragging = true;
            activeElement = _activeElement;
        }

        public void InitiateRotating(VisualEditorBorder _activeElement)
        {
            isRotating = true;
            activeElement = _activeElement;
        }

        private void mainCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            
            if (isDragging)
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    activeElement.DoDrag(e, Zoom);
                    ActualizeCanvas();
                }
                else
                {
                    isDragging = false;
                    _page.Miniature.Clear();
                    _page.Miniature.Add(SetCurrentPreview());
                    //activeElement.EndChanges();
                }
            }
            if (isRotating)
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    activeElement.DoRotate(e);
                    ActualizeCanvas();
                }
                else
                {
                    isRotating = false;
                    _page.Miniature.Clear();
                    _page.Miniature.Add(SetCurrentPreview());
                    //activeElement.EndChanges();
                }
            } 
        }
    }
}
