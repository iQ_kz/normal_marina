﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using SurveyWinClient;

namespace SurveyWinClient.View
{
    /// <summary>
    /// Interaction logic for TreeViewWindow.xaml
    /// </summary>
    /// 
    


    public partial class TreeViewWindowView : UserControl
    {
        public static readonly DependencyProperty ZoomProperty =
        DependencyProperty.Register("VEMZoom", typeof(double), typeof(TreeViewWindowView), new UIPropertyMetadata(null));
        public double Zoom
        {
            get
            {
                double toret = (double)GetValue(ZoomProperty);
                if (toret < 10)
                    return 0.1;
                else
                    return toret / 100;
            }
            set { SetValue(ZoomProperty, value); }
        }
        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            
            if (e.Property == ZoomProperty)
            {
                if(Zoom>0.1)
                    treeView2.LayoutTransform = new ScaleTransform(Zoom, Zoom);
            }
            base.OnPropertyChanged(e);
        }

        public TreeViewWindowView()
        {
            var binding = new Binding("Zoom");
            binding.Mode = BindingMode.TwoWay;
            SetBinding(ZoomProperty, binding);
            InitializeComponent();
        }
    }
}
